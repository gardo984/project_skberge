#! /usr/bin/env python
# -*- encoding:utf-8 -*-

if __name__ == '__main__':

    # load django modules
    import loader as obj
    obj.load_django()

    from apps.reservas import models as mdl
    from django.db.models import (
        Q, F, Count, Sum, Value,
        ExpressionWrapper,
        CharField, IntegerField, TimeField,
        Max, Min, Prefetch,
    )
    from datetime import datetime, timedelta
    from dateutil import relativedelta
    from sys import exit
    import time
    import urllib.request

    def send_mail(pattern_reserva, pattern_url):
        wmessage = 'Sending confirmation mail for reserva {}'.format(
            pattern_reserva)
        url_request = '{}{}'.format(pattern_url, pattern_reserva)
        outcome = urllib.request.urlopen(url_request).read()
        obj.printMessage(url_request)

    DEADLINE = 3
    DEADLINE_FIRST = 5
    DEADLINE_SECOND = 10

    # URL_MAIL =
    # 'http://salasqa.skbergeperu.com.pe:8091/intranet/reservas/reservas_alerts/'
    URL_MAIL = 'http://10.172.1.201:8001/intranet/reservas/reservas_alerts/'
    time_format = "%H:%M:%S"
    date_format = "%Y-%m-%d"
    date_filter = datetime.now() - timedelta(minutes=30)
    Qfilters = Q(
        fecha_evento=date_filter.date(),
        hora_inicio__gte=date_filter.time(),
        estado__id=1,
    )
    Qfilters.add(~Q(confirmacion=1), Qfilters.connector)

    wfields = [
        "id", "fecha_evento", "hora_inicio", "hora_final",
        "fecha_confirmacion", "nintentos",
    ]

    qsconfirmacion = mdl.ReservasConfirmacion.objects.filter(procesado=0)
    qsreservas = mdl.Reservas.objects.filter(Qfilters) \
        .prefetch_related(Prefetch("reservasconfirmacion", queryset=qsconfirmacion)) \
        .annotate(fecha_confirmacion=Max('reservasconfirmacion__fhregistro'),
                  nintentos=Count('reservasconfirmacion__id'),) \
        .order_by("hora_inicio") \
        .values(*wfields)

    if not qsreservas.count() > 0:
        obj.printMessage("There's not data to process")
        exit()

    obj.printMessage("Processing data....")
    current_time = datetime.now()
    for item in qsreservas:
        time_value = item.get("hora_inicio").strftime(time_format)
        date_value = item.get("fecha_evento").strftime(date_format)
        value = '{} {}'.format(date_value, time_value)
        object_time = datetime.strptime(
            value, '{} {}'.format(date_format, time_format))
        diff = relativedelta.relativedelta(
            object_time, current_time)
        # print(diff.minutes)
        if item.get("fecha_confirmacion") is None:
            if diff.minutes <= DEADLINE:
                send_mail(item.get("id"), URL_MAIL)
        else:
            wintentos = item.get("nintentos")
            wvalue = item.get("fecha_confirmacion")
            wdiff = relativedelta.relativedelta(current_time, wvalue)
            # print(wdiff.minutes)
            if wdiff.minutes >= DEADLINE_FIRST and wintentos == 1:
                # print(wdiff.minutes)
                wmessage = "Resending email for reserva {}".format(
                    item.get("id"))
                obj.printMessage(wmessage)
                send_mail(item.get("id"), URL_MAIL)
            if wdiff.minutes >= DEADLINE_SECOND and wintentos >= 2:
                # print(wdiff.minutes)
                wmessage = "Disabling reserva {} for overcome the time limit".format(
                    item.get("id"))
                obj.printMessage(wmessage)
                mdl.Reservas.objects.filter(id=item.get("id")) \
                    .update(estado_id=2, fhmodificacion=datetime.now(),)

    obj.printMessage("Process has finished.")
