#! /usr/bin/env python
# -*- encoding:utf-8 -*-

import os
import sys
from datetime import datetime


def load_django():
    dir_config = os.path.join(os.path.dirname(__file__), '../intranet')
    sys.path.append(os.path.abspath(dir_config))
    os.environ["DJANGO_SETTINGS_MODULE"] = 'intranet.settings'
    import django
    django.setup()


def printMessage(wmessage):
    print("{} - {}".format(getCurrentTime(), wmessage))


def getCurrentTime():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")
