#!/bin/bash
UAGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36"
HOST="10.172.1.201:8001"
PATH="http://${HOST}/intranet/api/v1/metas/obtener_ventas/"
# validating if range parameters were entered
if [ ! -z $1 ] && [ $1 = '--daily' ] 
then
	DATE_FROM=$(/bin/date +'%Y%m01')
	DATE_TO=$(/bin/date +'%Y%m%d')
	PATH="${PATH}?date_from=${DATE_FROM}&date_to=${DATE_TO}"
fi
/bin/echo -e "###### Process at $(/bin/date +'%F %T') ######\n"
/bin/echo -e "url: ${PATH}\n"
/usr/bin/curl -v -A "${UAGENT}"  "${PATH}"
/bin/echo -e "\n\n###### Process finished at $(/bin/date +'%F %T') ######"
