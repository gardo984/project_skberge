#! /usr/bin/env python
# -*- encoding:utf-8 -*-

if __name__ == '__main__':

     # load django modules
    import loader as obj
    obj.load_django()

    from apps.metas import models as mdlmet
    from django.contrib.auth import models as mdlauth
    from django.db.models import (
        Q, F, Count, Sum, Value,
        ExpressionWrapper,
        CharField, IntegerField, TimeField,
        Max, Min, Prefetch,
    )
    from datetime import datetime, timedelta
    from dateutil import relativedelta

    DEFAULT_PASSWD = '123456'
    DEFAULT_GRANT = 'PROFILE_RESERVAS'

    Qfilters = Q(
        estado__id=1,
        usuario_intranet__isnull=True,
        usr_usuario__isnull=False,
    )
    Qfilters.add(
        ~Q(email_empresarial__icontains="no disponible"),
        Qfilters.connector,
    )
    fields = [
        "usr_usuario", "nombres", "apellidos",
        "email_empresarial", "estado_id"
    ]
    obj.printMessage("Getting list of users that doesnt exists in user table")
    qs = mdlmet.Empleados.objects.filter(Qfilters) \
        .values(*fields)

    # insert all users
    rows = [mdlauth.User(
        username=x.get("usr_usuario").lower(),
        first_name=x.get("nombres").upper(),
        last_name=x.get("apellidos").upper(),
        email=x.get("email_empresarial").lower(),
        is_active=x.get("estado_id"),
    )for x in qs]
    obj.printMessage("Inserting user list....")
    mdlauth.User.objects.bulk_create(rows, len(rows))
    obj.printMessage(
        "{} users has been added into user table".format(str(len(rows))))

    # get all users added previously and update them with the default passwd
    wgroups = mdlauth.Group.objects.filter(name__icontains=DEFAULT_GRANT)
    Qfilter_User = Q(
        empleado_intranet_set__isnull=True,
        date_joined__icontains=datetime.now().date(),
    )
    lusers = mdlauth.User.objects.filter(Qfilter_User)
    for wuser in lusers:
        try:
            wuser.set_password(DEFAULT_PASSWD)
            wuser.save(update_fields=["password", ])
            mdlmet.Empleados.objects.filter(
                usr_usuario__icontains=wuser.username) \
                .update(usuario_intranet=wuser)

            wuser.groups.remove(*wuser.groups.all())
            if wgroups.count() > 0:
                wuser.groups.add(*wgroups)

            obj.printMessage(
                "Password is being updated for '{}'".format(wuser.username))
        except Exception as e:
            obj.printMessage("Error, {}".format(str(e)))
