from django.shortcuts import render, redirect
from django.http import HttpResponse

# no found


def handler404(request):
    return render(request, 'error/handler404.html', status=404)

# internal server error


def handler500(request):
    return render(request, 'error/handler404.html', status=500)
