
const listas = {
	list_salas : [],
	list_plantas : [],
	list_intervals : [1,3,5,10,15].map( x =>{
		return {id:x, value : x + ' segundos',}
	}),
} ;

var refreshCalendar = null ;
var refreshCurrentTime = null ;

const vApp = new Vue({
	delimiters : ['${','}'],
	el: '#vContainer',
	data : {
		fecha : null,
		sala : [],
		planta : null,
		list : listas,
		details : [],
		fields_display : false,
		intervalo : null,
		fields_status :false,
		current_time : null,
		current_room : sdefault,
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		TitleRowStatus(col,row){
			if (!this.details.rows[col]) return '';
			let item = this.details.rows[col][row] ;
			if(!item.allowed){
				return 'No disponible';
			} ;
			return item.reserva_id?'Reservado':'Disponible' ;
		},
		StyleRowStatus(col,row){
			if (!this.details.rows[col]) return '';
			let item = this.details.rows[col][row] ;
			if(!item.allowed){
				return 'box-disabled' ;	
			} ;
			return item.reserva_id?'box-red':'box-green' ;
		},
		getParameters(){
			return {
				fecha : moment(this.fecha).format("Y-MM-DD"),
				sala : this.sala.toString(),
				planta : this.planta,
			} ;
		},
		handleSearch(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_reservas_meetings',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.meetings;
				// console.log(this.details);
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo programacion de reservas.',
					icon:'warning',
				});
			}) ;
		},
		handleCleanFields(){
			this.planta = null ;
			this.sala = [] ;
		},
		getSalas(){
			axios({
				method : 'get',
				url: URI_SALAS,
				params : {status:1},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_salas = response.result;
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de Salas.',
					icon:'warning',
				});
			}) ;
		},
		getPlantas(){
			axios({
				method : 'get',
				url: URI_PLANTAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_plantas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de plantas.',
					icon:'warning',
				});
			}) ;
		},
		handleUptimeTime(){
			let wtime = new Date();
			this.current_time = moment(wtime).format("HH:mm:ss");
			if(/(2|4|6|7)/.test(parseInt(wtime.getHours()))){
				if (moment(wtime).format("mm:ss")=='00:00'){
					window.location.reload();
				}
			};
		},
	},
	watch : {
		'intervalo' : function(wnew,wold){
			clearInterval(refreshCalendar) ;
			refreshCalendar=null ;
			refreshCalendar = setInterval(
				this.handleSearch,(this.intervalo*1000)
			);
		},
	},
	computed : {
		lst_items(){
			// return this.details.filter(row=>{
			// 	try {
			// 		return true;
			// 	} catch (ex){
			// 		return false;
			// 	}
			// 	return ;
			// }) ;
			return ;
		},
		totales(){
			let wtotal =this.details.schedules?this.details.schedules.length:0;
			return {
				filas: wtotal,
			} ;
		},
	},
	mounted(){
		this.getSalas();
		this.getPlantas();
		this.fecha = new Date();
		if(idefault!=0){
			this.intervalo = idefault ;
			refreshCalendar = setInterval(
				this.handleSearch,(this.intervalo*1000)
			);
		} ;
		if(sdefault!=null){
			this.sala.push(sdefault);
			this.fields_status = true;
			this.fields_display = true ;
		} ;
		refreshCurrentTime = setInterval(
			this.handleUptimeTime,1000
		);
	},
}) ;