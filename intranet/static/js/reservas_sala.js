

const listas = {
	list_estados : [],
	list_salas : [],
	list_plantas : [],
	list_time : {
		from:'08:00',
		to:'20:00',
	}
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		sala : null,
		planta :null,
		estado :null,
		list : listas,
		details : [],
		item_sala : [],
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		cleanFields(){
			let witems = ['sala','estado','planta',]
			for(item in witems) {
				this[witems[item]] = null ;
			}
		},
		getParameters(){
			return {
				sala : this.sala,
				estado : this.estado,
				planta : this.planta,
			} ;
		},
		setValues(){
			if(this.item_sala.id){
				vModal.id = this.item_sala.id ;
				vModal.descripcion = this.item_sala.descripcion;
				vModal.estado = this.item_sala.estado.id ;
				vModal.planta = this.item_sala.planta.id ;
				vModal.capacidad = this.item_sala.limite_participantes ;
				vModal.fecha_vigencia = this.item_sala.fecha_vigencia ;
				vModal.hora_inicio = this.item_sala.hora_inicio?this.item_sala.hora_inicio:null ;
				vModal.hora_final = this.item_sala.hora_final?this.item_sala.hora_final:null ;
			} ;
		},
		showDialog() {
			vModal.dialog_status = true;
			vModal.setDefaultValues();
		},
		getPlantas(){
			axios({
				method : 'get',
				url: URI_PLANTAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_plantas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de plantas.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		getSalas(){
			axios({
				method : 'get',
				url: URI_SALAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_salas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de salas.',
					icon:'warning',
				});
			}) ;
		},
		getSearch(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_reservas_sala',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo Salas de reserva.',
					icon:'warning',
				});
			}) ;
		},
		modifyMeta(item){
			this.item_sala= item;
			this.setValues();
			vModal.dialog_status = true ;
		},
		removeMeta(item){
			this.item_sala = item;
			let wmessage  = `Se procederá con el eliminado de ${this.item_sala.descripcion}, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_reserva_sala',
				item : this.item_sala,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_sala = [],
					swal('Aviso',response.result.message,"success");
					this.getSalas();
					this.getSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					let defaultmsg= 'Error, eliminando Sala de Reservas.'
					swal({
						title:wmessage,
						text: err.response.data.result|| defaultmsg,
						icon:'warning',
					});
				}
			}) ;
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
			return ;
		},
		totales(){
			let wnohabilitados = 0 ;
			let whabilitados = 0 ;
			this.lst_items.forEach(function(witem,windex){
				whabilitados += (witem.estado.id==1?1:0);
				wnohabilitados += (witem.estado.id==1?0:1);
			}) ;
			return {
				habilitados: whabilitados,
				nohabilitados: wnohabilitados,
				filas: this.lst_items.length,
			} ;
		},
	},
	mounted(){
		this.getSearch();
		this.getEstados();
		this.getPlantas();
		// this.getSalas();
	},
}) ;

const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		dialog_status : false,
		id : null,
		descripcion :null,
		planta: null,
		estado :null,
		capacidad : 0,
		fecha_vigencia : null,
		hora_final:null,
		hora_inicio:null,
		list : listas,
		range_time: {
			start: listas.list_time.from,
			step: '00:30',
			end: listas.list_time.to,
		},
		errors : [],
	},
	methods : {
		setDefaultValues(){
			if(!this.id){
				this.estado = 1;
				this.hora_inicio = listas.list_time.from;
				this.hora_final = listas.list_time.to;
			} ;
		},
		getParameters(){
			return {
				id : vApp.item_sala.id ? vApp.item_sala.id:null,
				descripcion : this.descripcion,
				planta : this.planta ? this.planta:null,
				estado : this.estado ? this.estado:null,
				capacidad : this.capacidad,
				fecha_vigencia : this.fecha_vigencia? this.fecha_vigencia:null,
				hora_inicio : this.hora_inicio ? this.hora_inicio:null,
				hora_final : this.hora_final ? this.hora_final:null,
				wtype : vApp.item_sala.id ?'update_reserva_sala':'save_information',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.descripcion) {
				swal("Error",'Ingrese descripcion','error');
				return false;
			} ;
			if(!wparameters.planta) {
				swal("Error",'Seleccione Planta / Piso','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			if(!wparameters.capacidad) {
				swal("Error",'Ingrese Nro. Participantes','error');
				return false;
			} ;
			return true;
		},
		cleanFields(){
			let wfields = [
				'descripcion','planta','estado','id',
				'capacidad', 'fecha_vigencia',
				'hora_inicio','hora_final',
			] ;
			let wvalues = [
				null,null,null,null,0,null,null,null,
			] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			if(vApp.item_sala){
				vApp.item_sala= [];
			} ;
		},
		closeDialog() {
			this.cleanFields();
			this.dialog_status = false ;
		},
		cancelDialog() {
			this.closeDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.getSalas();
					vApp.getSearch();
					this.closeDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, procesando guardado de datos sala de reservas.',
					icon:'warning',
				});
			}) ;
		},
		confirmDialog() {
			// if(!this.validatingFields()) return false;
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			let wobject = !e.target?e:e.target;
			this.validatingValue(wobject.name,wobject.value) ;
			return ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
	},
	watch : {
		'descripcion': function(wnew,wold){
			this.validatingValue('descripcion',wnew) ;
		},
		'planta': function(wnew,wold){
			this.validatingValue('planta',wnew) ;
		},
		'sala': function(wnew,wold){
			this.validatingValue('sala',wnew) ;
		},
		'estado': function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
		'capacidad': function(wnew,wold){
			this.validatingValue('capacidad',wnew) ;
		},
		'hora_inicio': function(wnew,wold){
			this.validatingValue('hora_inicio',wnew) ;
		},
		'hora_final': function(wnew,wold){
			this.validatingValue('hora_final',wnew) ;
		},
	},
	computed: {
		areThereErrors(){
			let wfields = [ 
				'descripcion','planta','estado','capacidad',
				'hora_inicio','hora_final',
			] ;
			for(item in wfields){
				this.validatingValue(
					wfields[item],
					this[wfields[item]]
				)
			}
			return this.errors.length>0?true:false;
		},
	},
}) ;