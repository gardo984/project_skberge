

const listas = {
	list_estados : [],
	list_salas : [],
	list_salas_allow : [],
	list_plantas : [],
	list_empleados : [],
	list_days_week : [],
	list_days:[],
	list_recurrence : [],
	list_interval : [],
} ;
const modalExternal = {
	status:false,
	data : {
		nombres : null,
		email : null,
	},
} ;
const modalSchedule = {
	status :false,
	data : {
		sala : [],
		fecha_busqueda : null,
		button_status : false,
		holiday_status : false,
		drag_status :false,
		drag_column : 0,
		details : [],

	},
} ;

const wrequests = { 
	delimiters : ['${','}'],
	name :"wrequests",
	template : '#container-requests',
	data(){
		return {
			descripcion : null,
			fecha_desde : null,
			fecha_hasta : null,
			estado : null,
			check_all:false,
			list : listas,
			details : [],
			item_reserva : [],
			errors : [],
		}
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods: {
		hideMenu(){
			if ($('.middle-leftside').hasClass('expand')){
				// hide the menu bar
				setTimeout(()=>{ 
					$('a.collapse-menu').click();
				},1000)	
			};
		},
		handleCleanFields(){
			let wdate_to = new Date();
			let wdate_from = wdate_to.addDays(wdate_to,-7) ;
			wdate_from = moment(wdate_from).format("Y-MM-D") 
			wdate_to = moment(wdate_to).format("Y-MM-D") 
			let witems = [
				'descripcion','fecha_desde','fecha_hasta',
				'estado',
			] ;
			let wvalues = [null,wdate_from,wdate_to,1,] ;
			for(item in witems) {
				this[witems[item]] = wvalues[item] ;
			} ;
		},
		getParameters(){
			return {
				descripcion : this.descripcion,
				fecha_desde : this.fecha_desde,
				fecha_hasta : this.fecha_hasta,
				estado : this.estado,
			} ;
		},
		handleNew(){
			this.$emit('change-page','/reserva') ;
		},
		handleSearch(){
			if(this.areThereErrors) {
				swal('Aviso','Hay campos por completar.','error')
				return false ;
			} ;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_reservas',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de reservas.',
					icon:'warning',
				});
			}) ;
		},
		modifyReserva(item){
			this.$emit('change-page','/reserva',item) ;
		},
		validatingSelectedItems() {
			try {
				if (this.lst_items_selected.length === 0)
					throw new Error('No hay registros seleccionados que procesar.');
				return true;
			} catch (err) {
				swal({
					title:"Advertencia", 
					text:err.message, 
					icon:"warning"
				});
				return false;
			}
		},
		anularReserva(item){
			if (!this.validatingSelectedItems()) return;

			let wmessage = `Se procederá con la cancelación de los ` +
				`registros seleccionados: ${this.lst_items_selected.length}, ¿Desea continuar?` ;

			swal({
				title : 'Confirmación',
				text:wmessage,
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							let list_id = this.lst_items_selected.map(r => r.id || '') ;
							this.removeProcess(list_id) ;
						break;
				} ;
			}) ;
		},
		removeProcess(p_ids){
			let wparameters = {
				wtype : 'remove_reserva',
				id_list: p_ids,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.item_feriado = [],
					swal('Aviso',response.result.message,"success");
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, eliminando reservas.',
						icon:'warning',
					});
				}
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
	},
	computed: {
		areThereErrors(){
			return this.errors.length>0?true:false;
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
		lst_items_selected() {
			return this.lst_items.filter(i => i.check === true);
		},
	},
	watch : {
		'fecha_desde': function(wnew, wold){
			this.validatingValue('fecha_desde',wnew) ;
		},
		'fecha_hasta': function(wnew, wold){
			this.validatingValue('fecha_hasta',wnew) ;
		},
		'check_all': function (newCheck) {
			this.lst_items.forEach(item => {
				item.check = newCheck;
			});
		},
	},
	mounted(){
		this.handleCleanFields() ;
		this.handleSearch();
		this.hideMenu();
	},
}
const wreserva ={ 
	delimiters : ['${','}'],
	name :"wreserva",
	template : '#container-form',
	data(){
		return {
			sala: null,
			solicitante: null,
			planta:null,
			fecha:null,
			time_from :null,
			time_to:null,
			estado:null,
			participante :{
				id: null,
				value :null,
			},
			list : listas,
			details : [],
			range_time: {
				start: '08:30',
				step: '00:15',
				end: '18:30'
			},
			remote_empleado: {
				loading:false,
				list : [],
			},
			remote_participante: {
				loading:false,
				list : [],
			},
			item_reserva : [],
			errors : [],
			modal : modalSchedule,
			motivo : null,
			capacidad: null,
			modal_external : modalExternal,
			// added at 2020.01.26
			include:true,
			recurrence:false,
			recurrence_day_week:[],
			recurrence_type:null,
			recurrence_from:null,
			recurrence_to:null,
			recurrence_interval:null,
			recurrence_day:null,
		}
	},
	methods : {
		handleUncheckItems(){
			for (c in this.modal.data.details.headers){
				let col = this.modal.data.details.headers[c];
				if (col.id==0) continue;
				for (r in this.modal.data.details.rows[col.id]){
					let row = this.modal.data.details.rows[col.id][r];
					row.check=false;
				}
			}
		},
		handleMouseDown(col,row){
			let details = this.modal.data.details ;
			if (!details.rows[col]) return '';
			let item = details.rows[col][row] ;
			// validation by availability
			if(!item.allowed) return false;
			// validation by column
			let header = this.getHeaderItem(col);
			if(!header.id) return false;
			if(header.allowed==0) return false;

			if (!isMobile()){
				// if its not mobile
				this.handleUncheckItems();
				this.modal.data.drag_status = true ;
				this.modal.data.drag_column = col ;
				// if the chart is not selected
				if (!item.reserva_id) item.check=true ;

				// if the owner of the booking is modifing
				if(this.item_reserva.length==0 || !item.reserva_id) return false ;
				if( this.item_reserva.empleado.id == uidemployer && this.item_reserva.id==item.reserva_id){
					item.check=true ;
				} ;

			}else{
				// if its mobile
				if (this.getSelectSchedules.length>0){
					if(this.modal.data.drag_column!=col) return false;
				} ;

				if (!item.reserva_id) {
					if (item.check){
						item.check=false;
						if (!this.getSelectSchedules.length>0) {
							this.modal.data.drag_column = 0  ;	
						}
					}else{
						item.check=true;
						this.modal.data.drag_column = col ;
					}
				};
			}
		},
		handleMouseMove(col,row){
			if (isMobile()) return false;
			let details = this.modal.data.details ;
			if (!details.rows[col]) return false;
			if(this.modal.data.drag_column!=col) return false;
			// validation by column
			let header = this.getHeaderItem(col);
			if(!header.id) return false;
			if(header.allowed==0) return false;

			let item = details.rows[col][row] ;
			if(!item.allowed) return false;
			if(this.modal.data.drag_status){
				if (!item.reserva_id) item.check=true ;

				if(this.item_reserva.length==0 || !item.reserva_id) return false ;
				if( this.item_reserva.empleado.id == uidemployer && this.item_reserva.id==item.reserva_id){
					item.check=true ;
				} ;
			} ;
		},
		handleMouseUp(col,row){
			this.modal.data.drag_status = false;
		},
		getHeaderItem(col){
			let item = [];
			let outcome = this.modal.data.details.headers.filter(r=>{
				return r.id==col
			});
			if (outcome.length>0){
				item = outcome[0] ;
			}
			return item;
		},
		handleHolidayValidation(){
			let wparameters = {
				wtype : 'get_holiday_validation',
				fecha : moment(this.modal.data.fecha_busqueda).format("Y-MM-DD"),
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if (response.result) {
					this.modal.data.holiday_status= true;
					this.modal.data.button_status= true;
					this.modal.data.details = [];
					return false;
				}
				if(this.modal.data){
					this.modal.data.holiday_status= false;
					this.modal.data.button_status= false;
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, en validacion de feriado.',
						icon:'warning',
					});
				}
			}) ;
		},
		// StyleRowStatus(wposition,witem){
		// 	if(witem.reserva_id) return 'box-busy' ;
		// 	return (wposition % 2==0)?'box-even':'box-odd';
		// },
		TitleRowStatus(col,row){
			let details = this.modal.data.details ;
			if (!details.rows[col]) return '';
			let item = details.rows[col][row] ;
			if(!item.allowed){
				return 'No disponible';
			} ;
			return item.reserva_id?'Reservado':'Disponible' ;
		},
		StyleRowStatus(col,row){
			let details = this.modal.data.details ;
			if (!details.rows[col]) return '';
			let item = details.rows[col][row] ;
			if (item.check){
				return 'box-selected' ;
			};
			if(!item.allowed){
				return 'box-disabled' ;	
			} ;
			return item.reserva_id?'box-red':'box-green' ;
		},
		getSchedulesByDate(){
			let wparameters = {
				wtype : 'get_schedules',
				fecha : moment(this.modal.data.fecha_busqueda).format("Y-MM-DD"),
				sala : this.modal.data.sala.toString(),
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				// this.modal.data.details = response.result.schedules ;
				this.modal.data.details = response.result.meetings ;
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo disponibilidad por sala.',
						icon:'warning',
					});
				}
			}) ;
		},
		handleModalSelectItem(witem){
			if (witem.reserva_id) return false;
			if(witem.check){
				witem.check = false;
			}else{
				witem.check = true;
			} ;
			// this.getSelectedRange ;
		},
		handleModalSearch(){
			if(!this.modal.data.fecha_busqueda){
				swal('Error','Ingrese una fecha valida','warning') ;
				return false ;
			} ;
			// if(!this.modal.data.sala){
			// 	swal('Error','Seleccione Sala','warning') ;
			// 	return false ;
			// } ;
			this.getSchedulesByDate();
		},
		setRangeTimeSelection(){
			let wrange = this.getRangeTimeSelection();
			this.time_from = wrange.time_from ;
			this.time_to = wrange.time_to ;
			this.fecha = this.modal.data.fecha_busqueda;
			this.sala = wrange.sala.id;
			this.capacidad = wrange.sala.limite_participantes || 0;
			this.modalCleanFields();
		},
		getRangeTimeSelection(){
			let wresult = this.getSelectSchedules;
			let wfrom = wresult[0].time_value.split("-")[0] ;
			let wto = wresult[wresult.length-1].time_value.split("-")[1] ;
			let wsala = this.modal.data.drag_column ;
			let lstsala = this.list.list_salas_allow.filter(row=> { 
				return row.id==wsala ;
			}) ;
			return {
				time_value : `${wfrom} a ${wto}`,
				time_from : wfrom.trim(),
				time_to : wto.trim(),
				fecha : moment(this.modal.data.fecha_busqueda).format("DD/MM/Y"),
				sala : lstsala.length>0? lstsala[0]:null,
			};
		},
		validateRangeSelection(wlist){
			for(x in wlist){
				if(x==wlist.length-1) return true;
				let vcurrent = wlist[x] ;
				let vnext = wlist[parseInt(x)+1];
				let values = vcurrent.time_value.split('-');
				let values_next= vnext.time_value.split('-');
				let vfrom = values[1].trim().replace(':','') ;
				let vto =  values_next[0].trim().replace(':','');
				if(vfrom!=vto) return false;
			} ;
			return true ;
		},
		handleModalSubmit(){
			if (!this.getSelectSchedules.length>0){
				swal('Aviso','No hay filas seleccionadas a procesar.','error')
				return false;
			} ;
			// console.log(this.getSelectSchedules);
			if(!this.validateRangeSelection(this.getSelectSchedules)){
				swal('Aviso','Rango seleccionado no paso validacion.','error');
				return false;
			}
			let wrange = this.getRangeTimeSelection();
			let wsala = wrange.sala!=null? wrange.sala.descripcion:'';
			swal({
				title:'Confirmación',
				text:`Se programará la reserva en '${wsala}' de ${wrange.time_value} para la fecha ${wrange.fecha}, ¿Desea Continuar?`,
				buttons: {
					yes:'Si',
					no : 'No',
				},
				icon : 'info',
			}).then(rsp=> {
				switch(rsp){
					case 'yes':
							this.modal.status =false ;
							this.setRangeTimeSelection();
							// added at 2020.01.26
							this.handleAddGuestAsParticipant();
						break ;
				} ;
			})
		},
		modalCleanFields(){
			this.modal.data.sala=[] ;
			this.modal.data.fecha_busqueda=null ;
			this.modal.data.details=[] ;
			this.modal.data.drag_column = 0 ;
			this.modal.data.drag_status = false ;
		},
		handleModalClose(){
			this.modal.status = false ;
			this.modalCleanFields();
		},
		handleAvailability(date_value){
			if(date_value){
				this.modal.status = true ;
				this.modal.data.fecha_busqueda = this.fecha;
				this.$emit('get-salas-allowed') ;
				// let outcome = this.list.list_salas.filter(row=>{
				// 	return row.id == this.sala ;
				// });
				// if (outcome.length>0){
				// 	this.modal.data.sala = [outcome[0].id] ;
				// }
				// this.modal.data.sala = this.sala?;
				// this.getSchedulesByDate();
			} ;
		},
		handleModalSubmitExternal(){
			let wparameters = Object.assign({},{
				wtype:'add_external_member',
				nombres: this.modal_external.data.nombres,
				email: this.modal_external.data.email,
			}) ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.$emit('refresh-employees') ;
					this.handleModalCloseExternal();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let response = err.response ;
				let wmessage = response.status+ ' ' +response.statusText ;
				let defaultmsg = 'Error, procesando agregado de participante externo.';
				swal({
					title:wmessage,
					text: response.data.result|| defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleModalCloseExternal(){
			this.modal_external.status=false;
			cleanObject(this.modal_external.data) ;
		},
		handleAddExternalMember(){
			this.modal_external.status=true;
		},
		// added at 2020.01.27
		applicantExist(wid){
			for(item in this.lst_items){
				let wobj = this.lst_items[item] ;
				if (!wobj.visible) continue ;
				if(wobj.empleado.id == wid){
					return true ;
				} ;
			} ;
			return false;
		},
		handleAddParticipante(){
			if (!this.participante.id){
				swal('Error','Ingrese participante a agregar.','error') ;
				return false;
			} ;
			let wcapacidad = this.capacidad||0;
			if(this.lst_items.length>=wcapacidad){
				swal('Error','Capacidad de participantes superado.','error') ;
				return false;
			} ;
			if(this.participante.id){
				// added at 2020.01.27
				if (this.applicantExist(this.participante.id)){
					swal('Error','Participante ya se encuentra agregado.','error') ;
					return false;
				}

				let witem = this.participante.id ;
				let wapplicant = this.list.list_empleados.filter(row=> {
					return row.id ==  witem ;
				}) ;
				if (wapplicant.length>0) {
					this.details.push({
						check : false,
						empleado : {
							id : wapplicant[0].id,
							fullname : wapplicant[0].value,
							email_empresarial : wapplicant[0].email_empresarial,
						},
						visible : true,
					}) ;
					this.participante.id = null ;
					this.participante.value = null ;
				}
			} ;
		},
		handleRemoveParticipante(item){
			if(item){
				let wmessage = `Se procederá con la eliminación de ${item.empleado.fullname}, ¿Desea continuar?` ;
				swal({
					title : 'Confirmación',
					text : wmessage,
					buttons : {
						yes: 'Si',
						no: 'No',
					},
					icon : 'info',
				}).then(rsp=>{
					switch(rsp){
						case 'yes' :
								this.lst_items.forEach((wobj,windex)=> {
									if(wobj.empleado.id == item.empleado.id){
										wobj.visible = false ;
										return ;
									} ;
								}) ;
							break;
					}
				}) ;
			} ;
		},
		selectParticipante(item){
			this.participante.id = item.id ;
			this.participante.value = item.value ;
		},
		searchParticipante(query,cb){
			if (query !== '') {
	            this.remote_participante.list = this.list.list_empleados.filter(item => {
	              return item.value.toLowerCase()
	                	.indexOf(query.toLowerCase()) > -1;
	            }) ;
	        } else {
	         	this.remote_participante.list = [];
	        };
	        cb(this.remote_participante.list);
		},
		searchParticipante2(query){
			 if (query !== '') {
		          this.remote_participante.loading = true;
		          setTimeout(() => {
		            this.remote_participante.loading = false;
		            this.remote_participante.list = this.list.list_empleados.filter(item => {
		              return item.value.toLowerCase()
		                	.indexOf(query.toLowerCase()) > -1;
		            });
		          }, 200);
	        } else {
	         	 this.remote_participante.list = [];
	        }
		},
		searchEmpleado(query){
			 if (query !== '') {
		          this.remote_empleado.loading = true;
		          setTimeout(() => {
		            this.remote_empleado.loading = false;
		            this.remote_empleado.list = this.list.list_empleados.filter(item => {
		              return item.value.toLowerCase()
		                	.indexOf(query.toLowerCase()) > -1;
		            });
		          }, 200);
	        } else {
	         	 this.remote_empleado.list = [];
	        }
		},
		getParameters(){
			return  {
				 solicitante : this.solicitante?this.solicitante:null,
				 sala : this.sala?this.sala:null,
				 planta : this.planta?this.planta:null,
				 fecha : this.fecha?moment(this.fecha).format('Y-MM-D'):null,
				 time_from : this.time_from?this.time_from:null,
				 time_to : this.time_to?this.time_to:null,
				 estado : this.estado,
				 id: this.item_reserva.id ?this.item_reserva.id:null,
				 participantes : this.lst_items.length>0 ?this.lst_items:[],
				 motivo : this.motivo,
				 include : this.include?true:false,
				 recurrence:this.recurrence?true:false,
				 recurrence_day_week:this.recurrence_day_week.length>0?this.recurrence_day_week:null,
				 recurrence_type:this.recurrence_type?this.recurrence_type:null,
				 recurrence_from:this.recurrence_from?moment(this.recurrence_from).format('Y-MM-D'):null,
				 recurrence_to:this.recurrence_to?moment(this.recurrence_to).format('Y-MM-D'):null,
				 recurrence_interval:this.recurrence_interval,
				 recurrence_day:this.recurrence_day,
				 wtype : this.item_reserva.id ?'update_reserva':'save_information',
			} ;
		},
		validatingFields(){
			let wparameters = this.getParameters();
			if(!wparameters.solicitante) {
				swal("Error",'Ingrese solicitante','error');
				return false;
			} ;
			if(!wparameters.sala) {
				swal("Error",'Ingrese Sala','error');
				return false;
			} ;
			if(!wparameters.planta) {
				swal("Error",'Ingrese Planta','error');
				return false;
			} ;
			if(!wparameters.fecha) {
				swal("Error",'Ingrese Fecha','error');
				return false;
			} ;
			if(!wparameters.time_from) {
				swal("Error",'Ingrese horario desde.','error');
				return false;
			} ;
			if(!wparameters.time_to) {
				swal("Error",'Ingrese horario hasta.','error');
				return false;
			} ;
			if(!wparameters.estado) {
				swal("Error",'Seleccione Estado','error');
				return false;
			} ;
			return true;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.$emit('close-dialog') ;
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, procesando guardado de datos en reservas.',
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			if(this.lst_items.length==0){
				swal('Aviso','No hay participantes en reserva.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleCancelOperation(){
			let wmessage  = `Se cancelaran los cambios ingresados, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
				icon:'warning',
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.$emit('close-dialog') ;
						break;
				} ;
			}) ;
		},
		setDefaultValues(){
			this.estado = 1;
			this.fecha = new Date();
			this.remote_empleado.list = this.list.list_empleados.filter(item => {
              	return item.id == uidemployer ;
            }) ;
			this.solicitante = uidemployer ;
		},
		setValues(){
			if(this.item_reserva){
				this.estado = this.item_reserva.estado.id;
				this.sala = this.item_reserva.sala.id;
				this.planta = this.item_reserva.planta.id;
				let wfrom =` ${this.item_reserva.fecha_evento} ${this.item_reserva.hora_inicio}`;
				this.time_from = moment(wfrom,"Y-MM-D H:mm:ss").format("HH:mm");
				let wto =` ${this.item_reserva.fecha_evento} ${this.item_reserva.hora_final}`;
				this.time_to = moment(wto,"Y-MM-D H:mm:ss").format("HH:mm");
				this.fecha = this.item_reserva.fecha_evento;
	            this.remote_empleado.list = [Object.assign({},{
            		id: this.item_reserva.empleado.id,
            		value: this.item_reserva.empleado.fullname,
            	})];
	            this.solicitante = this.item_reserva.empleado.id;
	            this.motivo = this.item_reserva.motivo;
	            this.capacidad = this.item_reserva.sala.limite_participantes;
	            this.include = this.item_reserva.include==1?true:false ;
	            // added at 2020.02.02
	            this.recurrence = this.item_reserva.recurrence==1?true:false ;
	            if (this.recurrence==1){
	            	if (this.item_reserva.recurrence_from!=null){
	            		let wfrom = this.item_reserva.recurrence_from ;
	            		this.recurrence_from = moment(wfrom,"Y-MM-D").format("Y-MM-D");
	            	};
	            	if (this.item_reserva.recurrence_to!=null){
	            		let wto = this.item_reserva.recurrence_to ;
	            		this.recurrence_to = moment(wto,"Y-MM-D").format("Y-MM-D");
	            	};
	            	this.recurrence_type = this.item_reserva.recurrence_type?this.item_reserva.recurrence_type.id:null;
	            	this.recurrence_interval = this.item_reserva.recurrence_interval ||null;
	            	this.recurrence_day = this.item_reserva.recurrence_day || null
	            	if (this.item_reserva.recurrence_day_week.length>0){
	            		let items = this.item_reserva.recurrence_day_week.split(",");
	            		this.recurrence_day_week = items.map(
	            			(x)=> parseInt(x)
	            		);
	            	}
	            }
			} ;
		},
		getReserva(wid){
			let wparameters = {
				id:wid, 
				wtype : 'get_reserva_item',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers : axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.item_reserva = response.result[0];
				this.details = this.item_reserva.participantes ;
				this.setValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo reserva.',
					icon:'warning',
				});
			}) ;	
		},
		validatingStatus(){
			let wparams = this.$route.params ;
			if(Object.keys(wparams).length>0){
				this.getReserva(wparams.uid);
			}else{
				this.setDefaultValues();
			} ;
			// this.$refs.sala.focus() ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		},
		// added at 2020.01.26
		handleAddGuestAsParticipant(){
			if (this.include){
				if (this.applicantExist(this.solicitante)){
					return false;
				}
				Object.assign(this.participante,{
					id: this.solicitante,
				}) ;
				this.handleAddParticipante();
			} ;
		},
	},
	watch : {
		'solicitante' : function(wnew,wold){
			this.validatingValue('solicitante',wnew) ;
		},
		'sala':function(wnew,wold){
			this.validatingValue('sala',wnew) ;
			let item = this.list.list_salas.filter(row=>{
				return row.id == wnew ;
			}) ;
			if(item.length>0) {
				this.planta = item[0].planta.id ;
				return ; 
			}
			// this.planta = null ;
		},
		'planta' : function(wnew,wold){
			this.validatingValue('planta',wnew) ;
		},
		'fecha' : function(wnew,wold){
			this.validatingValue('fecha',wnew) ;
		},
		'time_from' : function(wnew,wold){
			this.validatingValue('time_from',wnew) ;
		},
		'time_to' : function(wnew,wold){
			this.validatingValue('time_to',wnew) ;
		},
		'estado' : function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
		'motivo' : function(wnew,wold){
			this.validatingValue('motivo',wnew) ;
		},
		'modal.data.fecha_busqueda': function(wnew,wold){
			if (wnew==null) return false;
			this.handleHolidayValidation()
		},
		// added at 2020.01.26
		'include':function(wnew,wold){
			if (wnew){
				if (this.sala==null) return false;
				this.handleAddGuestAsParticipant();
			}else{
				let item = this.lst_items.filter((x)=>{
					return x.empleado.id == this.solicitante ;
				}) ;
				if (item.length>0){
					this.handleRemoveParticipante(item[0]);
				}
			}
		},
		'recurrence_type':function(wnew,wold){
			if (this.recurrence_interval==null){
				this.recurrence_interval = 1;
			} ;
			if (this.recurrence_type!=2){
				this.recurrence_day_week=[];
			} ;
			if (this.recurrence_type!=3){
				this.recurrence_day=null;
			} ;
			if (this.recurrence){
				this.validatingValue('recurrence_type',wnew) ;
			};

		},
		'recurrence_from':function(wnew,wold){
			if (this.recurrence){
				this.validatingValue('recurrence_from',wnew) ;
			}
		},
		'recurrence_to':function(wnew,wold){
			if (this.recurrence){
				this.validatingValue('recurrence_to',wnew) ;
			}
		},
		'recurrence_interval':function(wnew,wold){
			if (this.recurrence){
				this.validatingValue('recurrence_interval',wnew) ;
			}
		},
		'recurrence_day':function(wnew,wold){
			if (this.recurrence){
				// if recurrence's type is monthly
				if (this.recurrence_type==3){
					this.validatingValue('recurrence_day',wnew) ;
				} ;
			}
		},
		'recurrence_day_week':function(wnew,wold){
			if (this.recurrence){
				// if recurrence's type is weekly
				if (this.recurrence_type==2){
					this.validatingValue('recurrence_day_week',wnew) ;
				} ;
			}
		},
	},
	computed : {
		recurrence_status(){
			if (this.item_reserva){
				if (this.item_reserva.recurrence==true){
					return true ;
				}
			}
			return false;
		},
		getSala(){
			let wsala = this.sala ;
			let item = this.list.list_salas.filter((x)=> {
				return x.id == wsala ;
			}) ;
			if (item.length>0){
				return item[0].descripcion
			}
			return '' ;
		},
		getPlanta(){
			let wplanta = this.planta ;
			let item = this.list.list_plantas.filter((x)=> {
				return x.id == wplanta ;
			}) ;
			if (item.length>0){
				return item[0].descripcion
			}
			return '' ;
		},
		getSummary(){
			if (this.getSala == '') return '' ;
			let wdate = moment(this.fecha).format('D/MM/Y')
			let wmessage = `Sala ${this.getSala}, ${this.getPlanta} ` +
				`de ${this.time_from} a ${this.time_to} a la Fecha de ` +
				`${wdate}` ;
			return wmessage
		},
		getSelectedRange(){
			let wlist = []
			let wsource = this.modal.data.details ;
			this.modal.data.details.forEach(function(wele,windex){
				if (wele.check) wlist.push({ p:windex, d:wele,}) ;
				if(windex==wsource.length-1){
					if (wlist.length==0) return false ;
					let f = wlist[0].p ;
					let l = wlist[wlist.length-1].p ;
					let wrange = wsource.slice(f,l) ;
					if (wrange.filter(row=>{ return row.reserva_id }).length>0){
						swal('Alerta','Seleccion de rango no valido.','warning')
						return false;
					} ;
					wrange.forEach(function(x,y){ x.check = true ; }) ;
				} ;
			}) ;
		},
		getSelectSchedules(){
			let ncolumn = this.modal.data.drag_column ;
			if (ncolumn==0) return [];
			return this.modal.data.details.rows[ncolumn].filter(row=>{
				return row.check == true;
			}) ;
		},
		areThereErrors(){
			let wfields = [
				'solicitante','sala','planta',
				'fecha','time_from','time_to',
				'estado','motivo',
			];
			let wfields_recurrence = [
				"recurrence_type","recurrence_interval",
				"recurrence_from","recurrence_to",
			];
			if (this.recurrence){
				wfields.push(...wfields_recurrence);
				switch(this.recurrence_type){
					case 2:
							wfields.push("recurrence_day_week");
						break;
					case 3:
							wfields.push("recurrence_day");
						break;
				}
			} ;

			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return row.visible==true ;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
		button_det_status(){
			let wcapacidad = this.capacidad||0;
			if(wcapacidad>this.lst_items.length){
				return false ;
			}else{
				return true;
			} ;
		},
	},
	created(){
	},
	mounted(){
		this.validatingStatus();
	},
}

const routes =[
	{
		path:'/',component: wrequests,
	},
	{ 
		path:'/reserva',component: wreserva,
	},
	{ 
		path:'/reserva/:uid',
		component: wreserva,
		props : true,
	},
]  ;
const router = new VueRouter({
	routes: routes
})
const vcontainer = new Vue({
	delimiters : ['${','}'],
	el : "#vTabs",
	data : {
		list : listas,
	},
	router,
	methods : {
		getEmpleados(){
			axios({
				method : 'get',
				url: URI_EMPLEADOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_empleados = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de Empleados.',
						icon:'warning',
					});
				}
			}) ;
		},
		getPlantas(){
			axios({
				method : 'get',
				url: URI_PLANTAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_plantas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de plantas.',
					icon:'warning',
				});
			}) ;
		},
		// added at 2020.02.01
		getRecurrenceWeekDays(){
			axios({
				method : 'get',
				url: URI_WEEKDAYS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_days_week = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de dias de semana.',
						icon:'warning',
					});
				}
			}) ;
		},
		getRecurrenceType(){
			axios({
				method : 'get',
				url: URI_RECURRENCE,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_recurrence = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de tipo recurrencias.',
						icon:'warning',
					});
				}
			}) ;
		},
		getDays(){
			let limit = 31 ;
			for(x=1;x<=limit;x++){
				let item = {
					id:x,value: x,
				};
				this.list.list_days.push(item);
			};
		},
		getRecurrenceIntervals(){
			let limit = 3 ;
			for(x=1;x<=limit;x++){
				let item = {
					id:x,value: x,
				};
				this.list.list_interval.push(item);
			};
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de estados.',
						icon:'warning',
					});
				}
			}) ;
		},
		getSalasByGrants(){
			axios({
				method : 'get',
				url: URI_SALAS,
				params : {
					status:1, grants:1,
				},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_salas_allow = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de salas by grants.',
					icon:'warning',
				});
			}) ;
		},
		getSalas(){
			axios({
				method : 'get',
				url: URI_SALAS,
				params : {status:1},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_salas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de salas.',
					icon:'warning',
				});
			}) ;
		},
		onChangePage(wpage,wparams){
			let wurl = wpage ;
			if (wparams){
				wurl = `${wurl}/${wparams.id}`
			};
			this.$router.push({
				path:wurl,
				params: wparams,
			});
		},
		onCloseDialog(){
			this.$router.push({path:'/'});	
		},
	},
	mounted(){
		this.getEstados();
		this.getSalas();
		this.getPlantas();
		this.getEmpleados();
		// added at 2020.02.01
		this.getRecurrenceType();
		this.getRecurrenceWeekDays();
		this.getRecurrenceIntervals();
		this.getDays();
	},
}) ;
