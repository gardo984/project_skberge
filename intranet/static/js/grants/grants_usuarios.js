
const listas = {
	list_estados : [],
	list_concesionarios : [],
	list_paises : [],
	list_perfiles : [],
	list_accesos : [],
	list_marcas : [],
	list_salas : [],
	list_empleados: [],
	list_confirmacion : [
		{id:1,value:'SI',},
		{id:2,value:'NO',},
	],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		descripcion:null,
		estado :null,
		list : listas,
		details : [],
		item : [],
		visible_status:false,
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		handleClean(){
			let wfields = [ 
				'descripcion','estado',
			] ;
			let wvalues = [ null,null, ] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			this.list.page.page_current = 1;
		},
		getParameters(){
			return {
				descripcion : this.descripcion || null,
				estado : this.estado || null,
				npage : this.list.page.page_current,
			} ;
		},
		getConcesionarios(){
			axios({
				method : 'get',
				url: URI_CONCESIONARIOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_concesionarios = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de concesionarios.',
					icon:'warning',
				});
			}) ;
		},
		getMarcas(){
			axios({
				method : 'get',
				url: URI_MARCAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_marcas = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de marcas.',
					icon:'warning',
				});
			}) ;
		},
		getEmpleados(){
			axios({
				method : 'get',
				url: URI_EMPLEADOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_empleados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de empleados.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		
		removeProcess(){
			let wparameters = { 
				id: this.item.id,
			} ;
			axios({
				method : 'post',
				url: URI_REMOVE,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.item = [],
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response  = err.response ;
				let defaultmsg = 'Error, procesando eliminacion usuario.';
				swal({
					title:wmessage,
					text: response.data?response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleModify(item){
			this.item = item ;
			this.visible_status = true ;
			vModal.dialog_status = true ;
			vModal.setDefaultValues();
		},
		handleRemove(item){
			this.item = item;
			let wmessage  = `Se procederá con el eliminado de usuario : ${this.item.fullname}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleSearch(){
			let wparameters = this.getParameters()
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows ;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de usuarios.',
						icon:'warning',
					});
				}
			}) 
		},
		handleAddItem(){
			vModal.dialog_status=true ;
			this.visible_status=true;
			vModal.form.estado=1;
			vModal.form.all_bookings=2;
			vModal.form.all_salas=2;
			vModal.form.is_new=1;
			vModal.getUserGrants();
			// vModal.handleProfilesList();
		},
		
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			});
		},
		totales(){
			return {
				filas: this.lst_items.length,
			} ;
		},
	},
	mounted(){
		// this.getMarcas();
		// this.getConcesionarios();
		this.getEmpleados();
		this.getEstados();
		this.handleSearch();
	},
}) ;

const fields = {
	id:null,
	username : null,
	password:null,
	first_name : null,
	last_name:null,
	dni:null,
	email:null,
	estado:null,
	// requiere_aprobacion:null,
	perfiles : [],
	accesos : [],
	concesionarios:[],
	marcas:[],
	salas:[],
	empleado :{
		id: null,
		value :null,
	},
	all_bookings:null,
	all_salas : null,
	is_new : null,
}
const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		dialog_status : false,
		form : fields,
		list : listas,
		errors:[],
		remote_empleado: {
			loading:false,
			list : [],
		},
	},
	methods : {
		searchParticipante(query,cb){
			if (query !== '') {
	            this.remote_empleado.list = this.list.list_empleados.filter(item => {
	              return item.value.toLowerCase()
	                	.indexOf(query.toLowerCase()) > -1;
	            }) ;
	        } else {
	         	this.remote_empleado.list = [];
	        };
	        cb(this.remote_empleado.list);
		},
		selectParticipante(item){
			this.form.empleado.id = item.id ;
			this.form.empleado.value = item.value ;
		},
		handleSalasFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
		handleProfilesFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        handleGrantsFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        handleConcesionarioFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        handleMarcaFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
		setDefaultValues(){
			if(vApp.item.id){
				this.form.id = vApp.item.id ;
				this.form.username = vApp.item.username ;
				this.form.first_name = vApp.item.first_name ;
				this.form.last_name = vApp.item.last_name;
				this.form.estado = vApp.item.is_active?1:2 ;
				this.form.all_bookings = vApp.item.userextension?vApp.item.userextension.check_all:2 ;
				this.form.all_salas = vApp.item.userextension?vApp.item.userextension.salas_all:2 ;
				let wis_new = vApp.item.userextension?vApp.item.userextension.is_new:0 ;
				this.form.is_new = wis_new===0?2:wis_new;
				this.form.email = vApp.item.email;
				this.form.dni = vApp.item.userextension?vApp.item.userextension.dni:null;
				let wempleado = vApp.item.empleado_intranet_set?vApp.item.empleado_intranet_set.id:null;
				if(wempleado!=null){
					this.remote_empleado.list = this.list.list_empleados.filter(item => {
		              	return item.id == wempleado ;
		            }) ;
		            let x = this.remote_empleado.list;
		           	if (x.length>0){
						Object.assign(this.form.empleado,{
							id:x[0].id, value:x[0].value,
						});
		           	}
				}
				// this.form.requiere_aprobacion = vApp.item.userextension?vApp.item.userextension.requiere_aprobacion:null;
				this.getUserGrants()
			}
		},
		setGrantValues(){
			// let wlist = [
			// 	this.list.list_accesos, this.list.list_perfiles,
			// 	this.list.list_marcas, this.list.list_concesionarios,
			// ]
			// let wsource = [
			// 	vModal.form.accesos, vModal.form.perfiles,
			// 	vModal.form.marcas, vModal.form.concesionarios,
			// ]
			let wlist = [
				this.list.list_accesos, this.list.list_perfiles,
				this.list.list_salas,
			]
			let wsource = [
				vModal.form.accesos, vModal.form.perfiles,
				vModal.form.salas,
			]
			for (item in wlist){
				wlist[item].forEach((obj,index)=>{
					if(obj.user_id!=null){
						wsource[item].push(obj.id) ;
					}
				}) ;	
			}
		},
		getUserGrants(){
			axios({
				method : 'get',
				url: URI_GRANTS,
				params : {uid:this.form.username||null,},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_perfiles = response.result.perfiles;
				this.list.list_accesos = response.result.permisos;
				this.list.list_salas = response.result.salas;
				// this.list.list_marcas = response.result.marcas;
				// this.list.list_concesionarios = response.result.concesionarios;
				this.setGrantValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo datos de usuario.',
					icon:'warning',
				});
			}) ;
		},
		getParameters(){
			return {
				fields : {
					id : this.form.id,
					clave : this.form.password,
					usuario : this.form.username,
					first_name : this.form.first_name,
					last_name : this.form.last_name,
					is_active : this.form.estado==1?1:0,
					email : this.form.email,
					dni : this.form.dni,
					empleado : this.form.empleado.id,
					check_all : this.form.all_bookings,
					salas_all : this.form.all_salas,
					is_new : this.form.is_new==2?0:this.form.is_new,
					// requiere_aprobacion : this.form.requiere_aprobacion,
				},
				permissions: this.form.accesos,
				groups: this.form.perfiles,
				salas: this.form.salas,
				// marcas: this.form.marcas,
				// concesionarios: this.form.concesionarios,
			} ;
		},
		cleanFields(){
			cleanObject(vModal.form) ;
			// ['perfiles','accesos','marcas','concesionarios'].forEach((wobj,windex)=>{
			['perfiles','accesos',].forEach((wobj,windex)=>{
				vModal.form[wobj] = [];
			});
			this.errors = [];
			let ltransfer =  [
				this.$refs.elt_perfiles, this.$refs.elt_permisos,
				// this.$refs.elt_concesionarios, 
				// this.$refs.elt_marcas,
			]
			for(x in ltransfer){
				ltransfer[x].clearQuery("left");
				ltransfer[x].clearQuery("right");
			}
			if(vApp.item){
				vApp.item= [];
			} ;
		},
		handleCloseDialog() {
			this.dialog_status = false ;
			vApp.visible_status = false ;
			this.cleanFields();
		},
		handleCancelDialog() {
			this.handleCloseDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			let wpath = !this.form.id?URI_APPEND:URI_UPDATE;
			axios({
				method : 'post',
				url: wpath,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.handleSearch();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response = err.response ;
				let defaultmsg = 'Error, procesando guardado de datos sucursal.';
				swal({
					title:wmessage,
					text: response.data? response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog() {
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation();
						break;
				} ;
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	computed:{
		areThereErrors(){
			// let wfields = [ 
			// 	'first_name','last_name','dni',
			// 	'email','estado','requiere_aprobacion',
			// ]
			let wfields = [ 
				'first_name','last_name','dni',
				'email','estado','all_bookings',
				'all_salas','is_new',
			]
			for(item in wfields){
				this.validatingValue(
					wfields[item],
					this.form[wfields[item]]
				)
			}
			return this.errors.length>0?true:false;
		},
	},
	watch:{
		'form.first_name' : function(wnew,wold){
			this.validatingValue('first_name',wnew) ;
		},
		'form.last_name' : function(wnew,wold){
			this.validatingValue('last_name',wnew) ;
		},
		'form.dni' : function(wnew,wold){
			this.validatingValue('dni',wnew) ;
		},
		'form.empleado.value' : function(wnew,wold){
			let wvalue = wnew==null?'':wnew;
			if(wvalue==''){
				 cleanObject(this.form.empleado);
			} ;
		},
		// 'form.requiere_aprobacion' : function(wnew,wold){
		// 	this.validatingValue('requiere_aprobacion',wnew) ;
		// },
		'form.email' : function(wnew,wold){
			this.validatingValue('email',wnew) ;
		},
		'form.estado' : function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
		'form.all_bookings' : function(wnew,wold){
			this.validatingValue('all_bookings',wnew) ;
		},
		'form.all_salas' : function(wnew,wold){
			this.validatingValue('all_salas',wnew) ;
		},
		'form.is_new' : function(wnew,wold){
			this.validatingValue('is_new',wnew) ;
		},
	},
}) ;