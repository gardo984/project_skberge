
var SIsession= null ;
var STexpiration = null ;

function sessionRefresh(){
	axios({
		method : 'post',
		url:URI_REFRESH,
		data : {},
		responseType : 'json',
		headers: axiosPostHeaders,
	}).then(rsp=> {
		let response  = rsp.data ;
		$('#h_deadline').val(response.result.date) ;
	}).catch( err=> {
		console.log(err.response.data) ;
		let wmessage = err.response.status+ ' ' +err.response.statusText ;
		let defaultmsg = 'Error, procesando renovacion de sesion.';
		swal({
			title:wmessage,
			text: defaultmsg,
			icon:'warning',
		});
	}) ;
} ;

$(document).ready(function(){
	$('.top-side .layer-sandwich span i').on("click",function(e){
		e.preventDefault();
		$('.middle-leftside, .middle-rightside').toggleClass('appear')
	}) ;
	$('.layer-menu>li>a').on("click",function(e){
		e.preventDefault();
		// event to expand navigator
		if ($('.middle-leftside').hasClass('collapse')){
			$('a.collapse-menu').click();
		};
		// validation if the current select item is the same
		let witem = $(e.currentTarget).find('label').text()
		let wstatus = false;
		$.each($('.layer-menu>li.active'),function(windex,wele){
			if ($(wele).find('a label:first').text() == witem){
				wstatus = true ; 
				$(wele).removeClass('active');
				return false;
			}
			$(wele).removeClass('active');
		});
		// if the element clicked is different that the current one
		if (!wstatus){
			let wparent = $(e.currentTarget.parentElement) ;
			wparent.toggleClass('active') ;
		}
	}) ;

	$('.layer-user span.user-img').on("click",function(e){
		$('.layer-user-details').toggleClass("appear");
	}) ;
	$('a.collapse-menu').on('click',function(e){
		e.preventDefault();
		$.each($('.layer-menu>li.active'),function(windex,wele){
			$(wele).removeClass('active');
		});
		$('.middle-leftside, .middle-rightside').toggleClass('expand') ;
		$('.middle-leftside, .middle-rightside').toggleClass('collapse') ;
	}) ;

	SIsession = setInterval(getIntervalLeft,1000) ;

	// extend session event
	$('.session-body footer .btn-primary').on('click',function(e){
		e.preventDefault();
		displayExpirationAlert();
		sessionRefresh()
	}) ;
}) ;