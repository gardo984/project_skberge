

const vApp = new Vue({
	el : '#container-user',
	data : {
		loading : false,
		email : null,
		errors : [],
	},
	methods : {
		getParameters(){
			return {
				email:this.email,
			} ;
		},
		handleKeyup(e){
			if(e.target){
				this.validatingValue(e.target.name,e.target.value) ;
			} ;
		},
		handleClick(){
			if(this.areThereErrors){
				swal('Error','Hay campos por completar.','error') ;
				return false;
			}
			let wparameters = this.getParameters();
			Object.assign(wparameters,{
				wtype : 'send_password_reset',
			}) ;
			this.loading=true;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.loading=false;
					swal('',response.result,"success");
				}
			}).catch( err=> {
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					this.loading=false;
					swal({
						title:wmessage,
						text: err.response.data.result,
						icon:'warning',
					});
				}
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
	},
	computed : {
		button_status(){
			if (this.email==null || this.email==''){
				return true;
			};
			if (this.loading){
				return true;
			} ;
			return false ;
		},
		areThereErrors(){
			let wfields = ['email'] ;
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
	},
})