from django.shortcuts import render
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import (
    JsonResponse,
    Http404,
)
from apps.metas import (
    models as mdlmetas
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srlzcat,
)
from apps.reservas import (
    models as mdl,
    serializers as srlz,
)
from django.db.models import (
    Value, Case, When, CharField, Q, F, Count,
    Prefetch, IntegerField, CharField,
)

from apps.connections.config import (
    MYSQLConnection,
)
from apps.common.utils import Validators
import json

# Create your views here.

from django.utils.decorators import method_decorator as md
from django.contrib.auth.decorators import (
    login_required,
    permission_required as perm,
)
from django.core.exceptions import PermissionDenied
from apps.reservas.utils import (
    getScheduleList, getScheduleListBySala,
    sendMailBookingConfirmation,
    mailBookingGetReserva,
    getTimeInRange,
    registerRecurrenceEvents,
)
from datetime import datetime, timedelta
import time

from apps.mail.views import ICalendarGenerator, SendMailer

# added at 2020.01.18
import requests
import msal

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


@md(login_required, name='dispatch')
class ViewReservasSalas(View):
    template_name = 'reservas/reservas_salas.html'
    template_header = 'SKBerge | Salas de Reservación'
    template_title = 'Salas de Reservación'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewReservasSalas, self).dispatch(*args, **kwargs)

    @md(perm('catalogos.add_sala', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @md(perm('catalogos.add_sala', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_reservas_sala":
                    return self.getSalasSearch(request)
                if wtype == "save_information":
                    return self.saveInformation(request)
                if wtype == "update_reserva_sala":
                    return self.updateSalas(request)
                if wtype == "remove_reserva_sala":
                    return self.removeSala(request)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('catalogos.add_sala', raise_exception=True),)
    def getSalasSearch(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            # connection = MYSQLConnection()
            wdata = json.loads(request.body.decode("utf8"))
            wfilters = {}
            wsala = wdata.get('sala') or ''
            wplanta = wdata.get('planta') or ''
            westado = wdata.get('estado') or ''

            if wsala != '':
                wfilters['descripcion__icontains'] = wsala
            if westado != '':
                wfilters['estado'] = mdlmetas.Estados.objects.get(
                    id=int(westado))
            if wplanta != '':
                wfilters['planta'] = mdlcat.Planta.objects.get(
                    id=int(wplanta))

            wqueryset = mdlcat.Sala.objects.filter(**wfilters) \
                .select_related("estado") \
                .select_related("username") \
                .select_related("planta") \
                .order_by("descripcion").distinct()
            # print(str(wqueryset.query))
            wfields = [
                "id", "fhregistro", "fhmodificacion",
                "descripcion", "limite_participantes",
                "estado", "planta", "username", "username_modif",
                "fecha_vigencia", "hora_inicio", "hora_final",
            ]
            woutcome = srlzcat.SalaSerializer(
                wqueryset,
                fields=wfields,
                many=True,
            ).data
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('catalogos.add_sala', raise_exception=True),)
    def saveInformation(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wdescription = wdata.get("descripcion") or ''
            if len(wdescription) == 0:
                raise ValueError('Error, descripcion no puede ser null.')
            westado = wdata.get("estado")
            wplanta = wdata.get("planta")
            wcapacidad = wdata.get("capacidad") or 1
            whora_inicio = wdata.get("hora_inicio") or None
            whora_final = wdata.get("hora_final") or None
            wfecha_vigencia = wdata.get("fecha_vigencia") or None
            if not wfecha_vigencia is None:
                wfecha_vigencia, wstatus = Validators.isDate(
                    wfecha_vigencia, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, Fecha Vigencia invalida.')

            objEstado = mdlmetas.Estados.objects.get(id=int(westado))
            objPlanta = mdlcat.Planta.objects.get(id=int(wplanta))
            wparameters = {
                "descripcion": wdescription.strip().upper(),
                "planta": objPlanta,
                "estado": objEstado,
                "limite_participantes": wcapacidad,
                "fecha_vigencia": wfecha_vigencia,
                "hora_inicio": whora_inicio,
                "hora_final": whora_final,
                "username": request.user,
            }
            objSala = mdlcat.Sala(**wparameters)
            objSala.save()
            wresponse['result'] = {
                "id": objSala.pk,
                "message": "Sala de Reserva procesada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('catalogos.change_sala', raise_exception=True),)
    def updateSalas(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widsala = wdata.get("id") or 0
            objSala = mdlcat.Sala.objects.get(id=int(widsala))
            wdescription = wdata.get("descripcion") or ''
            wcapacidad = wdata.get("capacidad") or 1
            whora_inicio = wdata.get("hora_inicio") or None
            whora_final = wdata.get("hora_final") or None
            if objSala == None:
                raise ValueError('Error, sala especificada no existe.')
            if len(wdescription) == 0:
                raise ValueError('Error, descripcion no puede ser null.')
            wfecha_vigencia = wdata.get("fecha_vigencia") or None
            if not wfecha_vigencia is None:
                wfecha_vigencia, wstatus = Validators.isDate(
                    wfecha_vigencia, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, fecha vigencia invalida.')

            widestado = wdata.get("estado") or 0
            widplanta = wdata.get("planta") or 0
            objEstado = mdlmetas.Estados.objects.get(id=int(widestado))
            objPlanta = mdlcat.Planta.objects.get(id=int(widplanta))

            objSala.estado = objEstado
            objSala.planta = objPlanta
            objSala.descripcion = wdescription.strip().upper()
            objSala.limite_participantes = wcapacidad
            objSala.fecha_vigencia = wfecha_vigencia
            objSala.hora_inicio = whora_inicio
            objSala.hora_final = whora_final
            objSala.username_modif = request.user
            objSala.save()
            wresponse['result'] = {
                "id": objSala.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('catalogos.delete_sala', raise_exception=True),)
    def removeSala(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widsala = wdata.get("item").get("id") or 0

            wfrom = datetime.now() - timedelta(days=7)
            wto = wfrom + timedelta(days=60)
            Qfilters = Q(
                estado__id=1,
                fhregistro__range=[wfrom, wto],
                sala__id=widsala,
            )
            objReservas = mdl.Reservas.objects.filter(Qfilters)
            if objReservas.count() > 0:
                wresponse['status'] = 422
                raise ValueError('Error, Sala cuenta con reservas activas.')

            objSala = mdlcat.Sala.objects.filter(id=int(widsala))
            if not objSala.exists():
                wresponse['status'] = 422
                raise ValueError('Error, Sala especificada no existe.')
            objSala.update(
                estado=mdlmetas.Estados.objects.get(id=2),
                username_modif=request.username,
                fhmodificacion=datetime.now(),
            )
            wresponse['result'] = {
                "message": "Sala fue eliminada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@md(login_required, name='dispatch')
class ViewReservasFeriados(View):
    template_name = 'reservas/reservas_feriados.html'
    template_header = 'SKBerge | Registro de Feriados'
    template_title = 'Registro de Feriados'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewReservasFeriados, self).dispatch(*args, **kwargs)

    @md(perm('reservas.add_feriados', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @md(perm('reservas.add_feriados', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_reservas_feriados":
                    return self.getFeriadosSearch(request)
                elif wtype == "save_information":
                    return self.saveInformation(request)
                elif wtype == "remove_reserva_feriado":
                    return self.removeFeriado(request)
                elif wtype == "update_reserva_feriado":
                    return self.updateFeriado(request)
                else:
                    pass
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getFeriadosSearch(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            # connection = MYSQLConnection()
            wdata = json.loads(request.body.decode("utf8"))
            wferiado = wdata.get('fecha') or ''
            wfilters = Q(id__isnull=False)
            if wferiado != '':
                # if date comes in the format value, change it by the default
                wfecha, wstatus = Validators.isDate(wferiado, "%d/%m/%Y")
                wferiado = wfecha.strftime(
                    "%Y-%m-%d") if wstatus else wferiado
                # query the value for both fields descripcion and fecha
                wfilters = Q(descripcion__icontains=wferiado) | \
                    Q(fecha__icontains=wferiado)

            wqueryset = mdl.Feriados.objects.filter(wfilters) \
                .select_related("estado") \
                .select_related("username") \
                .order_by("-fecha").distinct()
            # print(str(wqueryset.query))
            woutcome = srlz.FeriadosSerializer(
                wqueryset,
                many=True,
            ).data
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.add_feriados', raise_exception=True),)
    def saveInformation(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wdescription = wdata.get("descripcion") or ''
            if len(wdescription) == 0:
                raise ValueError('Error, descripcion no puede ser null.')
            westado = wdata.get("estado")
            wfecha = wdata.get("fecha") or None
            if not wfecha is None:
                wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, Fecha invalida.')

            objEstado = mdlmetas.Estados.objects.get(id=int(westado))
            wparameters = {
                "descripcion": wdescription.strip().upper(),
                "estado": objEstado,
                "fecha": wfecha,
            }
            objFeriado = mdl.Feriados(**wparameters)
            objFeriado.save()
            wresponse['result'] = {
                "id": objFeriado.pk,
                "message": "Sala de Reserva procesada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.change_feriados', raise_exception=True),)
    def updateFeriado(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widferiado = wdata.get("id") or 0
            objFeriado = mdl.Feriados.objects.get(id=widferiado)
            wdescription = wdata.get("descripcion") or ''
            if len(wdescription) == 0:
                raise ValueError('Error, descripcion no puede ser null.')
            wfecha = wdata.get("fecha") or None
            if not wfecha is None:
                wfecha, wstatus = Validators.isDate(
                    wfecha, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, fecha invalida.')

            widestado = wdata.get("estado") or 0
            objEstado = mdlmetas.Estados.objects.get(id=int(widestado))

            objFeriado.estado = objEstado
            objFeriado.descripcion = wdescription.strip().upper()
            objFeriado.fecha = wfecha
            objFeriado.save()
            wresponse['result'] = {
                "id": objFeriado.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.delete_feriados', raise_exception=True),)
    def removeFeriado(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widferiado = wdata.get("item").get("id") or 0
            objFeriado = mdl.Feriados.objects.filter(id=int(widferiado))
            if not objFeriado.exists():
                raise ValueError('Error, Sala especificada no existe.')
            objFeriado.get().delete()
            wresponse['result'] = {
                "message": "Feriado fue eliminado satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@md(login_required, name='dispatch')
class ViewReservasCalendario(View):
    template_name = 'reservas/reservas_calendario.html'
    template_header = 'SKBerge | Solicitudes de Reservas'
    template_title = 'Solicitudes de Reservas'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewReservasCalendario, self).dispatch(*args, **kwargs)

    @md(perm('reservas.add_reservas', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @md(perm('reservas.add_reservas', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_reserva_item":
                    return self.getReserva(request)
                if wtype == "get_reservas":
                    return self.getReservasSearch(request)
                if wtype == "save_information":
                    return self.saveInformation(request)
                if wtype == "update_reserva":
                    return self.updateReserva(request)
                if wtype == "remove_reserva":
                    return self.removeReserva(request)
                if wtype == "get_schedules":
                    obj = ViewReservasProgramacion()
                    return obj.getMeetings(request)
                if wtype == "get_holiday_validation":
                    return self.validateHoliday(request)
                if wtype == "add_external_member":
                    return self.addExternalMember(request)

            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def addExternalMember(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wnombres = wdata.get("nombres") or None
            wemail = wdata.get("email") or None
            if None in [wnombres, wemail]:
                wresponse["status"] = 422
                raise ValueError(str('Error, datos incompletos.'))

            Qfilters = Q(
                tipo_empleado__iexact=2,
                email_empresarial__iexact=wemail,
            )
            qs = mdlmetas.Empleados.objects.filter(Qfilters)
            if qs.count() > 0:
                x = qs.first()
                err = "Error, email se encuentra registrado a {}".format(
                    x.get_fullname.upper())
                raise ValueError(str(err))

            wparameters = {
                "nombres": wnombres.upper(),
                "email_empresarial": wemail.lower(),
                "tipo_empleado": 2,
            }
            objEmpleado = mdlmetas.Empleados(**wparameters)
            objEmpleado.save()

            wresponse['result'] = {
                "message": "Datos fueron procesados satisfactoriamente"
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def validateHoliday(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wfecha = wdata.get("fecha") or None
            wstatus = False
            if not Validators.isDate(wfecha, '%Y-%m-%d'):
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            objFeriado = mdl.Feriados.objects.filter(fecha=wfecha)
            if objFeriado.exists():
                wstatus = True
            wresponse["result"] = wstatus
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.add_reservas', raise_exception=True),)
    def getReserva(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            # connection = MYSQLConnection()
            wdata = json.loads(request.body.decode("utf8"))
            wreserva = wdata.get('id') or ''
            wqueryset = mdl.Reservas.objects.filter(id=int(wreserva)) \
                .order_by("-fhregistro").distinct()
            if not wqueryset.exists():
                raise ValueError('Error, reserva invalido.')
            woutcome = srlz.ReservasSerializer(
                wqueryset,
                many=True,
            ).data
            wqueryset_det = mdl.ReservasParticipantes.objects.filter(
                reserva=wqueryset.get())
            wdetails = srlz.ReservasParticipantesSerializer(
                wqueryset_det,
                many=True,
            ).data
            for item in wdetails:
                item["check"] = False
                item["visible"] = True
                item["empleado"]['fullname'] = '{} {}'.format(
                    item.get("empleado").get("nombres"),
                    item.get("empleado").get("apellidos"),
                )
            woutcome[0]['participantes'] = wdetails
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.add_reservas', raise_exception=True),)
    def getReservasSearch(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            # connection = MYSQLConnection()
            wdata = json.loads(request.body.decode("utf8"))
            Qfilters = Q(id__isnull=False, )

            if hasattr(request.user, 'userextension'):
                if request.user.userextension.check_all != 1:
                    Qfilters.add(Q(username=request.user,), Qfilters.connector)
            else:
                Qfilters.add(Q(username=request.user,), Qfilters.connector)

            wfecha_desde = wdata.get('fecha_desde') or None
            wfecha_hasta = wdata.get('fecha_hasta') or None
            if None not in[wfecha_desde, wfecha_hasta]:
                wfecha_desde, wstatus = Validators.isDate(
                    wfecha_desde, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, fecha desde invalido.')
                wfecha_hasta, wstatus = Validators.isDate(
                    wfecha_hasta, "%Y-%m-%d")
                if not wstatus:
                    raise ValueError('Error, fecha hasta invalido.')
                Qfilters.add(
                    Q(fecha_evento__range=[wfecha_desde, wfecha_hasta]),
                    Qfilters.connector
                )

            westado = wdata.get('estado') or ''
            if westado != '':
                Qfilters.add(
                    Q(estado__id=westado),
                    Qfilters.connector
                )

            wdescription = wdata.get('descripcion') or ''
            if wdescription != '':
                Qfilters.add(
                    Q(sala__descripcion__icontains=wdescription) |
                    Q(motivo__icontains=wdescription),
                    Qfilters.connector
                )

            qsempleados = (
                mdlmetas.Empleados.objects.all()
                .select_related("estado")
                .select_related("sucursal")
                .select_related("area")
                .select_related("id_puesto")
            )

            wqueryset = (
                mdl.Reservas.objects.filter(Qfilters)
                .select_related("sala__estado")
                .select_related("planta__estado")
                .select_related("username")
                .select_related("estado")
                .select_related("recurrence_type")
                .prefetch_related(Prefetch("empleado", queryset=qsempleados))
                .order_by("-fhregistro").distinct()
            )
            # print(str(wqueryset.query))
            woutcome = srlz.ReservasSerializer(
                wqueryset,
                many=True,
            ).data
            wresponse['result'] = woutcome
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def validationLayer(self, request, **kwargs):
        wresponse = self.defaultResponse.copy()
        status = True
        error = None
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wsolicitante = wdata.get("solicitante") or 0
            wfecha = wdata.get("fecha") or None
            westado = wdata.get("estado")
            wsala = wdata.get("sala")
            wplanta = wdata.get("planta")
            wtime_from = '{} {}'.format(wfecha, wdata.get("time_from"))
            wtime_to = '{} {}'.format(wfecha, wdata.get("time_to"))
            wmotivo = wdata.get("motivo") or ''

            if kwargs.get("process"):
                if kwargs.get("process") == "update":
                    widreserva = wdata.get("id")
                    wobjReserva = mdl.Reservas.objects.filter(id=widreserva)
                    if not wobjReserva.exists():
                        raise ValueError('Error, reserva invalido.')

            objSolicitante = mdlmetas.Empleados.objects \
                .filter(id=int(wsolicitante))
            if not objSolicitante.exists():
                raise ValueError('Error, solicitante invalido.')

            if wfecha is None:
                raise ValueError('Error, fecha invalido.')
            wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
            if not wstatus:
                raise ValueError('Error, fecha invalido.')

            wtime_format = "%Y-%m-%d %H:%M"
            if wtime_from is None:
                raise ValueError('Error, horario inicio invalido.')
            wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
            if not wstatus:
                raise ValueError('Error, horario inicio invalido.')

            if wtime_to is None:
                raise ValueError('Error, horario final invalido.')
            wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
            if not wstatus:
                raise ValueError('Error, horario final invalido.')

            objSala = mdlcat.Sala.objects.filter(id=int(wsala))
            if not objSala.exists():
                raise ValueError('Error, sala invalido.')
            objPlanta = mdlcat.Planta.objects.filter(id=int(wplanta))
            if not objPlanta.exists():
                raise ValueError('Error, planta invalido.')
            objEstado = mdlmetas.Estados.objects.filter(id=int(westado))
            if not objEstado.exists():
                raise ValueError('Error, estado invalido.')
            if len(wmotivo.strip()) == 0:
                raise ValueError('Error, motivo invalido.')

        except Exception as e:
            status = False
            error = str(e)
        return [status, error]

    def alertLayer(self, request, reservaInstance):
        _status, _error, _addition = None, None, None
        STATUS_ADDITION, STATUS_CHANGE = 2, 3
        try:
            wdata = json.loads(request.body.decode("utf8"))
            new_time = '{} - {}'.format(
                wdata.get("time_from"), wdata.get("time_to")
            )
            current_time = '{} - {}'.format(
                reservaInstance.hora_inicio.strftime("%H:%M"),
                reservaInstance.hora_final.strftime("%H:%M"),
            )
            new_attendees = len(wdata.get("participantes"))
            current_attendees = reservaInstance.reservasparticipantes_set.count()
            wnew = [x.get("empleado").get("email_empresarial")
                    for x in wdata.get("participantes")]
            wcurrent = [x.empleado.email_empresarial
                        for x in reservaInstance.reservasparticipantes_set.all()]
            wattendees = ','.join(set(wnew) - set(wcurrent))

            # modified at 2020.01.19
            # validating change in meeting room
            if wdata.get("sala") != reservaInstance.sala.id:
                _status = STATUS_CHANGE
                return [_status, _error, _addition]

            # validating change in recurrence flag
            wrecurrence = 1 if wdata.get("recurrence") == True else 0
            if wrecurrence != reservaInstance.recurrence:
                _status = STATUS_CHANGE
                return [_status, _error, _addition]

            # validating change in datetime appointment
            if current_time != new_time:
                _status = STATUS_CHANGE
                return [_status, _error, _addition]
            # validating any possible addition of attendees
            if new_attendees > current_attendees:
                _status = STATUS_ADDITION
                _addition = wattendees
                return [_status, _error, _addition]

        except Exception as e:
            _error = str(e)
        return [_status, _error, _addition]

    def saveDetails(self, request, **kwargs):
        status = True
        error = None
        try:
            objReserva = kwargs.get("reserva")
            wdetails = kwargs.get("participantes")
            if objReserva.pk:
                if len(wdetails) > 0:
                    wparticipantes = []
                    mdl.ReservasParticipantes.objects.filter(
                        reserva=objReserva).delete()
                    for item in wdetails:
                        objEmpleado = mdlmetas.Empleados.objects.get(
                            id=int(item.get("empleado").get("id")))
                        wkeys = {
                            "reserva": objReserva,
                            "estado": mdlmetas.Estados.objects.get(id=1),
                            "empleado": objEmpleado,
                        }
                        obj = mdl.ReservasParticipantes(**wkeys)
                        wparticipantes.append(obj)
                    mdl.ReservasParticipantes.objects.bulk_create(
                        wparticipantes)
        except Exception as e:
            error = str(e)
            status = False
        return [error, status]

    def sendEmailAlert(self, **kwargs):
        try:
            CalendarAlert(**kwargs)
        except Exception as e:
            print(str(e))

    @md(perm('reservas.add_reservas', raise_exception=True),)
    def saveInformation(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            waddition = {'process': 'new'}
            wstatus, werror = self.validationLayer(request, **waddition)
            if werror:
                raise ValueError(str(werror))

            wdate_format = '%Y-%m-%d'
            wsolicitante = wdata.get("solicitante")
            wfecha = wdata.get("fecha") or None
            wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
            westado = wdata.get("estado")
            wsala = wdata.get("sala")
            wplanta = wdata.get("planta")
            wtime_format = "%Y-%m-%d %H:%M"
            wtime_from = '{} {}'.format(
                wdata.get("fecha"), wdata.get("time_from"))
            wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
            wtime_to = '{} {}'.format(wdata.get("fecha"), wdata.get("time_to"))
            wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
            wmotivo = wdata.get("motivo")
            wdetails = wdata.get("participantes") or list()
            winclude = 1 if wdata.get("include") == True else 0
            wrecurrence = 1 if wdata.get("recurrence") == True else 0
            wparameters = {
                "empleado": mdlmetas.Empleados.objects
                .get(id=int(wsolicitante)),
                "fecha_evento": wfecha,
                "sala": mdlcat.Sala.objects.get(id=int(wsala)),
                "planta": mdlcat.Planta.objects.get(id=int(wplanta)),
                "estado": mdlmetas.Estados.objects.get(id=int(westado)),
                "hora_inicio": wtime_from,
                "hora_final": wtime_to,
                "motivo": wmotivo,
                "username": request.user,
                "include": winclude,
                "recurrence": wrecurrence,
            }

            # added at 2020.02.01
            wrec_from, wrec_to, wrec_type, wrec_interval, wrec_day = (
                None, None, None, None, None
            )
            wrec_day_week = None
            if wrecurrence == 1:
                if wdata.get("recurrence_from") != None:
                    wrec_from, wstatus = Validators.isDate(
                        wdata.get("recurrence_from"), wdate_format)
                if wdata.get("recurrence_to") != None:
                    wrec_to, wstatus = Validators.isDate(
                        wdata.get("recurrence_to"), wdate_format)
                if wdata.get("recurrence_type") != None:
                    wrec_type = mdl.RecurrenceType.objects.get(
                        id=wdata.get("recurrence_type"),
                    )
                wrec_interval = wdata.get("recurrence_interval") or 0
                wrec_day = wdata.get("recurrence_day") or 0
                day_week = wdata.get("recurrence_day_week") or None
                if day_week != None:
                    wrec_day_week = ','.join(
                        list([str(x) for x in day_week])
                    )
                wparameters.update({
                    "recurrence_type": wrec_type,
                    "recurrence_interval": wrec_interval,
                    "recurrence_from": wrec_from,
                    "recurrence_to": wrec_to,
                    "recurrence_day": wrec_day,
                    "recurrence_day_week": wrec_day_week,
                })
            #####################

            objReserva = mdl.Reservas(**wparameters)
            objReserva.save()
            error, status = self.saveDetails(request, **{
                "reserva": objReserva,
                "participantes": wdetails,
            })
            if error:
                raise ValueError(str(error))

            if objReserva.recurrence == 1:
                registerRecurrenceEvents(**{"instance": objReserva, })

            # send email with the meeting file .ics to applicants

            if objReserva.master is None:
                self.sendEmailAlert(
                    reserva_id=objReserva.pk,
                    type=5,
                )
            # ---------------------------
            wresponse['result'] = {
                "id": objReserva.pk,
                "message": "Reserva procesada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.delete_reservas', raise_exception=True),)
    def removeReserva(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            # widreserva = wdata.get("item").get("id") or 0
            lsitems = wdata.get("id_list") or []
            objReservas = (
                mdl.Reservas.objects.filter(id__in=lsitems,)
                .select_related("master")
                .select_related("estado")
            )
            if not objReservas.exists():
                raise ValueError('Error, reserva(s) especificadas no existen.')
            objReservas.update(
                estado=2,
                fhmodificacion=datetime.now(),
                username_modif=request.user,
            )
            wresponse['result'] = {
                "message": "Reserva(s) fueron anuladas satisfactoriamente.",
            }
            for item in objReservas:
                self.sendEmailAlert(
                    reserva_id=item.pk, type=7,
                )

            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @md(perm('reservas.change_reservas', raise_exception=True),)
    def updateReserva(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            waddition = {'process': 'update'}
            wstatus, werror = self.validationLayer(request, **waddition)
            if werror:
                raise ValueError(str(werror))

            wtime_format = "%Y-%m-%d %H:%M"
            wdate_format = '%Y-%m-%d'
            wsolicitante = wdata.get("solicitante")
            westado = wdata.get("estado")
            wsala = wdata.get("sala")
            wplanta = wdata.get("planta")
            wfecha = wdata.get("fecha")
            wfecha, wstatus = Validators.isDate(wfecha, "%Y-%m-%d")
            wtime_from = '{} {}'.format(
                wdata.get("fecha"), wdata.get("time_from"))
            wtime_from, wstatus = Validators.isDate(wtime_from, wtime_format)
            wtime_to = '{} {}'.format(wdata.get("fecha"), wdata.get("time_to"))
            wtime_to, wstatus = Validators.isDate(wtime_to, wtime_format)
            wmotivo = wdata.get("motivo")
            wdetails = wdata.get("participantes") or list()
            winclude = 1 if wdata.get("include") == True else 0

            # added at 2020.02.01
            wrecurrence = 1 if wdata.get("recurrence") == True else 0
            wrec_from, wrec_to, wrec_type, wrec_interval, wrec_day = (
                None, None, None, None, None
            )
            wrec_day_week = None
            if wrecurrence == 1:
                if wdata.get("recurrence_from") != None:
                    wrec_from, wstatus = Validators.isDate(
                        wdata.get("recurrence_from"), wdate_format)
                if wdata.get("recurrence_to") != None:
                    wrec_to, wstatus = Validators.isDate(
                        wdata.get("recurrence_to"), wdate_format)
                if wdata.get("recurrence_type") != None:
                    wrec_type = mdl.RecurrenceType.objects.get(
                        id=wdata.get("recurrence_type"),
                    )
                wrec_interval = wdata.get("recurrence_interval") or 0
                wrec_day = wdata.get("recurrence_day") or 0
                day_week = wdata.get("recurrence_day_week") or None
                if day_week != None:
                    wrec_day_week = ','.join(
                        list([str(x) for x in day_week])
                    )

            # save parent
            widreserva = wdata.get("id") or 0
            objReserva = mdl.Reservas.objects.get(id=widreserva)
            # validate any changes about datetime meeting or participants
            wstatus, error, wchanges = self.alertLayer(request, objReserva)
            if error:
                raise ValueError(str(error))
            # -------------------------------------
            objReserva.fecha_evento = wfecha
            objReserva.hora_inicio = wtime_from
            objReserva.hora_final = wtime_to
            objReserva.empleado = mdlmetas.Empleados.objects.get(
                id=int(wsolicitante))
            objReserva.sala = mdlcat.Sala.objects.get(id=int(wsala))
            objReserva.planta = mdlcat.Planta.objects.get(id=int(wplanta))
            objReserva.estado = mdlmetas.Estados.objects.get(id=int(westado))
            objReserva.motivo = wmotivo.strip()
            objReserva.username_modif = request.user
            objReserva.include = winclude

            # added at 2020.02.01
            if wrecurrence == 1:
                objReserva.recurrence = wrecurrence
                objReserva.recurrence_from = wrec_from
                objReserva.recurrence_to = wrec_to
                objReserva.recurrence_type = wrec_type
                objReserva.recurrence_day = wrec_day
                objReserva.recurrence_day_week = wrec_day_week
                objReserva.recurrence_interval = wrec_interval

            objReserva.save()
            # save details
            error, status = self.saveDetails(request, **{
                "reserva": objReserva,
                "participantes": wdetails,
            })
            if error:
                raise ValueError(str(error))

            # added at 2020.02.05
            nrecurrences = objReserva.master_set.all().count()
            if wrecurrence == 1 and nrecurrences == 0:
                registerRecurrenceEvents(**{"instance": objReserva, })

            # modified at 2020.01.19
            # validate alert status
            if wstatus != None:
                if objReserva.master is None:
                    self.sendEmailAlert(
                        reserva_id=objReserva.pk,
                        type=6,
                        # addition=wchanges,
                    )

            wresponse['result'] = {
                "id": objReserva.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getSchedules(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wfecha_evento = wdata.get("fecha")
            wsala = wdata.get("sala")
            wstart_time = '08:00'
            wend_time = '20:00'
            wday = datetime.strptime(wfecha_evento, "%Y-%m-%d").day
            # if datetime.now().date().day == wday:
            #     wstart_time = datetime.now().strftime("%H:%M")
            wkeys = {
                "fecha": wfecha_evento,
                "sala": wsala,
                "from_time": wstart_time,
                "to_time": wend_time,
            }
            wschedules, werror = getScheduleListBySala(**wkeys)
            if werror:
                raise ValueError(str(werror))
            wresponse["result"] = {
                "schedules": wschedules,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


# @md(login_required, name='dispatch')
class ViewReservasProgramacion(View):
    template_name = 'reservas/reservas_programacion.html'
    template_header = 'SKBerge | Programación de Reservas'
    template_title = 'Programación de Reservas'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfully.',
        "status": 200,
    }

    def dispatch(self, *args, **kwargs):
        return super(ViewReservasProgramacion, self).dispatch(*args, **kwargs)

    # @md(perm('reservas.schedule_reservas', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request, **kwargs):
        wresponse = self.defaultResponse.copy()
        header = self.template_header
        title = self.template_title
        if kwargs.get("idsala"):
            w_idsala = kwargs.get("idsala") or 0
            qssalas = mdlcat.Sala.objects.filter(
                id__iexact=w_idsala, estado__id=1,
            ).select_related("estado")

            if not qssalas.exists():
                raise Http404(str('Error, Page not found'))
        return render(request, self.template_name, locals())

    # @md(perm('reservas.schedule_reservas', raise_exception=True),)
    def post(self, request, **kwargs):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_reservas_meetings":
                    additional = {
                        "start_time": datetime.now().strftime("%H:%M"),
                    }
                    return self.getMeetings(request, **additional)
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    def getMeetings(self, request, **kwargs):
        wresponse = self.defaultResponse.copy()
        try:
            FORMAT_DATE_DEFAULT = '%Y-%m-%d'
            wdata = json.loads(request.body.decode("utf8"))
            wfecha_evento = wdata.get("fecha") or None
            werror, wstatus = Validators.isDate(
                wfecha_evento, FORMAT_DATE_DEFAULT)
            if not wstatus:
                wfecha_evento = datetime.now().strftime(FORMAT_DATE_DEFAULT)

            wsalas = wdata.get("sala") or ''
            wplanta = wdata.get("planta")
            # wstart_time = datetime.now().strftime("%H:%M")
            wfrom_time = '08:00'
            # just for information of the current day
            if datetime.now().strftime(FORMAT_DATE_DEFAULT) == wfecha_evento:
                if kwargs.get("start_time"):
                    wfrom_time = kwargs.get("start_time")

            wto_time = '20:00'
            srlzSalaSchedules, srzSalas, wresult = {}, {}, {}
            Qfilters = Q(id__isnull=False, estado=1)
            if len(wsalas) > 0:
                wlistado = wsalas.split(",")
                Qfilters.add(Q(id__in=wlistado), Qfilters.connector)

            salas_allowed = []
            # salas filter just by user
            if request.user.is_authenticated:
                wuser = request.user
                if hasattr(wuser, 'userextension'):
                    salas_allowed = [x.sala_id
                                     for x in request.user.catalogos_usersala_user_set.all()]
                    if wuser.userextension.salas_all != 1:
                        Qfilters.add(Q(id__in=salas_allowed,),
                                     Qfilters.connector)

            annotate_allowed = Case(When(id__in=salas_allowed, then=Value('1')),
                                    default=Value('0'),
                                    output_field=IntegerField())
            # modified at 2020.01.18
            objSalas = (
                mdlcat.Sala.objects.filter(Qfilters)
                .select_related("estado")
                .select_related("planta")
                .annotate(allowed=annotate_allowed)
            )

            wfields = [
                "id", "descripcion", "planta", "estado",
                "allowed", "limite_participantes",
            ]
            srzSalas = srlzcat.SalaSerializer(
                objSalas, fields=wfields, many=True).data
            for item in objSalas:
                wkeys = {
                    "fecha": wfecha_evento,
                    "sala": item.id,
                    "from_time": wfrom_time,
                    "to_time": wto_time,
                    "available_from": item.hora_inicio.strftime('%H:%M'),
                    "available_to": item.hora_final.strftime('%H:%M'),
                }
                woutcome, werror = getScheduleListBySala(**wkeys)
                if werror:
                    raise ValueError(str(werror))
                srlzSalaSchedules[item.id] = woutcome
            srzSalas.insert(0, {
                'descripcion': '', 'id': 0,
            })
            wresult["schedules"] = getScheduleList(
                from_time=wfrom_time,
                to_time=wto_time,
            )
            wresult["headers"] = srzSalas
            wresult["rows"] = srlzSalaSchedules
            wresponse["result"] = {
                "meetings": wresult,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


class ReservasAlerts(View):
    template_name = 'reservas/reservas_validation.html'
    template_header = 'SKBerge | Confirmación de Reservas'
    template_title = 'Confirmación de Reservas'
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfully.',
        "status": 200,
    }

    def get(self, request, **kwargs):
        if kwargs.get("confirmationid"):
            return self.getConfirmationProcess(request, **kwargs)
        if not kwargs.get("reservaid"):
            raise Http404(str('Error, Page not found'))
        return self.sendMailConfirmation(request, **kwargs)

    def sendMailConfirmation(self, request, **kwargs):
        wresponse = self.defaultResponse.copy()
        additional = {
            "reserva": kwargs.get("reservaid"),
            # "request": request,
        }
        sendMailBookingConfirmation(**additional)
        return JsonResponse(wresponse, status=wresponse.get("status"))

    def updateConfirmationStatus(
            self, pattern_reserva, pattern_confirmation, pattern_hashid
    ):
        DEADLINE_ANTICIPATION = 30
        if pattern_confirmation == "accept":
            mdl.Reservas.objects.filter(id=pattern_reserva) \
                .update(confirmacion=1, fhmodificacion=datetime.now(),)
            mdl.ReservasConfirmacion.objects.filter(hashid=pattern_hashid) \
                .update(procesado=1, fecha_procesado=datetime.now())
        elif pattern_confirmation == "reject":
            mdl.Reservas.objects.filter(id=pattern_reserva) \
                .update(
                    confirmacion=2,
                    estado=mdlmetas.Estados.objects.get(
                        descripcion__iexact="No activo"),
                    fhmodificacion=datetime.now(),
            )
            mdl.ReservasConfirmacion.objects.filter(hashid=pattern_hashid) \
                .update(procesado=1, fecha_procesado=datetime.now())
        elif pattern_confirmation == "anticipation":
            item = mdl.Reservas.objects.get(id=pattern_reserva, estado__id=1,)
            time_from = datetime.now()
            time_format = "%H:%M:%S"
            time_frange = "%H:%M"
            if item.hora_final > time_from.time():
                time_to = datetime.strptime(
                    item.hora_final.strftime(time_format), time_format)
                # get difference of now and the booking expiration time
                woutcome = time_to - time_from
                minutes = int(woutcome.seconds / 60)
                # if difference is greater than 30 minutes, free the time left
                if minutes > DEADLINE_ANTICIPATION:
                    result, err = getTimeInRange(
                        item.hora_inicio.strftime(time_frange),
                        item.hora_final.strftime(time_frange),
                        time_from.strftime(time_frange),
                    )
                    if result:
                        item.hora_final = result.get("to")
                        item.confirmacion = 2
                        # update the booking
                        item.save(update_fields=[
                                  "hora_final", "confirmacion", ])
                        # update the hashid to status 2
                        mdl.ReservasConfirmacion.objects.filter(hashid=pattern_hashid) \
                            .update(procesado=2, fecha_procesado=datetime.now())
                else:
                    item.confirmacion = 2
                    # update the booking
                    item.save(update_fields=["confirmacion", ])
                    # update the hashid to status 2
                    mdl.ReservasConfirmacion.objects.filter(hashid=pattern_hashid) \
                        .update(procesado=2, fecha_procesado=datetime.now())

    def getConfirmationProcess(self, request, **kwargs):
        wprocess = ["accept", "reject", "anticipation"]
        if not kwargs.get("confirmationid") in wprocess:
            raise Http404(str('Error, Page not found'))
        # validating hashid
        procesado_status = 1 if kwargs.get(
            "confirmationid") == "anticipation" else 0

        item = mdl.ReservasConfirmacion.objects.filter(
            hashid=kwargs.get("hashid"), procesado=procesado_status,)
        if not item.exists():
            raise Http404(str('Error, Page not found'))
        item = item.first()
        # getting reserva values
        objreserva, err = mailBookingGetReserva(item.reserva.id)
        if err:
            raise Http404(str('Error, Page not found'))
        content = {
            "confirmation_type": kwargs.get("confirmationid"),
            "reserva": objreserva,
        }
        self.updateConfirmationStatus(
            objreserva.get("id"), kwargs.get(
                "confirmationid"), kwargs.get("hashid"),
        )
        return render(request, self.template_name, locals())


class CalendarAlert(object):

    _reserva_id = None
    _reserva_type = None
    _reserva_object = None
    _list_type = {
        "1": "Creacion", "2": "Agregado participante",
        "3": "Cambio de horario", "4": "cancelacion de evento",
        "5": "Creacion en Microsoft Graph",
        "6": "Actualizacion en Microsoft Graph",
        "7": "Eliminacion en Microsoft Graph",
    }

    # added at 2020.01.15
    _MGRAPH_REGISTER, _MGRAPH_UPDATE, _MGRAPH_DELETE = 5, 6, 7
    _GRAPH_PROCESSES = [
        _MGRAPH_REGISTER, _MGRAPH_UPDATE, _MGRAPH_DELETE
    ]
    _GRAPH_PARAMETERS = [
        'MGRAPH_AUTHORITY',
        'MGRAPH_CLIENT_ID',
        'MGRAPH_SECRET',
        'MGRAPH_SCOPE',
        'MGRAPH_ACCOUNT',
        'MGRAPH_ACCOUNT_ID',
        'MGRAPH_TIMEZONE',
        'MGRAPH_TIMEZONE_HEADER',
        'MGRAPH_URL_API_CREATE',
        'MGRAPH_URL_API_UPDATE',
        'MGRAPH_URL_API_DELETE',
    ]
    _graph_params = None

    # added at 2020.02.02
    _RECURRENCE_DAILY, _RECURRENCE_WEEKLY, _RECURRENCE_MONTHLY = 1, 2, 3

    def __init__(self, *args, **kwargs):
        self._reserva_id = kwargs.get("reserva_id") or ''
        self._reserva_type = str(kwargs.get("type") or 1)
        self._reserva_object = mdl.Reservas.objects.get(
            id=int(self._reserva_id)
        )
        if int(self._reserva_type) == 2:
            self._reserva_addition = kwargs.get("addition") or list()
        self._graph_params = {}
        self.calendarProcess()
        return

    # added at 2020.01.15
    @property
    def get_reserva_type_descrip(self):
        return self._list_type.get(self._reserva_type)

    @property
    def get_reserva_type(self):
        return int(self._reserva_type)

    @property
    def get_graph_processes(self):
        return self._GRAPH_PROCESSES

    def get_param(self, p_parameter):
        return self._graph_params.get(p_parameter)

    def saveCalendar(self, **kwargs):
        item, error = None, None
        try:
            wkeys = {
                "reserva": self._reserva_object,
                "calendar_hash": kwargs.get("attachments")[0].get("uid"),
            }
            wqueryset = mdl.ReservasCalendarios.objects.filter(**wkeys)
            if wqueryset.exists():
                wqueryset.get().save()
            else:
                obj = mdl.ReservasCalendarios(**wkeys)
                obj.save()
        except Exception as e:
            error = str(e)
        return [item, error]

    def saveCalendarMail(self, **kwargs):
        status, error = True, None
        try:
            self._reserva_object.refresh_from_db()
            wkeys = {
                "motivo": self._list_type.get(self._reserva_type),
                "email_from": self._reserva_object.empleado.email_empresarial,
                "email_to": self._reserva_object.get_email_accounts,
                "calendar": self._reserva_object.reservascalendarios,
            }
            objReservaEnvio = mdl.ReservasEnvios(**wkeys)
            objReservaEnvio.save()
        except Exception as e:
            error = str(e)
            status = False
        return [status, error]

    def calendarProcess(self):
        # added at 2020.01.15
        graph_status = False
        if self.get_reserva_type in self._GRAPH_PROCESSES:
            graph_status = True

        # modified at 2020.01.15
        if graph_status == False:
            # work with calendar file
            result, error = self.calendarGetContext(
                self._reserva_id, self._reserva_type,
            )
            if error:
                raise ValueError(str(error))
            result, error = self.calendarGenerateFile(result)
            if error:
                raise ValueError(str(error))

            status, error = self.calendarSendEmail(result)
            if error:
                raise ValueError(str(error))
        elif graph_status == True:
            # work with microsoft graph resources
            self.MGraphProcess()
        else:
            pass

    def calendarSendEmail(self, wparameters):
        status, error = True, None
        try:
            wcontext = {
                'content_header': wparameters.get("mail_summary"),
                'content_body': '',
            }
            wemails = self._reserva_object.get_email_accounts.split(",")
            if int(self._reserva_type) == 2:
                wemails = [self._reserva_addition, ]
            wsettings = {
                'subject': wparameters.get("summary"),
                'body': wparameters.get("description"),
                "to": list(set(wemails)) or list(),
                "from": wparameters.get("organizer").get("email"),
                "template": 'mail/mail_template.html',
                "context": wcontext,
                "attachments": wparameters.get("attachments"),
            }
            objMail = SendMailer(**wsettings)
            error, status = objMail.BuildMessage()
            if error:
                print(str(error))
            objMail.Send()
            # save details about the mail
            status, error = self.saveCalendarMail(**wparameters)
            if error:
                raise ValueError(str(error))
        except Exception as e:
            error = str(e)
            status = False
        return [status, error]

    def calendarGetContext(self, reserva_uid, reserva_type):
        error, result = None, None
        try:
            obj = self._reserva_object
            wcalendar_uid = None
            wcalendar_type = self._list_type.get(self._reserva_type)
            if hasattr(obj, 'reservascalendarios'):
                wcalendar_uid = obj.reservascalendarios.calendar_hash

            worganizer = {
                "email": obj.empleado.email_empresarial,
                "cname": obj.empleado.get_fullname,
                "role": 'CHAIR',
            }
            wattendees = []
            for item in obj.reservasparticipantes_set.all():
                item_dict = {
                    "email": item.empleado.email_empresarial,
                    "name": item.empleado.get_fullname,
                }
                wattendees.append(item_dict)

            wtime_from = '{} {}'.format(
                str(obj.fecha_evento), str(obj.hora_inicio),
            )
            wtime_to = '{} {}'.format(
                str(obj.fecha_evento), str(obj.hora_final),
            )
            wmotivo = obj.motivo
            if int(self._reserva_type) == 4:
                wmotivo = '{} {}'.format(wcalendar_type, wmotivo)

            wparameters = {
                "calendar_uid": wcalendar_uid,
                "calendar_type_id": int(self._reserva_type),
                "calendar_type": wcalendar_type,
                "summary": wmotivo,
                "description": '',
                "location": obj.get_email_location,
                "time_from": wtime_from,
                "time_to": wtime_to,
                "time_date": str(obj.fecha_evento),
                "organizer": worganizer,
                "attendees": wattendees,
                "mail_summary": obj.get_email_summary,
                "mail_body": obj.get_email_description,
            }
            result = wparameters
        except Exception as e:
            error = str(e)
        return [result, error]

    def calendarGenerateFile(self, wparameters):
        error, result = None, None
        try:
            objCalendar = ICalendarGenerator(**wparameters)
            outcome, werror = objCalendar.generateFile()
            if werror:
                raise ValueError(str(werror))
            wparameters["attachments"] = [outcome]
            result = wparameters.copy()
            # save the calendar filename generated by reserva
            outcome, werror = self.saveCalendar(**result)
            if werror:
                raise ValueError(str(werror))
        except Exception as e:
            error = str(e)
        return [result, error]

    # added at 2020.01.15
    def MGraphProcess(self):
        if self.get_reserva_type == self._MGRAPH_REGISTER:
            self.MGraphRegisterMeeting()
        elif self.get_reserva_type == self._MGRAPH_UPDATE:
            self.MGraphUpdateMeeting()
        elif self.get_reserva_type == self._MGRAPH_DELETE:
            self.MGraphDeleteMeeting()
        else:
            pass

    def MGraphRegisterMeeting(self):
        error = None
        try:
            self.getMGraphVariables()
            wtoken = self.getMGraphToken()
            if not wtoken is None:
                self.makeMGraphRequest(wtoken)

        except Exception as e:
            error = str(e)
            print(error)

    def MGraphUpdateMeeting(self):
        error = None
        try:
            obj = self._reserva_object
            wid = len(obj.mgraph_id or '')
            if wid == 0:
                raise ValueError(
                    str('Nothing to process, calendar ID is empty'))

            self.getMGraphVariables()
            wtoken = self.getMGraphToken()
            if not wtoken is None:
                self.makeMGraphRequest(wtoken)
        except Exception as e:
            error = str(e)
            print(error)

    def MGraphDeleteMeeting(self):
        error = None
        try:
            obj = self._reserva_object
            wid = len(obj.mgraph_id or '')
            if wid == 0:
                raise ValueError(
                    str('Nothing to process, calendar ID is empty'))

            self.getMGraphVariables()
            wtoken = self.getMGraphToken()
            if not wtoken is None:
                self.makeMGraphRequest(wtoken)
        except Exception as e:
            error = str(e)
            print(error)

    def getMGraphMethod(self):
        if self.get_reserva_type == self._MGRAPH_REGISTER:
            return 'post'
        elif self.get_reserva_type == self._MGRAPH_DELETE:
            return 'delete'
        elif self.get_reserva_type == self._MGRAPH_UPDATE:
            return 'patch'
        return 'get'

    def updateMGraphStatus(self, response):
        try:
            obj = self._reserva_object
            if self.get_reserva_type == self._MGRAPH_REGISTER:
                if response.status_code == 201:
                    obj.mgraph_id = response.json().get("id")
                    obj.mgraph_enviado = 1
                    obj.save(
                        update_fields=['mgraph_id', 'mgraph_enviado', ],
                    )
            else:
                pass

        except Exception as e:
            msg = 'Error saving logs:{}'.format(str(e))
            print(msg)

    def getMGraphUrl(self):
        obj = self._reserva_object
        if self.get_reserva_type == self._MGRAPH_REGISTER:
            wurl = self.get_param("MGRAPH_URL_API_CREATE").format(
                self.get_param("MGRAPH_ACCOUNT_ID")
            )
        elif self.get_reserva_type == self._MGRAPH_UPDATE:
            wurl = self.get_param("MGRAPH_URL_API_UPDATE").format(
                self.get_param("MGRAPH_ACCOUNT_ID"),
                obj.mgraph_id,
            )
        elif self.get_reserva_type == self._MGRAPH_DELETE:
            wurl = self.get_param("MGRAPH_URL_API_DELETE").format(
                self.get_param("MGRAPH_ACCOUNT_ID"),
                obj.mgraph_id,
            )
        else:
            wurl = ''
        return wurl

    def makeMGraphRequest(self, p_token):
        try:
            wurl = self.getMGraphUrl()
            wjson = self.getMGraphCalendarJson()
            print(wjson)

            response = requests.request(
                method=self.getMGraphMethod(),
                url=wurl,
                headers={
                    'Authorization': 'Bearer ' + p_token.get("access_token"),
                    'Prefer': self.get_param("MGRAPH_TIMEZONE_HEADER"),
                    'Content-type': 'application/json',
                },
                json=wjson,
            )
            self.updateMGraphStatus(response)
            if response.headers.get("content-type") == 'application/json':
                print(response.json())

        except Exception as e:
            msg = 'Error performing mgraph request: {}'.format(str(e))
            print(msg)

    def getMGraphAttendees(self):
        obj = self._reserva_object
        wdetails = list((
            obj.reservasparticipantes_set.filter(
                estado__id=1,
            )
            .select_related("empleado")
            .select_related("estado")
            .select_related("reserva")
            .annotate(
                attendee_mail=F('empleado__email_empresarial'),
                attendee_name=F('empleado__nombres'),
                attendee_lastname=F('empleado__apellidos'),
            ).values(
                "attendee_mail", "attendee_name", "attendee_lastname",
            )
        ))
        wfullname = '{} {}'
        wattendees = [
            {
                "emailAddress": {
                    "address": item.get("attendee_mail"),
                    "name": wfullname.format(
                        (item.get("attendee_name") or '').strip(),
                        (item.get("attendee_lastname") or '').strip(),
                    ),
                },
                "type": "required",
            }
            for item in wdetails
        ]
        return wattendees

    def getMGraphRecurrence(self):
        obj = self._reserva_object
        wdate_format = '%Y-%m-%d'
        tag_recurrence = {
            "type": obj.recurrence_type.name_english,
            "interval": obj.recurrence_interval,
        }

        # based on the recurrence type the field will be added
        if obj.recurrence_type.id == self._RECURRENCE_WEEKLY:
            # tag recurrence type weekly
            tag_day_week = list(
                mdl.DaysOfWeek.objects.filter(
                    id__in=(obj.recurrence_day_week.split(",") or [])
                ).values_list("name_english", flat=True)
            )
            tag_recurrence.update({
                "daysOfweek": tag_day_week,
                "firstDayOfWeek": "monday",
            })
        elif obj.recurrence_type.id == self._RECURRENCE_MONTHLY:
            tag_recurrence.update({
                "dayOfMonth": obj.recurrence_day,
            })

        # official schema of the recurrence field
        outcome = {
            "recurrence": {
                "pattern": tag_recurrence,
                "range": {
                    "type": "endDate",
                    "startDate": obj.recurrence_from.strftime(wdate_format),
                    "endDate": obj.recurrence_to.strftime(wdate_format),
                    "recurrenceTimeZone": self.get_param("MGRAPH_TIMEZONE"),
                },
            }
        }

        return outcome

    def getMGraphCalendarJson(self):
        obj = self._reserva_object
        outcome = {}
        try:
            if self.get_reserva_type in [self._MGRAPH_REGISTER, self._MGRAPH_UPDATE]:
                wtime_from = '{}T{}'.format(
                    str(obj.fecha_evento), str(obj.hora_inicio),
                )
                wtime_to = '{} {}'.format(
                    str(obj.fecha_evento), str(obj.hora_final),
                )
                outcome = {
                    "subject": obj.motivo,
                    "body": {
                        "contentType": "HTML",
                        "content": obj.motivo,
                    },
                    "start": {
                        "dateTime": wtime_from,
                        "timeZone": self.get_param("MGRAPH_TIMEZONE"),
                    },
                    "end": {
                        "dateTime": wtime_to,
                        "timeZone": self.get_param("MGRAPH_TIMEZONE"),
                    },
                    "location": {
                        "displayName": obj.get_email_location,
                    },
                    "attendees": self.getMGraphAttendees(),
                }
                if obj.recurrence == 1:
                    outcome.update(self.getMGraphRecurrence())

            elif self.get_reserva_type == self._MGRAPH_DELETE:
                outcome = {}
            else:
                outcome = {}
        except Exception as e:
            msg = 'Error getting json body: {}'.format(str(e))
            print(e)
        return outcome

    def getMGraphToken(self):
        outcome, error = None, None
        try:
            app = msal.ConfidentialClientApplication(
                self.get_param("MGRAPH_CLIENT_ID"),
                authority=self.get_param("MGRAPH_AUTHORITY"),
                client_credential=self.get_param("MGRAPH_SECRET"),
                verify=False,
                # token_cache=...  # Default cache is in memory only.
            )
            result = None
            result = app.acquire_token_silent(
                [self.get_param("MGRAPH_SCOPE")], account=None
            )
            # validating if token was gotten or not
            if not result:
                # logging.info(
                #     "No suitable token exists in cache. Let's get a new one from AAD.")
                result = app.acquire_token_for_client(
                    scopes=self.get_param("MGRAPH_SCOPE")
                )
                if "access_token" in result:
                    outcome = result
            else:
                outcome = result

        except Exception as e:
            msg = 'Error getting mgraph token: {}'.format(str(e))
            print(msg)
        return outcome

    def getMGraphVariables(self):
        error = None
        try:
            qfilter = Q(
                parameter__in=self._GRAPH_PARAMETERS,
                estado__id=1,
            )
            qs = (
                mdlcat.Parameters.objects.filter(qfilter)
                .select_related("estado")
                .select_related("username")
                .select_related("username_modif")
                .values("parameter", "valor", "default")
            )

            if qs.exists():
                params = list(qs)
                for p in params:
                    self._graph_params.update(
                        {str(p.get("parameter")): str(
                            p.get("valor") or p.get("default"))}
                    )

        except Exception as e:
            msg = 'Error getting paramaters env: {}'.format(str(e))
            print(msg)
