from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.reservas import views as v

app_name = 'module_reservas'

urlpatterns = [
    url(r'^reservas_salas/$', v.ViewReservasSalas.as_view(), name='reservas_salas'),
    url(r'^reservas_feriados/$', v.ViewReservasFeriados.as_view(),
        name='reservas_feriados'),
    url(r'^reservas_calendario/$',
        v.ViewReservasCalendario.as_view(), name='reservas_calendario'),
    url(r'^reservas_programacion/(?P<idsala>\d{1,2})/$',
        v.ViewReservasProgramacion.as_view(),
        name='reservas_programacion_sala'),
    url(r'^reservas_programacion/$',
        v.ViewReservasProgramacion.as_view(), name='reservas_programacion'),
    url(r'^reservas_alerts/(?P<reservaid>\d+)/?$',
        v.ReservasAlerts.as_view(), name='reservas_alerts'),
    url(r'^reservas_alerts/(?P<confirmationid>[a-zA-Z]+)/(?P<hashid>[a-zA-Z-0-9]+)/?$',
        v.ReservasAlerts.as_view(), name='reservas_alerts_confirmation'),
    # url(r'^reservas_generar_calendario/$',
    # v.ViewGenerateCalendar.as_view(), name='reservas_generar_calendario'),
]
