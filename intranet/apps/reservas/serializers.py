#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.reservas import models as m
from apps.metas import serializers as srlmetas
from apps.catalogos import serializers as srlcat
from apps.common import serializers as srlcommon


class DaysOfWeekSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.DaysOfWeek
        fields = '__all__'


class RecurrenceTypeSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.RecurrenceType
        fields = '__all__'


class FeriadosSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Feriados
        fields = '__all__'


class ReservasSerializer(srlcommon.DynamicFieldsModelSerializer):
    empleado = srlmetas.EmpleadosSerializer(
        fields=[
            'id', 'ndoc', 'nombres', 'apellidos',
            'email_empresarial', 'fullname',
        ],
        read_only=True,)
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    sala = srlcat.SalaSerializer(
        fields=['id', 'descripcion', 'limite_participantes'],
        read_only=True,
    )
    planta = srlcat.PlantaSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    recurrence_type = RecurrenceTypeSerializer(
        fields=['id', 'name'], read_only=True,
    )
    # field for UI removing process
    check = serializers.SerializerMethodField()

    class Meta:
        model = m.Reservas
        fields = '__all__'

    def get_check(self, obj):
        return 0


class ReservasParticipantesSerializer(srlcommon.DynamicFieldsModelSerializer):
    empleado = srlmetas.EmpleadosSerializer(
        fields=[
            'id', 'ndoc', 'nombres',
            'apellidos', 'email_empresarial',
        ],
        read_only=True,)
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    reserva = ReservasSerializer(fields=['id', ], read_only=True,)

    class Meta:
        model = m.ReservasParticipantes
        fields = '__all__'
