#! /usr/bin/env python
# -*- encoding:utf8 -*-
from django.conf import settings as cnf
from django.urls import reverse
from datetime import datetime, timedelta
from apps.reservas import (
    models as mdl,
    serializers as srlz,
)
from django.db.models import (
    Q, F, Count, Sum, Prefetch,
    Case, When, Value, IntegerField,
)
from apps.common.utils import Validators as utl
from apps.mail.views import SendMailer

import re
import time


def isInRange(time_from, time_to, time_value):
    try:
        wfrom = time.strptime(time_from, "%H:%M")
        wto = time.strptime(time_to, "%H:%M")
        wcurrent = time.strptime(time_value, "%H:%M")
        if wcurrent >= wfrom and wcurrent <= wto:
            return True
        return False
    except Exception as e:
        return False


def getTimeInRange(wfrom, wto, wcurrent):
    outcome, error = None, None
    try:
        wschedules = getScheduleList(from_time=wfrom, to_time=wto)
        for w in wschedules:
            timex = w.get("time_value").split("-")[0].strip()
            timey = w.get("time_value").split("-")[1].strip()
            if isInRange(timex, timey, wcurrent):
                outcome = {
                    "from": timex, "to": timey, "value": wcurrent,
                }
                break
    except Exception as e:
        error = str(e)
    return [outcome, error]


def getScheduleListBySala(**kwargs):
    result, error = None, None
    try:
        wfecha_evento = kwargs.get("fecha")
        wsala = kwargs.get("sala")
        start_time = kwargs.get("from_time") or None
        end_time = kwargs.get("to_time") or None
        # get the time range of availability of the meeting room
        avail_from = kwargs.get("available_from") or None
        avail_to = kwargs.get("available_to") or None

        wschedules = getScheduleList(
            from_time=start_time,
            to_time=end_time,
            available_from=avail_from,
            available_to=avail_to,
        )

        Qfilters = Q(fecha_evento=wfecha_evento, sala__id=wsala, estado=1,)
        wquery = mdl.Reservas.objects.filter(Qfilters) \
            .select_related("estado") \
            .select_related("planta") \
            .select_related("sala__planta") \
            .select_related("username") \
            .select_related("empleado")

        wlistado = [item for item in wquery]
        for item in wlistado:
            for x in wschedules:
                time_from = item.hora_inicio.strftime("%H:%M")
                time_to = item.hora_final.strftime("%H:%M")
                time_value = '{}{}'.format(time_from, time_to)
                wtime_x = x.get("time_value").split("-")[0].strip()
                wtime_y = x.get("time_value").split("-")[1].strip()
                if isInRange(time_from, time_to, wtime_x) \
                        and isInRange(time_from, time_to, wtime_y):
                    wkeys = {
                        "reserva_id": item.id,
                        "solicitante": '{} {}'.format(
                            item.empleado.nombres, item.empleado.apellidos
                        ),
                        "nparticipantes": item.reservasparticipantes_set.count(),
                        "fhregistro": item.fhregistro,
                        "motivo": item.motivo,
                    }
                    x.update(wkeys)
        result = wschedules
    except Exception as e:
        error = str(e)
    return [result, error]


def getScheduleList(**kwargs):
    MINUTES_INTERVAL = 30
    TIME_FORMAT = '%H:%M'
    DATE_FORMAT = '%Y-%m-%d'
    WHILE_STATUS = True
    FROM_TIME = 8
    if kwargs.get("from_time"):
        if kwargs.get("from_time") != None:
            FROM_TIME = time.strptime(
                kwargs.get("from_time"), "%H:%M").tm_hour
    LIMIT_TIME = '23:30'
    if kwargs.get("to_time"):
        if kwargs.get("to_time") != None:
            LIMIT_TIME = kwargs.get("to_time")

    # get status availability status
    AVAIL_FROM = kwargs.get("available_from") or None
    AVAIL_TO = kwargs.get("available_to") or None
    AVAIL_ST = False
    if AVAIL_FROM != None and AVAIL_TO != None:
        AVAIL_ST = True
    # -----------------------------------
    wdate = '{} 00:00'.format(datetime.now().strftime(DATE_FORMAT))
    current_time = datetime.strptime(
        wdate, '{} {}'.format(DATE_FORMAT, TIME_FORMAT))
    list_schedule = list()
    while WHILE_STATUS:
        time_from = current_time.strftime(TIME_FORMAT)
        current_time += timedelta(minutes=MINUTES_INTERVAL)
        old_time = current_time - timedelta(minutes=MINUTES_INTERVAL)
        time_to = current_time.strftime(TIME_FORMAT)
        time_value = '{} - {}'.format(time_from, time_to)
        item = dict()
        item["allowed"] = True
        if AVAIL_ST:
            st_from = isInRange(AVAIL_FROM, AVAIL_TO, time_from)
            st_to = isInRange(AVAIL_FROM, AVAIL_TO, time_to)
            # print("value : {}, is in {} - {} = {}".format(
            #     time_from, AVAIL_FROM, AVAIL_TO, st_from
            # ))
            if not st_from or not st_to:
                item["allowed"] = False

        if int(old_time.strftime("%H")) >= FROM_TIME:
            item["value"] = time_value
            list_schedule.append(item)

        if current_time.strftime(TIME_FORMAT) == LIMIT_TIME:
            WHILE_STATUS = False

    wkeys = ['id', 'time_value', 'check', 'allowed', ]
    wresult = [dict(zip(wkeys,
                        [
                            re.sub(r':|-| ', '', item.get("value")),
                            item.get("value"), False, item.get("allowed")
                        ])) for item in list_schedule]
    return wresult


def mailBookingLog(**kwargs):
    wparams = {
        "reserva": mdl.Reservas.objects.get(id=kwargs.get("reserva")),
        "hashid": kwargs.get("hashid"),
    }
    obj = mdl.ReservasConfirmacion(**wparams)
    obj.save()


def mailBookingSendMail(**kwargs):
    status, error = False, None
    try:
        email = kwargs.get("email") or {}
        content = kwargs.get("content") or {}
        parent = content.get("parent")
        wcurrent_date = datetime.now().strftime("%d/%m/%Y")

        wtitle = 'Meeting confirmation / Confirmación de reunión - {} {}'.format(
            parent.get("sala").get("descripcion"), wcurrent_date,
        )

        wcontext = {
            'content_header': wtitle,
        }
        wcontext.update(kwargs)
        # print(wcontext)
        wsettings = {
            'subject': wcontext.get("content_header"),
            "to": [email.get("email_empresarial"), ] or list(),
            "template": 'mail/mail_reserva_confirmation.html',
            "context": wcontext,
        }
        objMail = SendMailer(**wsettings)
        error, status = objMail.BuildMessage()
        if error:
            raise ValueError(str(error))
        objMail.Send()

        # save hashid generated by email
        wparams = {
            "hashid": parent.get("urls").get("hashid"), "reserva": parent.get("id"),
        }
        mailBookingLog(**wparams)
        # ---------------------------------
    except Exception as e:
        error = str(e)
        print(error)
    return [status, error]


def mailBookingGetUrls(**kwargs):
    outcome, error = [], None
    try:
        wrequest = kwargs.get("request")
        wview_name = 'reservas:reservas_alerts_confirmation'
        whashid = utl.getMd5Hash(utl.getDatetimeHash(), 40)
        # outcome = dict([
        #     (x, wrequest.build_absolute_uri(reverse(wview_name, kwargs={
        #      "hashid": whashid, "confirmationid": x, })),)
        #     for x in ["accept", "reject", "anticipation"]
        # ])
        outcome = dict([
            (x, reverse(wview_name, kwargs={
             "hashid": whashid, "confirmationid": x, }),)
            for x in ["accept", "reject", "anticipation"]
        ])
        outcome["hashid"] = whashid
    except Exception as e:
        error = str(e)
    return [outcome, error]


def mailBookingGetReserva(pattern_reserva):
    outcome, error = None, None
    try:
        Qfilters = Q(id=pattern_reserva)
        wfields = [
            "id", "fhregistro", "fecha_evento", "hora_inicio",
            "hora_final", "empleado", "estado", "planta", "sala",
            "motivo", "username",
        ]
        qsreserva = mdl.Reservas.objects.filter(Qfilters) \
            .select_related("estado") \
            .select_related("planta__estado") \
            .select_related("sala__estado") \
            .select_related("estado") \
            .select_related("username")
        if not qsreserva.exists():
            wmessage = "Error, reserva no existe ID: {}".format(
                str(pattern_reserva))
            raise ValueError(wmessage)
        wsrlz = srlz.ReservasSerializer(
            qsreserva.first(), fields=wfields,).data
        outcome = wsrlz
    except Exception as e:
        error = str(e)
    return [outcome, error]


def mailBookingGetContent(**kwargs):
    outcome, error = None, None
    try:
        wreserva = kwargs.get("reserva")
        wrequest = kwargs.get("request")
        objreserva, err = mailBookingGetReserva(wreserva)
        if err:
            raise ValueError(err)
        wemails, err = mailBookingGetUrls(**kwargs)
        if err:
            raise ValueError(str(err))
        objreserva.update({
            "urls": wemails,
        })
        outcome = {
            "content": {
                "parent": objreserva,
                "site_url": cnf.SITE_URL,
            },
            "email": objreserva.get("empleado"),
        }
    except Exception as e:
        error = str(e)
    return [outcome, error]


def sendMailBookingConfirmation(**kwargs):
    status, error = False, None
    try:
        reserva = kwargs.get("reserva") or 0
        wparams = {
            "reserva": reserva,
            # "request": kwargs.get("request"),
        }
        wparams, err = mailBookingGetContent(**wparams)
        if err:
            raise ValueError(err)
        st, err = mailBookingSendMail(**wparams)
        if err:
            raise ValueError(err)
    except Exception as e:
        error = str(e)
        print(error)
    return [status, error]


# added at 2020.02.03
_RECURRENCE_DAILY, _RECURRENCE_WEEKLY, _RECURRENCE_MONTHLY = 1, 2, 3


def registerEventInstance(pattern_instance, pattern_parent, pattern_date):
    obj = pattern_instance
    obj.pk = None
    obj.fecha_evento = pattern_date
    obj.fhregistro = datetime.now()
    obj.fregistro = datetime.now().date()

    lsfields = [
        "recurrence", "recurrence_type", "recurrence_from",
        "recurrence_to", "recurrence_day", "recurrence_day_week",
        "recurrence_interval", "master",
    ]
    lsvalues = [
        0, None, None, None, 0, None, 0, None,
    ]
    # cleaning the recurrence fields to their default values
    for nindex, item in list(enumerate(lsfields)):
        if hasattr(obj, item):
            setattr(obj, item, lsvalues[nindex])

    obj.save()
    obj.master = pattern_parent
    obj.save(update_fields=["master", ])


def registerRecurrenceEvents(**kwargs):
    outcome, error = None, None
    try:
        DATE_FORMAT = '%Y-%m-%d'
        obj = None
        if not kwargs.get("instance"):
            raise ValueError(
                str('Nothing to process, reserva instance wasnt specified.'))

        obj = kwargs.get("instance")
        if obj.recurrence == 0:
            raise ValueError(str('Nothing to process, theres not recurrence.'))

        current_date = obj.recurrence_from
        # get meetings in the range of the recurrence
        qfilter = Q(
            estado__id=1,
            fecha_evento__range=[
                obj.recurrence_from, obj.recurrence_to],
            sala=obj.sala,
        )

        if obj.recurrence_type.id == _RECURRENCE_MONTHLY:
            qfilter.add(
                Q(fecha_evento__day=obj.recurrence_day,), qfilter.connector
            )

        qsmeetings = [x.strftime(DATE_FORMAT) for x in list(
            mdl.Reservas.objects.filter(qfilter)
            .values_list("fecha_evento", flat=True)
        )] or []

        qsdayweek = None
        # if recurrence type is weekly
        if obj.recurrence_type.id == 2:
            qsdayweek = list((
                mdl.DaysOfWeek.objects.filter(
                    estado__id=1, id__in=obj.recurrence_day_week.split(",")
                ).values_list("name_english", flat=True)
            )) or []

        lsrecurrence = []

        parent_clone, childs_clone = None, None
        parent_clone = (
            mdl.Reservas.objects.get(id=obj.id,)
        )
        # childs_clone = (

        # )
        # start the generation of the meetings
        WEEK_INIT = 1
        WEEK_LIMIT = 8
        while obj.recurrence_to >= current_date:
            if obj.recurrence_type.id == _RECURRENCE_DAILY:
                if not current_date.strftime(DATE_FORMAT) in qsmeetings:
                    # generate meeting
                    registerEventInstance(parent_clone, obj, current_date)

                # increment the date for the quantity of the interval
                current_date = current_date + \
                    timedelta(days=obj.recurrence_interval)

            if obj.recurrence_type.id == _RECURRENCE_WEEKLY:
                nday = None
                for item in range(WEEK_INIT, WEEK_LIMIT):
                    # increment the date with the counter
                    nday = current_date + timedelta(days=item)
                    # validate if there is another meeting the same date
                    if not nday.strftime(DATE_FORMAT) in qsmeetings:
                        if nday.strftime("%A").lower() in qsdayweek:
                            # generate meeting
                            registerEventInstance(parent_clone, obj, nday)

                # increment the date for the quantity of the interval
                current_date = current_date + \
                    timedelta(days=obj.recurrence_interval *
                              (WEEK_LIMIT - WEEK_INIT))

            if obj.recurrence_type.id == _RECURRENCE_MONTHLY:
                month_from = current_date.month
                month_to = (obj.recurrence_to.month + 1)
                for month in range(month_from, month_to):
                    nday = datetime(current_date.year, month,
                                    obj.recurrence_day)
                    # validate if there is another meeting the same date
                    if not nday.strftime(DATE_FORMAT) in qsmeetings:
                        # generate meeting
                        registerEventInstance(parent_clone, obj, nday)

                # increment the date to recurrence_to
                current_date = obj.recurrence_to + timedelta(days=1)

    except Exception as e:
        error = str(e)
    return [outcome, error]
