from django.db import models
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from decimal import Decimal

# Create your models here.

DEFAULT_STATUS = 1

# added at 2020.02.01


class DaysOfWeek(CommonStructure):

    name = models.CharField(
        max_length=50,
        null=False, blank=False,
        default=None,
    )
    name_english = models.CharField(
        max_length=50,
        null=False, blank=False,
        default=None,
    )
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE,
        default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = u'Días de Semana Recurrencia'
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['name'],),
            models.Index(fields=['name_english'],),
        ]

    def __str__(self):
        return '{} - {}'.format(
            str(self.pk), (self.name or ''),
        )


class RecurrenceType(CommonStructure):

    name = models.CharField(
        max_length=50,
        null=False, blank=False,
        default=None,
    )
    name_english = models.CharField(
        max_length=50,
        null=False, blank=False,
        default=None,
    )
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE,
        default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Tipo Recurrencia"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['name'],),
            models.Index(fields=['name_english'],),
        ]

    def __str__(self):
        return '{} - {}'.format(
            str(self.pk), (self.name or ''),
        )

#####


class Reservas(CommonStructure):
    planta = models.ForeignKey('catalogos.Planta', on_delete=models.CASCADE, )
    sala = models.ForeignKey('catalogos.Sala', on_delete=models.CASCADE, )
    empleado = models.ForeignKey('metas.Empleados', on_delete=models.CASCADE,)
    fecha_evento = models.DateField(
        null=False, blank=False, default=datetime.now,)
    hora_inicio = models.TimeField(
        default=datetime.now, auto_now=False, auto_now_add=False,)
    hora_final = models.TimeField(
        default=None, auto_now=False, auto_now_add=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)
    motivo = models.CharField(max_length=100,
                              null=False, blank=False,
                              default=None,
                              )
    confirmacion = models.PositiveIntegerField(
        null=True, blank=False, default=0,)
    # added at 2020.01.19
    mgraph_enviado = models.PositiveIntegerField(
        null=True, blank=False, default=0,)
    mgraph_id = models.CharField(
        max_length=250,
        null=True, blank=True,
        default=None,
    )
    include = models.PositiveIntegerField(
        null=True, blank=False, default=0,)

    # recurrence
    recurrence = models.PositiveIntegerField(
        null=True, blank=False, default=0,)
    recurrence_from = models.DateField(
        null=True, blank=False, default=None,)
    recurrence_to = models.DateField(
        null=True, blank=False, default=None,)
    recurrence_interval = models.PositiveIntegerField(
        null=True, blank=False, default=0,)
    recurrence_day_week = models.CharField(
        max_length=200,
        null=True, blank=True,
        default=None,
    )
    recurrence_day = models.PositiveIntegerField(
        null=True, blank=False, default=0,)
    recurrence_type = models.ForeignKey(
        'reservas.RecurrenceType',
        on_delete=models.SET_NULL,
        null=True,
        default=None,
    )
    # added at 2020.02.04
    master = models.ForeignKey(
        'reservas.Reservas', on_delete=models.SET_NULL,
        default=None, null=True,
        related_name='master_set'
    )

    class Meta:
        verbose_name = "Reservas"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        # unique_together = [
        #     'sala', 'fecha_evento', 'hora_inicio', 'estado',
        # ]
        indexes = [
            models.Index(fields=['fecha_evento'],),
            models.Index(fields=['hora_inicio'],),
            models.Index(fields=['hora_final'],),
            models.Index(fields=['motivo'],),
            models.Index(fields=['confirmacion'],),
            models.Index(fields=['mgraph_enviado'],),
            models.Index(fields=['mgraph_id'],),
            models.Index(fields=['include'],),
            models.Index(fields=['recurrence'],),
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]
        permissions = (
            ("schedule_reservas", "Ver programacion Reservas "),
        )

    def __str__(self):
        return str(self.pk)

    @property
    def get_email_accounts(self):
        waccounts = ''
        try:
            witems = [
                item.empleado.email_empresarial
                for item in self.reservasparticipantes_set.all()
                if item.empleado.email_empresarial.rfind("@") > -1
            ]
            witems.append(self.empleado.email_empresarial)
            waccounts = ', '.join(witems)
        except Exception as e:
            print('Error,{}'.format(str(e)))
        return waccounts.lower()

    @property
    def get_email_participantes(self):
        wparticipantes = ''
        try:
            witems = [item.empleado.get_fullname
                      for item in self.reservasparticipantes_set.all()]
            wparticipantes = ', '.join(witems)
        except Exception as e:
            print('Error,{}'.format(str(e)))
        return wparticipantes.upper()

    @property
    def get_email_location(self):
        wlocation = ''
        try:
            wlocation = '{}, {}'.format(
                self.sala.descripcion,
                self.planta.descripcion,
            )
        except Exception as e:
            print('Error,{}'.format(str(e)))
        return wlocation

    @property
    def get_email_summary(self):
        wsummary = ''
        try:
            wschedule = '{} {} - {}'.format(
                self.fecha_evento.strftime("%a %b %d %Y"),
                str(self.hora_inicio),
                str(self.hora_final),
            )
            wsummary = "Invitacion de {} @ {}".format(
                self.empleado.get_fullname, wschedule
            )
        except Exception as e:
            print('Error,{}'.format(str(e)))
        return wsummary

    @property
    def get_email_description(self):
        wdescription = ''
        try:
            wfecha = self.fecha_evento.strftime("%a %b %d %Y")
            wrango = '{} - {}'.format(
                str(self.hora_inicio),
                str(self.hora_final)
            )
            wlabel = 'Evento en {0}, {1} a la fecha {2} {3}  ' \
                + 'organizado por {4} y participantes : {5}.'
            wdescription = wlabel.format(
                self.sala.descripcion,
                self.planta.descripcion,
                wfecha, wrango,
                self.empleado.get_fullname,
                self.get_email_participantes,
            )
        except Exception as e:
            print('Error,{}'.format(str(e)))
        return wdescription

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class ReservasParticipantes(CommonStructure):
    reserva = models.ForeignKey(
        'reservas.Reservas', on_delete=models.CASCADE, )
    empleado = models.ForeignKey('metas.Empleados', on_delete=models.CASCADE,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)

    username = None

    class Meta:
        verbose_name = "Reservas Participantes"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = [
            'reserva', 'empleado',
        ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class ReservasCalendarios(CommonStructure):
    calendar_hash = models.CharField(max_length=50, null=False, blank=False,)
    reserva = models.OneToOneField(
        'reservas.Reservas', on_delete=models.CASCADE, )
    username = None

    class Meta:
        verbose_name = "Reservas Archivo Calendario"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return self.calendar_hash

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class ReservasConfirmacion(CommonStructure):
    hashid = models.CharField(max_length=45, null=True,
                              blank=True, default=None,)
    reserva = models.ForeignKey('reservas.Reservas', on_delete=models.CASCADE,
                                null=False, blank=False,)
    procesado = models.PositiveIntegerField(null=True, blank=False, default=0,)
    fecha_procesado = models.DateTimeField(
        null=True, auto_now=False, auto_now_add=False,)
    username = None

    class Meta:
        verbose_name = "Reservas Envios Confirmacion"
        default_permissions = ()
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['hashid'],),
            models.Index(fields=['procesado'],),
            models.Index(fields=['fecha_procesado'],),
        ]

    def __str__(self):
        return '{}'.format(self.hashid)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")


class ReservasEnvios(CommonStructure):
    calendar = models.ForeignKey(
        'reservas.ReservasCalendarios', on_delete=models.CASCADE, )
    motivo = models.CharField(max_length=100, null=False, blank=False,)
    email_from = models.CharField(max_length=50, null=False, blank=False,)
    email_to = models.TextField(null=False, blank=False,)

    username = None

    class Meta:
        verbose_name = "Reservas Envios Mail"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return '{}|{}'.format(
            self.calendar__calendar_hash,
            self.motivo,
        )

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Feriados(CommonStructure):
    fecha = models.DateField(null=False, blank=False, default=None,)
    descripcion = models.CharField(max_length=100, null=False, blank=False,
                                   default=None,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Feriados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['fecha'],),
            models.Index(fields=['fhregistro'],),
        ]

    def __str__(self):
        return str(self.fecha)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)
