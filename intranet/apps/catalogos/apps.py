from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CatalogosConfig(AppConfig):
    name = 'apps.catalogos'
    verbose_name = _('catalogos')

    def ready(self):
        from apps.catalogos import signals
