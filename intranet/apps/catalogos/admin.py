from django.contrib import admin
from apps.catalogos import models as mdls
from apps.catalogos import forms as frm

# Register your models here.


class MenusParentAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion", "icon_class",
        "orden",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion", "orden",
        "icon_class",
        "estado",
        "fhregistro",
        "fhmodificacion",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "descripcion",
        "estado__descripcion"
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class MenusDetalleAdmin(admin.ModelAdmin):
    form = frm.MenusDetallePermisosForm
    list_display = [
        "id", "descripcion", "parent", "view_name",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]

    fieldsets = (
        ('Información General', {
            'fields':  [
                "id", "descripcion", "icon_class",
                "view_name", "parent", "estado",
            ],
        }),
        ('Permisos por Modulo', {
            'fields': [
                "permisos",
            ]
        }),
        ('Fechas Importantes', {
            'fields':  [
                "fhregistro", "fhmodificacion",
            ],
        }),
    )
    filter_horizontal = ['permisos', ]
    # inlines = [
    #     DetallePermisosInline,
    # ]
    readonly_fields = [
        "id",
        "fhregistro", "fhmodificacion",
    ]
    search_fields = [
        "id", "descripcion",
        "parent__descripcion",
        "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class PlantaAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "descripcion",
        "estado__descripcion"
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class SalaAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion", "planta", "fecha_vigencia",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion", "planta",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "descripcion",
        "planta__descripcion",
        "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class MesesAdmin(admin.ModelAdmin):
    list_display = [
        "id", "mes", "descripcion", "descripcion_english",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "mes", "descripcion"]
    fields = [
        "id", "mes", "descripcion", "descripcion_english",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "mes", "descripcion",
        "descripcion_english", "estado__descripcion"
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class AreaAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "descripcion", "estado__descripcion"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class SucursalAdmin(admin.ModelAdmin):
    list_display = [
        "id", "cod_sucursal", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "cod_sucursal", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "descripcion",
                     "cod_sucursal", "estado__descripcion"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class ParametersAdmin(admin.ModelAdmin):
    list_display = [
        "id", "parameter", "descripcion",
        "valor", "default",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion", 'parameter', ]
    fields = [
        "id", "parameter", "descripcion",
        "valor", "default",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "descripcion", "estado__descripcion"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"

wmodels = [
    mdls.Sucursal, mdls.Area, mdls.Meses,
    mdls.Planta, mdls.Sala,
    mdls.MenusDetalle,
    mdls.MenusParent,
    mdls.Parameters,
]
winterfaces = [
    SucursalAdmin, AreaAdmin, MesesAdmin,
    PlantaAdmin, SalaAdmin,
    MenusDetalleAdmin,
    MenusParentAdmin,
    ParametersAdmin,
]
for windex, witem in enumerate(wmodels):
    admin.site.register(wmodels[windex], winterfaces[windex])

"""
class DetallePermisosInline(admin.TabularInline):
    pass
    verbose_name = "Permisos Adicionales"
    model = mdls.MenusDetallePermisos
    exclude = ['fhregistro','fhmodificacion',]
    extra=0
    min_num = 0
    max_num = 10
    filter_horizontal = ['permiso',]
"""
