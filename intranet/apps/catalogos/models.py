from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from django.db.models.manager import EmptyManager
from decimal import Decimal
from django.utils import timezone
# Create your models here.

DEFAULT_STATUS = 1


class Parameters(CommonStructure):
    parameter = models.CharField(max_length=25,
                                 null=False,
                                 blank=False,)
    descripcion = models.CharField(max_length=150,
                                   null=False,
                                   blank=False,)
    valor = models.CharField(max_length=100,
                             null=True,
                             blank=True,
                             default=None,)
    default = models.CharField(max_length=100,
                               null=True,
                               blank=True,
                               default=None,)
    estado = models.ForeignKey('metas.Estados',
                               on_delete=models.CASCADE,
                               default=DEFAULT_STATUS,)

    class Meta:
        verbose_name = "Parameters"
        ordering = ['fhregistro', ]
        get_latest_by = ['fhregistro', ]
        default_permissions = ()
        indexes = [
            models.Index(fields=['parameter'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return str(self.parameter)


class GroupExtension(models.Model):
    group = models.OneToOneField(
        'auth.Group', on_delete=models.CASCADE, null=False, blank=False,
    )
    is_active = models.PositiveIntegerField(default=1, null=True, blank=False,)
    fhregistro = models.DateTimeField(
        null=True, default=timezone.now, blank=False)
    fhmodificacion = models.DateTimeField(null=True, default=None, blank=False)

    class Meta:
        verbose_name = "Grupo Extension"
        default_permissions = ()
        permissions = (
            ("read_group", "Can read group"),
        )
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.group.pk)


class UserExtension(models.Model):
    username = models.OneToOneField(
        'auth.User', on_delete=models.CASCADE, null=False, blank=False,
    )
    is_lock = models.PositiveIntegerField(null=False, blank=False,
                                          default=0,
                                          validators=[
                                              MaxValueValidator(
                                                  1), MinValueValidator(0)
                                          ])
    is_new = models.PositiveIntegerField(null=False, blank=False,
                                         default=1,
                                         validators=[
                                             MaxValueValidator(
                                                 1), MinValueValidator(0)
                                         ])
    fecha_bloqueo = models.DateTimeField(
        null=True, blank=False, default=None,
    )
    fecha_cambio_clave = models.DateTimeField(
        null=True, blank=False, default=None,
    )

    dni = models.CharField(
        null=True,
        max_length=25,
        blank=True,
        default=None,
    )
    fhmodificacion = models.DateTimeField(
        default=None, null=True, blank=False,)
    check_all = models.PositiveIntegerField(
        default=2, null=True, blank=False,)
    salas_all = models.PositiveIntegerField(
        default=2, null=True, blank=False,)

    class Meta:
        verbose_name = "Usuario Extension"
        default_permissions = ()
        permissions = (
            ("read_user", "Can read user"),
        )
        indexes = [
            models.Index(fields=['fhmodificacion'],),
        ]


class RecuperoClave(CommonStructure):
    email = models.EmailField(blank=False, null=False,)
    hashid = models.CharField(max_length=25, null=False, blank=False,)
    fecha_vigencia = models.DateTimeField(
        null=False, blank=False, default=datetime.now,
    )
    fecha_recupero = models.DateTimeField(
        null=True, blank=False, default=None,
    )

    class Meta:
        verbose_name = "Recupero Clave"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = ['hashid', 'email', ]
        indexes = [
            models.Index(fields=['fecha_vigencia'],),
            models.Index(fields=['fecha_recupero'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return self.hashid


class MenusParent(CommonStructure):
    descripcion = models.CharField(
        max_length=50, null=False, blank=False, )
    icon_class = models.CharField(
        max_length=50, null=False, blank=False, default='fas fa-globe')
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.SET_NULL,
        null=True, default=DEFAULT_STATUS)
    orden = models.PositiveIntegerField(
        null=False, blank=False, default=0,)

    class Meta:
        verbose_name = "Menus Parent"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['orden'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class MenusDetalle(CommonStructure):
    descripcion = models.CharField(
        max_length=50, null=False, blank=False, )
    view_name = models.CharField(
        max_length=50, null=False, blank=False,)
    icon_class = models.CharField(
        max_length=50, null=False, blank=False, default='fas fa-globe')
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.SET_NULL,
        null=True, default=DEFAULT_STATUS)
    parent = models.ForeignKey(
        'catalogos.MenusParent',
        on_delete=models.SET_NULL,
        null=True,
        related_name="detalles",
    )
    permisos = models.ManyToManyField(
        'auth.permission',
        verbose_name=('Permisos por Menu Detalle'),
        blank=True,
        help_text=('Permisos especificos por Menu Detalle.'),
    )

    class Meta:
        verbose_name = "Menus Detalle"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Meses(CommonStructure):
    mes = models.PositiveIntegerField(
        null=False, blank=False,
        default=None,
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1)
        ],
    )
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    descripcion_english = models.CharField(
        max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None
    fregistro = None

    class Meta:
        verbose_name = "Descripcion mes del año"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        unique_together = ['mes', 'descripcion', 'estado', ]
        indexes = [
            models.Index(fields=['mes'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['descripcion_english'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def fregistro(self):
        raise AttributeError("'Manager' object has no attribute 'fregistro'")

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Planta(CommonStructure):
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Numero de Planta / Piso"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Sala(CommonStructure):
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    limite_participantes = models.IntegerField(
        null=True, blank=False, default=1,
        validators=[MaxValueValidator(9999), MinValueValidator(1)],
    )
    fecha_vigencia = models.DateField(null=True, blank=True, default=None)
    planta = models.ForeignKey(
        'catalogos.Planta', on_delete=models.CASCADE)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    hora_inicio = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True,)
    hora_final = models.TimeField(
        default=None, auto_now=False, auto_now_add=False, null=True,)

    class Meta:
        verbose_name = "Sala / Ambiente de reuniones"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fecha_vigencia'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Area(CommonStructure):
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None

    class Meta:
        verbose_name = "Areas"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Sucursal(CommonStructure):
    cod_sucursal = models.IntegerField(null=False, blank=False,)
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None

    class Meta:
        verbose_name = "Sucursales"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['cod_sucursal'],),
            models.Index(fields=['descripcion'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class UserSala(CommonStructure):
    sala = models.ForeignKey('catalogos.Sala',
                             on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User',
                             on_delete=models.CASCADE,
                             related_name="%(app_label)s_%(class)s_user_set",
                             )

    class Meta:
        verbose_name = "Usuario Sala"
        ordering = ['fhregistro', ]
        get_latest_by = ['fhregistro', ]
        default_permissions = ()
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    def __str__(self):
        return str(self.pk)
