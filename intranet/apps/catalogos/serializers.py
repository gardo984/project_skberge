#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.catalogos import models as m
from apps.metas import serializers as srlmetas
from apps.common import serializers as srlcommon


class SucursalSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Sucursal
        fields = '__all__'


class AreaSerializer(srlcommon.DynamicFieldsModelSerializer):

    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Area
        fields = '__all__'


class PlantaSerializer(srlcommon.DynamicFieldsModelSerializer):

    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Planta
        fields = '__all__'


class SalaSerializer(srlcommon.DynamicFieldsModelSerializer):
    user_id = serializers.IntegerField()
    allowed = serializers.IntegerField()
    planta = PlantaSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.Sala
        fields = '__all__'


class MenusDetalleSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.MenusDetalle
        fields = '__all__'


class MenusParentSerializer(srlcommon.DynamicFieldsModelSerializer):
    estado = srlmetas.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)
    detalles = MenusDetalleSerializer(
        fields=[
            'id', 'descripcion', 'view_name', 'icon_class',
        ],
        read_only=True,
        many=True,
    )

    class Meta:
        model = m.MenusParent
        fields = '__all__'
