
from apps.catalogos import models as mdls
from django import forms
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F


class MenusDetallePermisosForm(forms.ModelForm):

    class Meta:
        model = mdls.MenusDetalle
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MenusDetallePermisosForm, self).__init__(*args, **kwargs)
        Wexceptions = ["admin", "contenttypes", "sessions"]
        Qlist = Q(content_type__app_label__in=Wexceptions)
        self.fields[
            "permisos"].queryset = mdlauth.Permission.objects.exclude(Qlist)

"""
class MenusDetalleForm(forms.ModelForm):

	fields_readonly = [
		"id", "fhmodificacion", "fhregistro",
		"icon_class",
	]
	class Meta:
		model = mdls.MenusDetalle
		fields = [
			"id","descripcion",
			"icon_class", "view_name",
			"parent", "fhregistro",
			"fhmodificacion", "estado",
		]
		# widgets = {
		# 	"fhregistro" : 
		# }


	def __init__(self, *args, **kwargs):
		super(MenusDetalleForm, self).__init__(*args, **kwargs)
		instance = getattr(self, 'instance', None)
		if instance and instance.pk:
			self.fields['permisos']= mdls
			# pass
			# self.fields['icon_class'].widget.attrs['readonly'] = True
			# for item in self.fields_readonly:
			# 	if item in self.fields:
			# 		self.fields[item].widget.attrs['disabled'] = True
"""
