from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from apps.connections.config import (
    SQLConnection,
    MYSQLConnection,
)
from apps.api.querys import (
    cmd_obtener_ventas,
    cmd_obtener_ventas_anuladas,
    cmd_obtener_empleados,
    cmd_metas_obtener_ventas,
    cmd_metas_eliminar_empleados,
    cmd_metas_transfer_empleados,
)
from apps.metas import (
    models as mdl,
    serializers as srz,
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srzcat,
)
from intranet import settings
from datetime import datetime
from django.db.models import (
    Count, Max, Min, Avg, Sum, Q, F,
    Prefetch,
    OuterRef,
    Exists,
    Case, When,
    Value,
    IntegerField, CharField,
)

# To generate and modify a new xls file
from openpyxl import load_workbook
from shutil import copyfile
import os

# To subtract month from date value
from dateutil.relativedelta import relativedelta
from calendar import monthrange

# To Send Email
from apps.mail.views import SendMailer
from apps.common.utils import Validators
from django.contrib.auth import models as mdlauth
from apps.login import (
    serializers as srlzlog,
)
# added at 2020.02.01
from apps.reservas import (
    models as mdlre,
    serializers as srlzre,
)

# Create your views here.

defaultResponse = {
    "ok": 1,
    "result": 'Request has been processed successfully.',
    "status": 200,
}


# added at 2020.02.01
class APIGetRecurrenceWeekDays(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = (
                mdlre.DaysOfWeek.objects.filter(
                    estado=1,
                ).select_related("estado")
            )
            wfields = [
                'id', 'name', 'name_english',
            ]
            srzoutcome = srlzre.DaysOfWeekSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("name") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

# added at 2020.02.01


class APIGetRecurrence(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            outcome = (
                mdlre.RecurrenceType.objects.filter(
                    estado=1,
                ).select_related("estado")
            )
            wfields = [
                'id', 'name', 'name_english',
            ]
            srzoutcome = srlzre.RecurrenceTypeSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("name") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetPerfilPermisos(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = request.GET
            wgroupname = None
            if wdata.get("uid"):
                wgroupname = wdata.get("uid")
                objgroup = mdlauth.Group.objects.filter(id__iexact=wgroupname)
                if not objgroup.exists():
                    raise ValueError(str('Error, perfil no existe'))

            fields, error = self.obtener_fields(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            permisos, error = self.obtener_permisos(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def obtener_fields(self, groupname):
        result, error = None, None
        try:
            if not groupname is None:
                objgroup = mdlauth.Group.objects \
                    .filter(id__iexact=groupname) \
                    .select_related("groupextension") \
                    .first()
                wfields = ["id", "name", ]
                srlzoutcome = srlzlog.GroupSerializer(objgroup, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_permisos(self, groupname):
        result, error = [], None
        try:
            if groupname is None:
                wgroup_id, wgroup_grants = None, []
            else:
                objgroup = mdlauth.Group.objects.filter(
                    id__iexact=groupname).first()
                wgroup_grants = [x.id
                                 for x in objgroup.permissions.all()
                                 .prefetch_related('content_type')]
                wgroup_id = objgroup.id

            woutcome = mdlauth.Permission.objects.all() \
                .prefetch_related("content_type") \
                .annotate(group_id=Case(
                    When(id__in=wgroup_grants,
                         then=Value(wgroup_id)),
                    default=None,
                    output_field=IntegerField()
                )).order_by("content_type_id")

            wfields = ["id", "name", "group_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("name"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]


class APIGetUsuarioPermisos(APIView):

    def get(self, request):
        wresponse = defaultResponse.copy()
        try:
            wdata = request.GET
            wusername = None
            if wdata.get("uid"):
                wusername = wdata.get("uid")
                objuser = mdlauth.User.objects.filter(
                    username__iexact=wusername)
                if not objuser.exists():
                    raise ValueError(str('Error, usuario no existe'))

            fields, error = self.obtener_fields(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            permisos, error = self.obtener_permisos(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            perfiles, error = self.obtener_perfiles(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            permisos_salas, error = self.obtener_perm_salas(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            permisos_marcas = []
            permisos_concesionarios = []
            # permisos_marcas, error = self.obtener_perm_marcas(wusername)
            # if error:
            #     wresponse["status"] = 422
            #     raise ValueError(str(error))
            # permisos_concesionarios, error = self.obtener_perm_concesionarios(
            #     wusername)
            # if error:
            #     wresponse["status"] = 422
            #     raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
                "perfiles": perfiles,
                "marcas": permisos_marcas,
                "concesionarios": permisos_concesionarios,
                "salas": permisos_salas,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def obtener_fields(self, username):
        result, error = None, None
        try:
            if not username is None:
                objuser = mdlauth.User.objects \
                    .filter(username__iexact=username) \
                    .select_related("userextension") \
                    .first()

                wfields = [
                    "id", "first_name", "last_name", "email",
                    "userextension", "is_active", "username",
                    "fullname",
                ]
                srlzoutcome = srlzlog.UserSerializer(
                    objuser, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perfiles(self, username):
        result, error = None, None
        try:
            if username is None:
                wuser_id, wuser_profiles = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_profiles = [x.id for x in wuser.groups.all()]

            woutcome = mdlauth.Group.objects.all() \
                .annotate(user_id=Case(
                    When(id__in=wuser_profiles, then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                ))

            wfields = ["id", "name", "user_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True)
            result = srlzoutcome.data
            for item in result:
                item["label"] = item.get("name")
                item["key"] = item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_permisos(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_grants = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_grants = [x.codename
                                for x in mdlauth.Permission.objects.filter(
                                    user__username__iexact=username)]

            woutcome = mdlauth.Permission.objects \
                .prefetch_related('content_type') \
                .annotate(user_id=Case(
                    When(codename__in=wuser_grants,
                         then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                )).order_by("content_type")

            wfields = [
                "id", "name", "codename", "content_type", "user_id",
            ]
            srlzoutcome = srlzlog.PermissionSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("name"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perm_salas(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_salas = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_salas = [x.sala_id
                               for x in wuser.catalogos_usersala_user_set.all()]

            Qfilters = Q(estado__id=1,)
            woutcome = mdlcat.Sala.objects.filter(Qfilters) \
                .select_related("estado") \
                .select_related("planta") \
                .annotate(user_id=Case(
                    When(id__in=wuser_salas, then=Value(wuser_id)),
                    default=Value(None),
                    output_field=IntegerField()
                ))

            wfields = [
                "id", "descripcion", "planta", "user_id",
            ]
            srlzoutcome = srzcat.SalaSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get(
                    "descripcion"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perm_marcas(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_marcas = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_marcas = [x.marca_id
                                for x in wuser.catalogos_usermarca_user_set.all()]

            woutcome = mdl.Marca.objects.all() \
                .select_related("estado") \
                .annotate(user_id=Case(
                    When(id__in=wuser_marcas, then=Value(wuser_id)),
                    default=Value(None),
                    output_field=IntegerField()
                ))

            wfields = [
                "id", "nombre", "cod_marca", "user_id",
            ]
            srlzoutcome = srz.MarcaSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("nombre"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]

    def obtener_perm_concesionarios(self, username):
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_cons = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_cons = [x.concesionario_id
                              for x in wuser.catalogos_userconcesionario_user_set.all()]

            woutcome = mdl.Concesionario.objects.all() \
                .select_related("estado") \
                .annotate(user_id=Case(
                    When(id__in=wuser_cons, then=Value(wuser_id)),
                    default=Value(None),
                    output_field=IntegerField()
                ))

            wfields = [
                "id", "nombre", "user_id",
            ]
            srlzoutcome = srz.ConcesionarioSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("nombre"), item.get("id")

        except Exception as e:
            error = str(e)
        return [result, error]


class APIGetEstados(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdl.Estados.objects.filter(estado=1)
            wfields = [
                'id', 'descripcion', 'estado',
            ]
            srzoutcome = srz.EstadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetSalas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = request.GET
            Qfilters = Q(id__isnull=False,)
            wgrants = int(wdata.get("grants") or 0)
            # get list by grants
            if wgrants == 1:
                if request.user.is_authenticated:
                    wsalas = [x.sala_id
                              for x in request.user.catalogos_usersala_user_set.all()]
                    Qfilters.add(Q(id__in=wsalas,), Qfilters.connector)

            # get list by status
            if wdata.get("status"):
                westado = wdata.get("status")
                Qfilters.add(
                    Q(estado=mdl.Estados.objects.get(id__iexact=westado)),
                    Qfilters.connector
                )
                # if not Validators.isNumber(westado):
                #     raise ValueError('Error, tipo de dato no es valido.')
                # objEstado = mdl.Estados.objects.filter(id=int(westado))
                # if not objEstado.exists():
                #     raise ValueError('Error, estado no existe.')
                # wfilters['estado'] = objEstado.get()

            # modified at 2020.01.18
            outcome = (
                mdlcat.Sala.objects.filter(Qfilters)
                .select_related("estado")
                .select_related("planta")
            )
            wfields = [
                'id', 'descripcion', 'estado',
                'planta', 'limite_participantes',
            ]
            srzoutcome = srzcat.SalaSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetPlantas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdlcat.Planta.objects.filter(estado=1)
            wfields = [
                'id', 'descripcion', 'estado',
            ]
            srzoutcome = srzcat.PlantaSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APISendEmailMetas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            connection = MYSQLConnection()
            wcurrent_date = (datetime.now() + relativedelta(months=-1))
            wcmd = cmd_metas_obtener_ventas.format(wcurrent_date.month, 'null')
            werror, wresult = connection.get_query_dict(wcmd)
            if werror:
                raise ValueError(str(werror))
            # generate xls files
            error, wresult = self.generate_files(wresult, request)
            if error:
                raise ValueError(str(error))
            # send emails
            error, wresult = self.send_emails(wresult)
            if error:
                raise ValueError(str(error))
            wresponse["result"] = wresult
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def generate_files(self, items, request):
        error = None
        try:
            wstatic = os.path.join(settings.BASE_DIR, 'static', 'others')
            wmedia = os.path.join(settings.BASE_DIR, 'media', 'metas_report')
            wtemplate = os.path.join(wstatic, 'template_metas.xlsx')
            for item in items:
                winterval = datetime.now().strftime("%Y%m%d%H%M%S%f")
                wfilename = 'xls_{}.xlsx'.format(winterval)
                xlsfile = os.path.join(wmedia, wfilename)
                copyfile(wtemplate, xlsfile)
                wb = load_workbook(xlsfile)
                ws = wb.active
                ws.title = item.get("empleado_nombres") or ''
                wcells = ["B2", "E4", "D18", "D19", "D20"]
                wvalues = [
                    "empleado_nombres", "sueldo",
                    "meta", "nventas", "nventas_anuladas"
                ]
                for windex, wcell in enumerate(wcells):
                    ws[wcell].value = item.get(wvalues[windex])
                wb.save(xlsfile)
                wb.close()
                item["urlpath"] = request.build_absolute_uri(
                    os.path.join(settings.MEDIA_URL, 'metas_report', wfilename)
                )
        except Exception as e:
            error = str(e)
        return [error, items]

    def send_emails(self, items):
        error = None
        try:
            for item in items:
                wcontext = {
                    'content_header': 'Metas de ventas {}'.format(item.get("empleado_nombres")),
                    'content_body': 'Revisar reporte generado en {}.'.format(item.get("urlpath")),
                }
                wsettings = {
                    'subject': wcontext.get("content_header"),
                    'body': wcontext.get("content_body"),
                    "to": list([item.get("empleado_email")], ) or list(),
                    "template": 'mail/mail_template.html',
                    "context": wcontext,
                }
                objMail = SendMailer(**wsettings)
                error, status = objMail.BuildMessage()
                if error:
                    print(str(error))
                objMail.Send()
        except Exception as e:
            error = str(e)
        return [error, items]


class APIMetasEmpleados(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdl.Empleados.objects.filter(
                estado=1,
            )
            wfields = [
                'id', 'ndoc', 'nombres', 'apellidos',
                "email_empresarial",
            ]
            srzoutcome = srz.EmpleadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                wnombres = "{} {}".format(
                    item.get('nombres'), item.get('apellidos'))
                item["value"] = wnombres
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetJefes(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdl.Empleados.objects.filter(
                id_puesto__descripcion='JEFE COMERCIAL',
                estado=1,
            )
            wfields = [
                'id', 'ndoc', 'nombres', 'apellidos',
            ]
            srzoutcome = srz.EmpleadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                wnombres = "{} {}".format(
                    item.get('nombres'), item.get('apellidos'))
                item["value"] = wnombres
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetMarcas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            outcome = mdl.Marca.objects.filter(estado=1)
            wfields = [
                'id', "cod_marca", "nombre",
            ]
            srzoutcome = srz.MarcaSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["value"] = item.get('nombre')
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetVentas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        connection = SQLConnection()
        mysql_connection = MYSQLConnection()
        wresponse = self.defaultResponse.copy()
        wcurrent_date = (datetime.now() + relativedelta(months=-1))
        wlastday = monthrange(wcurrent_date.year, wcurrent_date.month)[1]
        wdate_from = wcurrent_date.strftime("%Y%m01")
        wdate_to = '{0}{1}'.format(wcurrent_date.strftime('%Y%m'), wlastday)

        # parameters add at 2018.08.31
        wdata = request.GET
        if wdata.get("date_from"):
            wdate_from = wdata.get("date_from")
        if wdata.get("date_to"):
            wdate_to = wdata.get("date_to")

        try:
            queryset = mdl.TablesTransfer.objects.filter(
                estado__id=1, table='metas_cuponesmodel')
            woutcome = list()
            for item in queryset:
                wcmd = item.command_source.format(wdate_from, wdate_to)
                werror, wresult = connection.get_query_with_headers(wcmd)
                if werror:
                    raise ValueError(str(werror))

                if item.additional_commands or '' != '':
                    error, status = mysql_connection.execute_query(
                        item.additional_commands.format(wdate_from, wdate_to),
                        True
                    )
                    if error:
                        raise ValueError(str(error))

                woutcome.append(wresult.get('rows'))
                wrows = [tuple(item.values()) for item in wresult.get('rows')]
                wcolumns = item.fields.split(',')
                wparameters = {"columns":  wcolumns, "rows": wrows}
                mdl.CuponesModel().insert_bulk(**wparameters)
                print("{0} fueron agregados satisfactoriamente :{1} registros.".format(
                    item.comments, len(wresult.get('rows'))
                ))

            wresponse['result'] = {
                "ventas": woutcome[0],
                "ventas_anulados": woutcome[1],
            }
            return Response(wresponse, status=wresponse.get("status"))

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdateMarcas(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_marcas()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_marcas(self):
        wresponse = self.defaultResponse.copy()
        try:
            witems = mdl.CuponesModel.objects.values(
                'cod_marca', 'marca').annotate(nrows=Count('id'))
            for item in witems:
                obj, status = mdl.Marca.objects.get_or_create(
                    cod_marca=item.get('cod_marca'), nombre=item.get("marca"))

            woutcome = srz.MarcaSerializer(
                mdl.Marca.objects.all(), many=True,
            )
            wresponse['result'] = {
                "message": "Actualizacion de Marcas fue realizado satisfactoriamente.",
                "outcome": 	woutcome.data,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdateConcesionarios(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_concesionarios()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_concesionarios(self):
        wresponse = self.defaultResponse.copy()
        try:
            witems = mdl.CuponesModel.objects \
                .values('ruc_concesionario', 'nombre_concesionario') \
                .annotate(nrows=Count('id'))
            for item in witems:
                obj, status = mdl.Concesionario.objects.get_or_create(
                    ndoc=item.get('ruc_concesionario'),
                    nombre=item.get("nombre_concesionario")
                )

            wfields = ["id", "nombre", "ndoc", "estado"]
            woutcome = srz.ConcesionarioSerializer(
                mdl.Concesionario.objects.all(),
                many=True,
                fields=wfields,
            )
            wresponse['result'] = {
                "message": "Actualizacion de Concesionarios fue realizado satisfactoriamente.",
                "outcome": woutcome.data,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetEmpleados(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        connection = SQLConnection()
        mysql_connection = MYSQLConnection()
        wresponse = self.defaultResponse.copy()
        try:

            error, status = mysql_connection.execute_query(
                cmd_metas_eliminar_empleados)
            if error:
                raise ValueError(str(error))

            queryset = mdl.TablesTransfer.objects.filter(
                estado__id=1, table='metas_empleadosmodel')
            woutcome = list()
            for item in queryset:
                wcmd = item.command_source
                werror, wresult = connection.get_query_with_headers(wcmd)
                if werror:
                    raise ValueError(str(werror))
                woutcome.append(wresult.get('rows'))
                wrows = [tuple(item.values()) for item in wresult.get('rows')]
                wcolumns = item.fields.split(',')
                wparameters = {"columns":  wcolumns, "rows": wrows}
                mdl.EmpleadosModel().insert_bulk(**wparameters)
                print("{0} fueron agregados satisfactoriamente :{1} registros.".format(
                    item.comments, len(wresult.get('rows'))
                ))

            wresponse['result'] = woutcome[0]
            return Response(wresponse, status=wresponse.get("status"))

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdateEmpleados(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Request has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_empleados()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_empleados(self):
        wresponse = self.defaultResponse.copy()
        try:
            connection = MYSQLConnection()
            wcmd = cmd_metas_transfer_empleados
            werror, wresult = connection.get_query_dict(wcmd)
            if werror:
                raise ValueError(str(werror))
            wresponse['result'] = {
                "message": "Actualizacion de Empleados fue realizado satisfactoriamente.",
                "outcome":  wresult,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdateSucursales(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_sucursales()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_sucursales(self):
        wresponse = self.defaultResponse.copy()
        try:
            witems = mdl.CuponesModel.objects \
                .values('cod_sucursal', 'nombre_sucursal') \
                .annotate(nrows=Count('id'))
            for item in witems:
                obj, status = mdlcat.Sucursal.objects.get_or_create(
                    cod_sucursal=item.get('cod_sucursal'),
                    descripcion=item.get("nombre_sucursal")
                )

            wfields = ["id", "cod_sucursal", "descripcion", "estado"]
            woutcome = srzcat.SucursalSerializer(
                mdlcat.Sucursal.objects.all(),
                many=True,
                fields=wfields,
            )
            wresponse['result'] = {
                "message": "Actualizacion de Sucursales fue realizado satisfactoriamente.",
                "outcome": woutcome.data,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdateArea(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_areas()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_areas(self):
        wresponse = self.defaultResponse.copy()
        try:
            witems = mdl.EmpleadosModel.objects \
                .values('usr_area') \
                .annotate(nrows=Count('id'))
            for item in witems:
                obj, status = mdlcat.Area.objects.get_or_create(
                    descripcion=item.get('usr_area'),
                )

            wfields = ["id", "descripcion", "estado"]
            woutcome = srzcat.AreaSerializer(
                mdlcat.Area.objects.all(),
                many=True,
                fields=wfields,
            )
            wresponse['result'] = {
                "message": "Actualizacion de Areas fue realizado satisfactoriamente.",
                "outcome": woutcome.data,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIUpdatePuestos(APIView):
    defaultResponse = {
        "ok": 1,
        "result": 'Process has been processed successfuly.',
        "status": 200,
    }

    def get(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            return self.transfer_puestos()
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))

    def transfer_puestos(self):
        wresponse = self.defaultResponse.copy()
        try:
            witems = mdl.EmpleadosModel.objects \
                .values('usr_puesto') \
                .annotate(nrows=Count('id'))
            for item in witems:
                obj, status = mdl.Puestos.objects.get_or_create(
                    descripcion=item.get('usr_puesto'),
                )

            wfields = ["id", "descripcion", "estado"]
            woutcome = srz.PuestosSerializer(
                mdlcat.Area.objects.all(),
                many=True,
                fields=wfields,
            )
            wresponse['result'] = {
                "message": "Actualizacion de Puestos fue realizado satisfactoriamente.",
                "outcome": woutcome.data,
            }
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))
