from django.contrib import admin
from apps.metas import models as mdls
# site.register your models here.


class TablesTransferAdmin(admin.ModelAdmin):
    list_display = [
        "id", "table", "comments",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "table"]
    # fields = [
    #     "id", "descripcion",
    #     "fhregistro", "fhmodificacion",
    #     "estado",
    # ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "table", "comments",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class EmpleadosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "nombres", "apellidos",
        "ndoc", "email_personal", "id_puesto",
        "usuario_intranet",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "nombres"]
    # fields = [
    #     "id", "descripcion",
    #     "fhregistro", "fhmodificacion",
    #     "estado",
    # ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "nombres", "apellidos", "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class EstadosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "descripcion", "estado"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class PuestosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "descripcion", "estado__descripcion"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class MarcaAdmin(admin.ModelAdmin):
    list_display = [
        "id", "cod_marca", "nombre",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "cod_marca", "nombre"]
    fields = [
        "id", "cod_marca", "nombre",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "cod_marca", "nombre"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class ConcesionarioAdmin(admin.ModelAdmin):
    list_display = [
        "id", "nombre", "ndoc",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "nombre"]
    fields = [
        "id", "nombre", "ndoc",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = ["id", "nombre", "ndoc"]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"

admin.site.register(mdls.Marca, MarcaAdmin)
admin.site.register(mdls.Concesionario, ConcesionarioAdmin)
admin.site.register(mdls.Puestos, PuestosAdmin)
admin.site.register(mdls.Estados, EstadosAdmin)
admin.site.register(mdls.Empleados, EmpleadosAdmin)
admin.site.register(mdls.MarcaJefeConcesionario)
admin.site.register(mdls.MetaJefe)
admin.site.register(mdls.MetaMarca)
admin.site.register(mdls.TablesTransfer, TablesTransferAdmin)
