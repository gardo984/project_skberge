#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.metas import models as m
from apps.common import serializers as srlcommon


class EstadosSerializer(srlcommon.DynamicFieldsModelSerializer):

    class Meta:
        model = m.Estados
        fields = '__all__'


class PuestosSerializer(srlcommon.DynamicFieldsModelSerializer):

    class Meta:
        model = m.Puestos
        fields = '__all__'


class MarcaSerializer(srlcommon.DynamicFieldsModelSerializer):
    # user_id = serializers.IntegerField()
    estado = EstadosSerializer(
        fields=["id", "descripcion", "estado", ],
        read_only=True,
    )

    class Meta:
        model = m.Marca
        fields = '__all__'


class ConcesionarioSerializer(srlcommon.DynamicFieldsModelSerializer):

    # asignado_status = serializers.SerializerMethodField()
    asignado = serializers.CharField()
    estado = EstadosSerializer(
        fields=["id", "descripcion", "estado", ],
        read_only=True,
    )

    class Meta:
        model = m.Concesionario
        fields = '__all__'

    # def get_asignado_status(self,obj):
    #     return self.asignado or 0


class EmpleadosSerializer(srlcommon.DynamicFieldsModelSerializer):

    fullname = serializers.SerializerMethodField()

    class Meta:
        model = m.Empleados
        fields = '__all__'

    def get_fullname(self, obj):
        wlast_name = obj.apellidos or ''
        wfirst_name = obj.nombres or ''
        return '{} {}'.format(wfirst_name, wlast_name)


class MetaJefeSerializer(srlcommon.DynamicFieldsModelSerializer):

    jefe = EmpleadosSerializer(
        fields=['id', 'ndoc', 'nombres', 'apellidos', ],
        read_only=True
    )
    marca = MarcaSerializer(
        fields=['id', 'nombre', 'cod_marca', ],
        read_only=True
    )

    class Meta:
        model = m.MetaJefe
        fields = '__all__'
