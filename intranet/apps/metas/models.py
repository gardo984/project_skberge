from django.db import models
from apps.common.models import CommonStructure
from apps.connections.config import MYSQLConnection
from datetime import datetime
from decimal import Decimal
# Create your models here.


DEFAULT_STATUS = 1


class Estados(CommonStructure):
    descripcion = models.CharField(max_length=200, null=False, blank=False,)
    estado = models.IntegerField(null=False, default=1, blank=False)

    class Meta:
        verbose_name = "Estados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Marca(CommonStructure):
    nombre = models.CharField(max_length=200, null=False, blank=False,)
    cod_marca = models.CharField(max_length=25, null=False, blank=False,)
    estado = models.ForeignKey(
        Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Marca"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['nombre'],),
            models.Index(fields=['cod_marca'],),
        ]

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Concesionario(CommonStructure):
    nombre = models.CharField(max_length=200, null=False, blank=False,)
    ndoc = models.CharField(max_length=25, null=False, blank=False,)
    estado = models.ForeignKey(
        Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None

    class Meta:
        verbose_name = "Concesionario"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['nombre'],),
            models.Index(fields=['ndoc'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Puestos(CommonStructure):
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    estado = models.ForeignKey(
        Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)
    username = None

    class Meta:
        verbose_name = "Puestos"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class Empleados(CommonStructure):
    OPTIONS_TIPO_EMPLEADO = (
        (1, 'INTERNO'),
        (2, 'EXTERNO'),
    )

    ndoc = models.CharField(max_length=25, null=True, blank=True,)
    nombres = models.CharField(max_length=200, null=False, blank=False,)
    apellidos = models.CharField(max_length=200, null=False, blank=True,)
    fecha_nacimiento = models.DateField(null=True, blank=True, default=None)
    email_empresarial = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    email_personal = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    id_puesto = models.ForeignKey(
        Puestos,
        null=True, default=None,
        on_delete=models.SET_NULL
    )
    numero_anexo = models.CharField(
        max_length=25, null=True, blank=True, default=None,)
    numero_celular = models.CharField(
        max_length=25, null=True, blank=True, default=None,)
    fecha_ingreso = models.DateField(null=True, blank=True, default=None,)
    fecha_baja = models.DateField(null=True, blank=True, default=None,)
    estado = models.ForeignKey(
        Estados, on_delete=models.CASCADE, default=DEFAULT_STATUS)
    sueldo = models.DecimalField(
        max_digits=12, decimal_places=2, null=True, blank=False,
        default=Decimal('0.00'), )

    usr_usuario = models.CharField(
        max_length=25, null=True, blank=True, default=None,)

    area = models.ForeignKey(
        'catalogos.Area', null=True, default=None, blank=True, on_delete=models.SET_NULL
    )
    sucursal = models.ForeignKey(
        'catalogos.Sucursal', null=True, default=None, blank=True, on_delete=models.SET_NULL
    )
    direccion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)

    usuario_intranet = models.OneToOneField(
        'auth.User',
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        blank=False,
        related_name='empleado_intranet_set',
    )
    tipo_empleado = models.PositiveIntegerField(
        default=1, choices=OPTIONS_TIPO_EMPLEADO,
        null=False, blank=False,)

    username = None

    class Meta:
        verbose_name = "Empleados"
        get_latest_by = ["fhregistro", ]
        ordering = ["fhregistro", ]
        indexes = [
            models.Index(fields=['nombres'],),
            models.Index(fields=['apellidos'],),
            models.Index(fields=['fecha_ingreso'],),
            models.Index(fields=['fecha_baja'],),
            models.Index(fields=['fecha_nacimiento'],),
            models.Index(fields=['ndoc'],),
            models.Index(fields=['usr_usuario'],),
            models.Index(fields=['tipo_empleado'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['fregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    def __str__(self):
        return "{} {}".format(self.nombres, self.apellidos)

    @property
    def get_fullname(self):
        return "{} {}".format(self.nombres, self.apellidos)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class MarcaJefeConcesionario(CommonStructure):
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    concesionario = models.ForeignKey(Concesionario, on_delete=models.CASCADE)
    jefe = models.ForeignKey(Empleados, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Marca Jefe Concesionario"
        ordering = ['fhregistro', ]
        get_latest_by = ['fhregistro', ]
        indexes = [
            models.Index(fields=['fregistro'],),
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
        ]

    def __str__(self):
        return str(self.pk)


class MetaJefe(CommonStructure):
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    jefe = models.ForeignKey(Empleados, on_delete=models.CASCADE)
    anio = models.IntegerField(null=False, default=0)
    mes = models.IntegerField(null=False, default=0)
    meta = models.IntegerField(null=False, default=0)

    class Meta:
        verbose_name = "Meta Jefe"
        ordering = ['-fhregistro', ]
        get_latest_by = ['-fhregistro', ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['anio'],),
            models.Index(fields=['mes'],),
            models.Index(fields=['meta'],),
        ]

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class MetaMarca(CommonStructure):
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    anio = models.IntegerField(null=False, default=0)
    mes = models.IntegerField(null=False, default=0)
    meta = models.IntegerField(null=False, default=0)

    class Meta:
        verbose_name = "Meta Marca"
        ordering = ['-fhregistro', ]
        get_latest_by = ['-fhregistro', ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['fhmodificacion'],),
            models.Index(fields=['anio'],),
            models.Index(fields=['mes'],),
            models.Index(fields=['meta'],),
        ]

    def __str__(self):
        return str(self.pk)

    def save(self, *args, **kwargs):
        if not self.id is None:
            self.fhmodificacion = datetime.now()
        super().save(*args, **kwargs)


class CuponesModel(CommonStructure):
    cod_marca = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    marca = models.CharField(max_length=200, null=True,
                             blank=True, default=None,)
    modelo = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    version = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    material = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    vin = models.CharField(max_length=200, null=True,
                           blank=True, default=None,)
    ano_fabricacion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    ruc_concesionario = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    nombre_concesionario = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    cod_sucursal = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    nombre_sucursal = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    numero_factura = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    fecha_venta_importador = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    fecha_venta_cliente = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    fecha_entrega_cliente = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    precio_venta_importador = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    fecha_cancelacion_financiera = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    id_cupon = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    fecha_anulacion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)

    username = None
    fhregistro = None
    fhmodificacion = None

    class Meta:
        verbose_name = "Base Cupones extraida de SQL Server"
        indexes = [
            models.Index(fields=['cod_marca'],),
            models.Index(fields=['marca'],),
            models.Index(fields=['ruc_concesionario'],),
            models.Index(fields=['nombre_concesionario'],),
            models.Index(fields=['id_cupon'],),
            models.Index(fields=['fregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def fhregistro(self):
        raise AttributeError("'Manager' object has no attribute 'fhregistro'")

    @property
    def fhmodificacion(self):
        raise AttributeError(
            "'Manager' object has no attribute 'fhmodificacion'")

    def insert_bulk(self, **kwargs):
        try:
            wtable = 'metas_cuponesmodel'
            wcolumns = kwargs.get('columns') or {}
            wrows = kwargs.get('rows') or {}
            if wtable == '':
                raise ValueError('Table operation not specified.')
            if len(wrows) == 0:
                raise ValueError('There are not rows to insert.')
            wmodel_fields = kwargs.get('columns')
            wparse_columns = ','.join(wmodel_fields)
            wparse_values = ','.join(["%s" for item in wmodel_fields])
            wsentence = "insert into {}({}) values ({}) ;".format(
                wtable, wparse_columns, wparse_values)
            connection = MYSQLConnection()
            error, status = connection.execute_insert(wsentence, wrows)
            if error:
                raise ValueError(str(error))
        except Exception as e:
            wmessage = str(e)
            print(wmessage)

    def __str__(self):
        return self.id_cupon


class EmpleadosModel(CommonStructure):
    usr_id = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_nombre = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_puesto = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_email = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_usuario = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_telefono = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_particular = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_area = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_empresa = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_sucursal = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_direccion = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_foto = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    usr_estado = models.CharField(
        max_length=200, null=True, blank=True, default=None,)
    username = None
    fhregistro = None
    fhmodificacion = None

    class Meta:
        verbose_name = "Base Empleados extraida de SQL Server"
        indexes = [
            models.Index(fields=['usr_nombre'],),
            models.Index(fields=['usr_puesto'],),
            models.Index(fields=['usr_area'],),
            models.Index(fields=['usr_empresa'],),
            models.Index(fields=['usr_sucursal'],),
            models.Index(fields=['usr_estado'],),
            models.Index(fields=['fregistro'],),
        ]

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def fhregistro(self):
        raise AttributeError("'Manager' object has no attribute 'fhregistro'")

    @property
    def fhmodificacion(self):
        raise AttributeError(
            "'Manager' object has no attribute 'fhmodificacion'")

    def insert_bulk(self, **kwargs):
        try:
            wtable = self._meta.db_table
            wcolumns = kwargs.get('columns') or {}
            wrows = kwargs.get('rows') or {}
            if wtable == '':
                raise ValueError('Table operation not specified.')
            if len(wrows) == 0:
                raise ValueError('There are not rows to insert.')
            wmodel_fields = kwargs.get('columns')
            wparse_columns = ','.join(wmodel_fields)
            wparse_values = ','.join(["%s" for item in wmodel_fields])
            wsentence = "insert into {}({}) values ({}) ;".format(
                wtable, wparse_columns, wparse_values)
            connection = MYSQLConnection()
            error, status = connection.execute_insert(wsentence, wrows)
            if error:
                raise ValueError(str(error))
        except Exception as e:
            wmessage = str(e)
            print(wmessage)

    def __str__(self):
        return self.usr_nombre


class TablesTransfer(CommonStructure):
    fields = models.TextField(blank=True, null=True, default=None,)
    table = models.CharField(
        max_length=100, blank=True, null=True, default=None,)
    command_source = models.CharField(
        max_length=100, blank=True, null=True, default=None,)
    comments = models.CharField(
        max_length=100, blank=True, null=True, default=None,)
    additional_commands = models.TextField(
        blank=True, null=True, default=None,)
    estado = models.ForeignKey(Estados, on_delete=models.CASCADE)
    username = None
    fregistro = None

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")

    @property
    def fregistro(self):
        raise AttributeError("'Manager' object has no attribute 'fregistro'")

    def __str__(self):
        return self.comments
