
from django.core.management.base import BaseCommand
from apps.login import models as mdls
from apps.metas import models as mdlsmeta
from apps.login import utils as utl


class Command(BaseCommand):

    can_import_settings = True
    help = "Load default grants into table login_seguridad"

    DEFAULT_STATUS = 1
    LIST_GRANTS = [
        {"parametro": "PARAM_PASSWORD_LENGTH", "descripcion": "Longitud minima de clave de acceso",
         "valor": "0", "valor_default": "8", },
        {"parametro": "PARAM_PASSWORD_EXPIRATION", "descripcion": "Expiracion de clave de acceso en dias",
         "valor": "0", "valor_default": "30", },
        {"parametro": "PARAM_LOGIN_ATTEMPS", "descripcion": "Limite de intentos fallidos al momento de iniciar sesion",
         "valor": "0", "valor_default": "3", },
        {"parametro": "PARAM_ACCOUNT_INACTIVITY", "descripcion": "Tiempo maximo de inactividad de cuenta en dias",
         "valor": "0", "valor_default": "30", },
        {"parametro": "PARAM_ACCOUNT_LOCK_TIME", "descripcion": "Tiempo de Bloqueo por intentos fallidos al iniciar sesion",
         "valor": "0", "valor_default": "4", },
        {"parametro": "PARAM_PWDEXPIRATION_FLAG", "descripcion": "Estado de Expiracion de clave de acceso 0:NO,1:SI",
         "valor": "0", "valor_default": "0", },
        {"parametro": "PARAM_LOGATTEMPS_FLAG", "descripcion": "Estado de intentos fallidos al momento de iniciar sesion 0:NO,1:SI",
         "valor": "0", "valor_default": "0", },
        {"parametro": "PARAM_PWDLENGTH_FLAG", "descripcion": "Estado de Longitud minima de clave de acceso 0:NO,1:SI",
         "valor": "0", "valor_default": "0", },
        {"parametro": "PARAM_MAILRESET_EXPIRATION", "descripcion": "Tiempo disponible para recupero de clave expresado en horas",
         "valor": "0", "valor_default": "24", },
        {"parametro": "PARAM_ACCOUNT_INACTIVITY_FLAG", "descripcion": "Estado de tiempo maximo de inactividad por cuenta 0:NO,1:SI",
         "valor": "0", "valor_default": "0", },
        {"parametro": "PARAM_ACCOUNT_NEWPWD_FLAG", "descripcion": "Estado de reseteo de clave por creacion de cuentas 0:NO,1:SI",
         "valor": "0", "valor_default": "0", },
    ]

    def handle(self, *args, **options):
        for item in self.LIST_GRANTS:
            obj = mdls.Seguridad.objects.filter(
                parametro__exact=item.get("parametro"))
            if not obj.exists():
                wparameters = item.copy()
                wparameters["estado"] = mdlsmeta.Estados.objects.get(
                    id=self.DEFAULT_STATUS)
                objSeguridad = mdls.Seguridad(**wparameters)
                objSeguridad.save()
        self.stdout.write("{} Seguridad grants has been updated.".format(
            utl.Validators.getDatetimeText()))
