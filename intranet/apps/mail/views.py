from django.shortcuts import render
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from django.conf import settings
from django.shortcuts import render
from django.views import View
from apps.common.utils import Validators
# references to create a calendar file

from icalendar import (
    Calendar, Event, vCalAddress, vText
)
from datetime import datetime
import pytz
import os

# Create your views here.


class SendMailer(object):
    """docstring for ClassName"""
    _from = None
    _to = None
    _cc = None
    _bcc = None
    _subject = 'SKBerge Mailer - '
    _body = None
    _template = None
    _message = None
    _attachments = None

    def __init__(self, **kwargs):
        # super(ClassName, self).__init__()
        # self.arg = arg
        self._from = kwargs.get('from') or settings.DEFAULT_FROM_EMAIL
        self._to = kwargs.get('to') or list()
        self._cc = kwargs.get('cc') or list()
        self._bcc = kwargs.get('bcc') or list()
        if self._from != settings.DEFAULT_FROM_EMAIL:
            self._subject = kwargs.get(
                'subject') or 'Default subject from mail server'
        else:
            self._subject += kwargs.get(
                'subject') or 'Default subject from mail server'
        self._body = kwargs.get(
            'body') or 'Default Content Body from mail server'
        self._template = kwargs.get('template') or ''
        self._context = kwargs.get('context') or dict({})
        self._attachments = kwargs.get('attachments') or list({})

    def BuildMessage(self):
        status = True
        error = None
        try:
            if self._template:
                wtemplate = get_template(self._template)
                html_content = wtemplate.render(self._context)
                self._message = EmailMessage(
                    subject=self._subject,
                    body=html_content,
                    from_email=self._from,
                    to=self._to,
                    cc=self._cc,
                    bcc=self._bcc,
                )
                self._message.content_subtype = "html"
            else:
                self._message = EmailMessage(
                    subject=self._subject,
                    body=self._body,
                    from_email=self._from,
                    to=self._to,
                    cc=self._cc,
                    bcc=self._bcc,
                )
            if len(self._attachments) > 0:
                for item in self._attachments:
                    self._message.attach_file(item.get("file_path"))
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]

    def Send(self):
        status = True
        error = None
        try:
            self._message.send()
        except Exception as e:
            status = False
            error = str(e)
        return [error, status]


class ICalendarGenerator(object):

    TIMEZONE = "America/Lima"
    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    DATETIME_UID = "%Y%m%dT%H%M%S"
    TIMEZONE

    event_summary, event_description = None, None
    _event_from, _event_to, event_date = None, None, None,
    event_organizer = None
    event_attendees = None
    event_location = None
    _event_uid = None
    _event_type = None

    STATUS_ADDITION, STATUS_CHANGE, STATUS_CANCEL = 2, 3, 4

    def __init__(self, **kwargs):
        # super(ClassName, self).__init__()
        self.event_summary = kwargs.get("summary") or ''
        self.event_description = kwargs.get("description") or ''
        self._event_from = kwargs.get("time_from") or ''
        self._event_to = kwargs.get("time_to") or ''
        self.event_date = kwargs.get("time_date") or ''
        self.event_organizer = kwargs.get("organizer") or list({})
        self.event_attendees = kwargs.get("attendees") or list({})
        self.event_location = kwargs.get("location") or ''
        self._event_uid = kwargs.get("calendar_uid")
        self._event_type = kwargs.get("calendar_type_id")
        return

    @property
    def timezone_default(self):
        return pytz.timezone(self.TIMEZONE)

    @property
    def event_from(self):
        time_from = datetime.strptime(self._event_from, self.DATETIME_FORMAT)
        time_from = time_from.replace(tzinfo=self.timezone_default)
        return time_from

    @property
    def event_to(self):
        time_to = datetime.strptime(self._event_to, self.DATETIME_FORMAT)
        time_to = time_to.replace(tzinfo=self.timezone_default)
        return time_to

    @property
    def event_alert(self):
        alert_value = "{} 00:10:00".format(self.event_date)
        time_alert = datetime.strptime(alert_value, self.DATETIME_FORMAT)
        time_alert = time_alert.replace(tzinfo=self.timezone_default)
        return time_alert

    def _get_event_keys(self):
        return [
            "summary", "description", "dtstart",
            "dtend", "dtstamp"
        ]

    def _get_event_values(self):
        return [
            self.event_summary,
            self.event_description,
            self.event_from,
            self.event_to,
            self.event_alert,
        ]

    @property
    def datetime_hash(self):
        return Validators.getDatetimeHash()

    @property
    def get_uid(self):
        wuid = datetime.now().strftime(self.DATETIME_UID)
        if not self._event_uid is None:
            return self._event_uid
        return '{}/27346262376@mxm.dk'.format(wuid)

    def generateFile(self):
        wresponse = {}
        werror = None
        try:
            wtimezone = pytz.timezone(self.TIMEZONE)
            cal = Calendar()
            cal.add('prodid', '-//My calendar product//mxm.dk//')
            cal.add('version', '2.0')
            if self._event_type == self.STATUS_CANCEL:
                cal.add('method', 'CANCEL')

            wparameters = self._get_event_keys()
            wvalues = self._get_event_values()
            event = Event()
            for index, item in enumerate(wparameters):
                event.add(wparameters[index], wvalues[index])

            organizer = vCalAddress('MAILTO:{}'.format(
                self.event_organizer.get("email")))
            organizer.params["cn"] = vText(self.event_organizer.get("cname"))
            organizer.params["role"] = vText(self.event_organizer.get("role"))

            event["organizer"] = organizer
            event["location"] = vText(self.event_location)
            event["uid"] = self.get_uid
            event.add('priority', 5)
            if self._event_type == self.STATUS_CANCEL:
                event.add('status', vText('CANCELLED'))

            for item in self.event_attendees:
                attendee = vCalAddress('MAILTO:{}'.format(item.get("email")))
                attendee.params['cn'] = vText(item.get("name"))
                attendee.params['ROLE'] = vText('REQ-PARTICIPANT')
                event.add('attendee', attendee, encode=0)

            cal.add_component(event)
            calendar_filename = "{}.ics".format(self.datetime_hash)
            calendar_path = '{}/reservas_calendar/{}'.format(
                settings.MEDIA_ROOT, calendar_filename)
            f = open(calendar_path, 'wb')
            f.write(cal.to_ical())
            f.close()
            wresponse["file_path"] = calendar_path
            wresponse["filename"] = calendar_filename
            wresponse["uid"] = event.get("uid")
        except Exception as e:
            werror = str(e)

        return [wresponse, werror]


class ViewTemplateMail(View):
    template_name = 'mail/mail_template.html'

    def get(self, request):
        return render(request, self.template_name, locals())
