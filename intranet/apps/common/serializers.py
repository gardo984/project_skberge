#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    fhregistro_format = serializers.SerializerMethodField()
    fhmodificacion_format = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    username_modif = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)

    def get_fhregistro_format(self, obj):
        if hasattr(obj, 'fhregistro'):
            if not obj.fhregistro is None:
                return obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return None

    def get_fhmodificacion_format(self, obj):
        if hasattr(obj, 'fhmodificacion'):
            if not obj.fhmodificacion is None:
                return obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return None

    def get_username(self, obj):
        if hasattr(obj, 'username'):
            if not obj.username is None:
                return obj.username.username
        return None

    def get_username_modif(self, obj):
        if hasattr(obj, 'username_modif'):
            if not obj.username_modif is None:
                return obj.username_modif.username
        return None
