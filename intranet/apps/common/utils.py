#! /usr/bin/env python
# -*- encoding: utf8 -*-

import re
import os
from datetime import datetime
import hashlib


class Validators(object):
    """docstring for Validators"""

    def __init__(self, arg):
        super(Validators, self).__init__()
        self.arg = arg

    def isNumber(value):
        try:
            result = re.match("^([0-9]+)$", value)
            status = False if result is None else True
            return status
        except Exception as e:
            return False

    def isString(value):
        try:
            result = re.match("^([a-zA-Z]+)$", value)
            status = False if result is None else True
            return status
        except Exception as e:
            return False

    def isDate(wvalue, wformat):
        result = None
        status = True
        try:
            result = datetime.strptime(wvalue, wformat)
            if type(result) is datetime:
                status = True
            else:
                status = False
        except Exception as e:
            status = False
        return [result, status]

    def validateType(value, options=[]):
        try:
            if len(options) == 0:
                return False
            if type(value) in options:
                return True
        except Exception as e:
            return False

    def ifDirectoryExists(wpath):
        error = None
        try:
            if not os.path.exists(wpath):
                os.makedirs(wpath)
        except Exception as e:
            error = str(e)
        return error

    def removeKeys(wdata, wvalues=[]):
        new_result = dict(wdata)
        try:
            for x in wvalues:
                del new_result[x]
        except Exception as e:
            error = str(e)
            print(error)
        return new_result

    def getDatetimeHash():
        whash = None
        try:
            whash = datetime.now().strftime("%Y%m%d%H%M%S%f")
        except Exception as e:
            error = str(e)
            print(error)
        return whash

    def getMd5Hash(value, length=12):
        whash = None
        try:
            obj = hashlib.md5()
            obj.update(value.encode("utf8"))
            whash = obj.hexdigest()[-length:]
        except Exception as e:
            error = str(e)
            print(error)
        return whash

    def getDatetimeText():
        whash = None
        try:
            whash = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        except Exception as e:
            error = str(e)
            print(error)
        return whash

    def getPageParameters():
        return {
            "total": 0,
            "size": 10,
            "count": 21,
        }

    def getContentFile(wpath='', wencoding='utf8'):
        wcontent, werror = None, None
        try:
            if not os.path.exists(wpath):
                raise ValueError(str('path {} doesnt exists.'.format(wpath)))
            wfile = open(wpath, 'r', encoding=wencoding)
            wcontent = wfile.readlines()
            wfile.close()
        except Exception as e:
            werror = str(e)
        return [wcontent, werror]
