from django.shortcuts import render, redirect
from django.conf import settings
from django.views import View
from rest_framework.response import Response
from django.http import (
    JsonResponse, Http404,
)
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from django.db.models import (
    Value, Case, When, CharField, Count,
    Q, F,
    Prefetch,
)
from apps.metas import (
    models as mdlmet,
    serializers as srlzmet,
    querys as qmet
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srlzcat,
)
from apps.connections.config import (
    MYSQLConnection,
)
from apps.grants import forms as frm
from apps.common import utils as utl
from datetime import datetime, timedelta
from django.contrib.auth import models as mdlauth
from apps.login import serializers as srlzlog
import os
import json

# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'
DEFAULT_MAIN_PAGE = '/main/'
if settings.DEBUG == False:
    DEFAULT_MAIN_PAGE = os.path.join(settings.PREFIX_URL, 'main')


# Profile process
@method_decorator(login_required, name='dispatch')
class ProfileView(View):

    template_name = 'grants/grants_main.html'
    template_title = 'Perfiles de Sistema'
    template_header = 'SKBerge | Mantenimiento {}'.format(template_title)

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.read_group', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.read_group', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            Qlist = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                Qlist.add(
                    Q(name__icontains=wdescription) |
                    Q(id__icontains=wdescription),
                    Qlist.connector
                )
            if wdata.get("estado"):
                wstatus = 1 if wdata.get("estado") == 1 else 0
                Qlist.add(
                    Q(groupextension__is_active=wstatus),
                    Qlist.connector
                )

            wfields = ['id', 'name', 'groupextension', ]
            wresult = mdlauth.Group.objects.filter(Qlist) \
                .select_related("groupextension") \
                .order_by("id")

            page["total"] = wresult.count()
            wserial = srlzlog.GroupSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_group', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFGroup(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            objItem = mdlauth.Group.objects.filter(id=wdata.get("id"))
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, grupo especificado no existe.'))
            objItem = objItem.first()
            objItem.name = wparameters.get("name")
            if hasattr(objItem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objItem.groupextension.is_active = wstatus
            objItem.save()

            if wdata.get("permissions"):
                objItem.permissions.remove(*objItem.permissions.all())
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objItem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileRemoveView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qlist = Q(id__isnull=False)
            if wdata.get("id"):
                Qlist.add(Q(id=wdata.get("id")), Qlist.connector)
            objItem = mdlauth.Group.objects.filter(Qlist)
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(str('Error, perfil especificado no existe.'))
            objItem = objItem.first()
            if hasattr(objItem, 'groupextension'):
                objItem.groupextension.is_active = 0
            objItem.save()
            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileAppendView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(ProfileAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_group', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            f = frm.MFGroup(wdata)
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())
            wparameters = f.clean()
            # wparameters["username"] = request.user
            objItem = mdlauth.Group()
            objItem.name = wparameters.get("name")
            objItem.save()
            if hasattr(objItem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objItem.groupextension.is_active = wstatus
                objItem.groupextension.save()

            if wdata.get("permissions"):
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objItem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


# User process


@method_decorator(login_required, name='dispatch')
class UserView(View):

    template_name = 'grants/grants_usuarios.html'
    template_title = 'Usuarios de Sistema'
    template_header = 'SKBerge | Mantenimiento {}'.format(template_title)

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.read_user', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.read_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            Qlist = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                Qlist.add(
                    Q(first_name__icontains=wdescription) |
                    Q(last_name__icontains=wdescription) |
                    Q(username__icontains=wdescription),
                    Qlist.connector
                )
            if wdata.get("estado"):
                westado = 1 if wdata.get("estado") == 1 else 0
                Qlist.add(
                    Q(is_active=westado), Qlist.connector
                )
            wfields = [
                'id', 'username', 'first_name', 'last_name',
                'email', 'userextension', 'fullname',
                'is_active', 'date_joined', 'empleado_intranet_set',
            ]
            wresult = mdlauth.User.objects.filter(Qlist) \
                .order_by("first_name") \
                .select_related("userextension") \
                .select_related("empleado_intranet_set")

            page["total"] = wresult.count()
            wserial = srlzlog.UserSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserUpdateView(View):

    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 0
            f = frm.MFUser(wdata.get("fields"))
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())

            wparam = f.clean()
            objItem = mdlauth.User.objects.filter(
                username__iexact=wparam.get("usuario"))
            ufields = [
                "first_name", "last_name", "email", "username", "is_active",
            ]
            if objItem.exists():
                objItem.update(**{x: wparam.get(x) for x in ufields})
                objItem = objItem.first()
                # if new password was typed
                if wparam.get("password") != '':
                    objItem.set_password(wparam.get("password"))
                    objItem.save(update_fields=["password", ])
                # update the extended table of user
                if hasattr(objItem, 'userextension'):
                    objItem.userextension.dni = wparam.get("dni")
                    objItem.userextension.check_all = wparam.get("check_all")
                    objItem.userextension.salas_all = wparam.get("salas_all")
                    objItem.userextension.is_new = wparam.get("is_new")
                    objItem.userextension.fhmodificacion = datetime.now()
                    objItem.userextension.save(
                        update_fields=[
                            "dni", "fhmodificacion", "check_all",
                            "salas_all", "is_new",
                        ])

            # update grants
            status, error = update_grants(objItem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserRemoveView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            Qlist = Q(id__isnull=False)
            if wdata.get("id"):
                Qlist.add(Q(id=wdata.get("id")), Qlist.connector)
            objItem = mdlauth.User.objects.filter(Qlist)
            if not objItem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, usuario especificado no existe.'))
            objItem = objItem.first()
            objItem.is_active = 0
            objItem.save()

            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserAppendView(View):
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    def dispatch(self, *args, **kwargs):
        return super(UserAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_user', raise_exception=True),)
    def post(self, request):
        wresponse = self.defaultResponse.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 1
            f = frm.MFUser(wdata.get("fields"))
            if not f.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not f.is_valid():
                wresponse['status'] = 400
                raise ValueError(f.get_error_first())

            wparam = f.clean()
            ufields = [
                "first_name", "last_name", "email",
                "username", "is_active",
            ]
            objItem = mdlauth.User(**{x: wparam.get(x) for x in ufields})
            objItem.set_password(wparam.get("clave"))
            objItem.save()
            if hasattr(objItem, 'userextension'):
                objItem.userextension.dni = wparam.get("dni")
                objItem.userextension.check_all = wparam.get("check_all")
                objItem.userextension.salas_all = wparam.get("salas_all")
                objItem.userextension.is_new = wparam.get("is_new")
                objItem.userextension.fhmodificacion = datetime.now()
                objItem.userextension.save(
                    update_fields=[
                        "dni", "fhmodificacion", "check_all",
                        "salas_all", "is_new",
                    ])

            # update grants
            status, error = update_grants(objItem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objItem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if type(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


def update_grants(item, request):
    status, error = True, None
    try:
        wdata = json.loads(request.body.decode("utf8"))
        # user permissions
        wperms = mdlauth.Permission.objects.filter(
            id__in=wdata.get("permissions") or []
        )
        item.user_permissions.remove(
            *item.user_permissions.all())
        if wperms.count() > 0:
            item.user_permissions.add(*wperms)

        # group permissions member
        wgroups = mdlauth.Group.objects.filter(
            id__in=wdata.get("groups") or []
        )
        item.groups.remove(*item.groups.all())
        if wgroups.count() > 0:
            item.groups.add(*wgroups)

        # salas permissions
        wsalas = mdlcat.Sala.objects.filter(
            id__in=wdata.get("salas") or []
        )
        item.catalogos_usersala_user_set.all().delete()
        if wsalas.count() > 0:
            objsalas = [mdlcat.UserSala(
                sala=x, user=item,
                fhregistro=datetime.now(),
                username=request.user,
            ) for x in wsalas]
            item.catalogos_usersala_user_set.bulk_create(
                objsalas, wsalas.count()
            )

        # attach employee to the user
        wempleado = wdata.get("fields").get("empleado")
        if wempleado != None:
            if hasattr(item, 'empleado_intranet_set'):
                mdlmet.Empleados.objects.filter(
                    id__iexact=item.empleado_intranet_set.id) \
                    .update(usuario_intranet=None)

            mdlmet.Empleados.objects.filter(id__iexact=wempleado) \
                .update(usuario_intranet=item)
        else:
            if hasattr(item, 'empleado_intranet_set'):
                mdlmet.Empleados.objects.filter(
                    id__iexact=item.empleado_intranet_set.id) \
                    .update(usuario_intranet=None)

        status = True
    except Exception as e:
        status, error = False, str(e)
    return [status, error]
