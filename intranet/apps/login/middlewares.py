from django import http
from datetime import datetime, timedelta
from dateutil import relativedelta
import calendar
from apps.login import utils as utl
from django.urls import get_callable, reverse
from apps.login import (
    views as v_login,
)
from apps.catalogos import models as mdlcat
import re
from django.conf import settings


def _get_view_update_password():
    return v_login.LoginExpirationPassword.as_view()


class PasswordExpiration:

    def _url_exceptions(self, request):
        wpath = request.environ.get("PATH_INFO")
        if not settings.DEBUG:
            if request.environ.get("REQUEST_URI"):
                wpath = request.environ.get("REQUEST_URI")
            else:
                wpath = '{}{}'.format(settings.PREFIX_URL, wpath[1:])

        wexceptions = [
            reverse('login:logout'),
            reverse('login:update_password'),
        ]
        for path in wexceptions:
            if re.match(path, wpath):
                return True
        return False

    def __init__(self, get_response=None):
        self.get_response = get_response
        super().__init__()

    def __call__(self, request):
        response = None
        if hasattr(self, 'process_request'):
            response = self.process_request(request)
        if not response:
            response = self.get_response(request)
        if hasattr(self, 'process_response'):
            response = self.process_response(request, response)
        return response

    def process_request(self, request):
        # validating if is ajax process
        if request.is_ajax():
            return

        # validating path info match with any url exceptions
        if self._url_exceptions(request):
            return

        # check if account is new, if it applies new passwd should be set.
        ACCOUNT_NEWPWD_FLAG = utl.getSecurityParameter(
            'PARAM_ACCOUNT_NEWPWD_FLAG'
        )
        if ACCOUNT_NEWPWD_FLAG != 0:
            if hasattr(request.user, 'userextension'):
                objExtension = request.user.userextension
                if objExtension.is_new == 1:
                    request.password_expirate = True
                    return

        # check if validation is enable
        PASSWORD_EXPIRATION_FLAG = utl.getSecurityParameter(
            'PARAM_PWDEXPIRATION_FLAG'
        )

        if PASSWORD_EXPIRATION_FLAG == 0:
            return
        # validating if user instance and userextension exists
        if hasattr(request.user, 'userextension'):
            # getting expiration days limit
            EXPIRATION_DAYS = utl.getSecurityParameter(
                'PARAM_PASSWORD_EXPIRATION'
            )
            objExtension = request.user.userextension
            # if extension instance doesnt have fecha_cambio_clave, it is set
            if objExtension.fecha_cambio_clave is None:
                objExtension.fecha_cambio_clave = datetime.now()
                objExtension.save()
                return
            # getting difference between current date and last time it was
            # changed
            diff = relativedelta.relativedelta(
                datetime.now(), objExtension.fecha_cambio_clave
            )
            ndays = diff.days
            if diff.months > 0:
                days_month = calendar.monthrange(
                    datetime.now().year, datetime.now().month)[-1]
                ndays += days_month
            if ndays > EXPIRATION_DAYS:
                # creating an interval flag
                request.password_expirate = True
        else:
            if request.user.is_authenticated:
                wparameters = {
                    "username": request.user, "fecha_cambio_clave": datetime.now()
                }
                objExtension = mdlcat.UserExtension(**wparameters)
                objExtension.save()

        return self.get_response(request)

    def process_view(self, request, callback, callback_args, callback_kwargs):
        # validating if is ajax process
        if request.is_ajax():
            return
        # validating if password has expirated or not
        if hasattr(request, 'password_expirate'):
            if request.password_expirate:
                return _get_view_update_password()(request)
        return

    def process_response(self, request, response):
        return response


class SessionExpiration:

    def __init__(self, get_response=None):
        self.get_response = get_response
        super().__init__()

    def __call__(self, request):
        response = None
        if hasattr(self, 'process_request'):
            response = self.process_request(request)
        if not response:
            response = self.get_response(request)
        if hasattr(self, 'process_response'):
            response = self.process_response(request, response)
        return response

    def process_request(self, request):
        # print("primero")
        pass

    def process_response(self, request, response):
        # print("segundo")
        return response


class PrivilegeLayer:

    def __init__(self, get_response=None):
        self.get_response = get_response
        super().__init__()

    def __call__(self, request):
        response = None
        if hasattr(self, 'process_request'):
            response = self.process_request(request)
        if not response:
            response = self.get_response(request)
        if hasattr(self, 'process_response'):
            response = self.process_response(request, response)
        return response

    def process_request(self, request):
        # print("primero")
        pass

    def process_response(self, request, response):
        # print("segundo")
        return response
