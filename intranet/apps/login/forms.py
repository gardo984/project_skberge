from django import forms
from django.contrib.auth import (
    models as mdlauth,
    authenticate,
)
from django.db.models import Q, F
from django.utils.translation import gettext_lazy as _
from apps.catalogos import (
    models as mdlcat,
)
from apps.login import utils as utl
from apps.common import forms as frm
import json

# Model Forms

# Forms

DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'


class MFUser(forms.ModelForm):
    NO_REQUIRED = []

    usuario = forms.CharField(min_length=4, max_length=50, required=True,
                              error_messages={
                                  'no_empty': DEFAULT_MSG_REQUIRED, },
                              )
    clave = forms.CharField(min_length=4, max_length=50, required=True,
                            error_messages={
                                'no_empty': DEFAULT_MSG_REQUIRED, },
                            )

    class Meta:
        model = mdlauth.User
        fields = [
            'first_name', 'last_name', 'email',
            'usuario', 'clave',
        ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFUser, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        wstatus = authenticate(
            username=cleaned_data.get("usuario"),
            password=cleaned_data.get("clave")
        )
        if wstatus is None:
            wmessage = {
                "clave": "no paso validacion",
            }
            raise forms.ValidationError(wmessage)
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                if item in ["email", ]:
                    cleaned_data[item] = cleaned_data.get(item).lower()
                    continue
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field"), wresult.get("message"),
            )
        return wmessage


class MFEmail(forms.ModelForm):

    NO_REQUIRED = []

    class Meta:
        model = mdlauth.User
        fields = ['email', ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFEmail, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        Qlist = Q(
            email__exact=cleaned_data.get("email")
        )
        objuser = mdlauth.User.objects.filter(Qlist)
        if not objuser.exists():
            wmessage = {
                "email": "no se encuentra registrado",
            }
            raise forms.ValidationError(wmessage)
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field"), wresult.get("message"),
            )
        return wmessage


class MFResetPassword(frm.MFDefault):

    username = forms.CharField(
        label="Usuario", max_length=30, strip=True, required=False, )
    password = forms.CharField(
        label='Contraseña', max_length=30, strip=True, required=True,)
    password2 = forms.CharField(
        label='Confirmación Contraseña',
        max_length=30, strip=True, required=True,)

    hashid = forms.CharField(label="Hash ID", max_length=25,
                             strip=True, required=True,)

    NO_REQUIRED = []

    class Meta:
        fields = [
            'username', 'password', 'password2', 'hashid',
        ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFResetPassword, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

    def clean(self):
        cleaned_data = super().clean()
        d = cleaned_data.copy()
        # validation if both passwords are equal of differ between them
        if d.get("password") != d.get("password2"):
            wmessage = {
                "password": "diferente a clave de confirmación",
            }
            raise forms.ValidationError(wmessage)

        PWDLENGTH_FLAG = utl.getSecurityParameter("PARAM_PWDLENGTH_FLAG")
        # check if validation is enable
        if PWDLENGTH_FLAG != 0:
            # validating password length based on system configuration
            password_length = utl.getSecurityParameter("PARAM_PASSWORD_LENGTH")
            if len(d.get("password")) < password_length or \
                    len(d.get("password2")) < password_length:
                wmessage = {
                    "password": "no cumple longitud minima de caracteres",
                }
                raise forms.ValidationError(wmessage)

        Qlist = Q(
            hashid__exact=d.get("hashid"),
            fecha_recupero__isnull=True,
        )
        obj = mdlcat.RecuperoClave.objects.filter(Qlist)
        if not obj.exists():
            wmessage = {"hashid": "no paso validacion", }
            raise forms.ValidationError(wmessage)
        cleaned_data["username"] = obj.first().username
        return cleaned_data


class MFUpdatePassword(frm.MFDefault):

    username = forms.CharField(
        label="Usuario", max_length=30, strip=True, required=True, )
    current_password = forms.CharField(
        label='Contraseña', max_length=30, strip=True, required=True,)
    new_password = forms.CharField(
        label='Nueva Contraseña', max_length=30, strip=True, required=True,)
    new_password2 = forms.CharField(
        label='Confirmación Nueva Contraseña',
        max_length=30, strip=True, required=True,)

    NO_REQUIRED = []

    class Meta:
        fields = [
            'username', 'current_password', 'new_password', 'new_password2',
        ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFUpdatePassword, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

    def clean(self):
        cleaned_data = super().clean()
        d = cleaned_data.copy()
        # validation if both passwords are equal of differ between them
        if d.get("new_password") != d.get("new_password2"):
            wmessage = {"new_password": "diferente a clave de confirmación", }
            raise forms.ValidationError(wmessage)

        # validating user credentials
        user = authenticate(
            username=d.get("username"), password=d.get("current_password")
        )
        if user == None:
            wmessage = {
                "current_password": "clave ingresada no paso validacion",
            }
            raise forms.ValidationError(wmessage)

        # check if validation is enable
        PWDLENGTH_FLAG = utl.getSecurityParameter("PARAM_PWDLENGTH_FLAG")
        if PWDLENGTH_FLAG != 0:
            # validating password length based on system configuration
            password_length = utl.getSecurityParameter("PARAM_PASSWORD_LENGTH")
            if len(d.get("new_password")) < password_length or \
                    len(d.get("new_password2")) < password_length:
                wmessage = {
                    "new_password": "no cumple longitud minima de caracteres",
                }
                raise forms.ValidationError(wmessage)

        return cleaned_data
