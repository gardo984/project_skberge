from django.db import models
from apps.common.models import CommonStructure

# Create your models here.
DEFAULT_STATUS = 1


class ClaveHistoricos(CommonStructure):
    old_password = models.CharField(max_length=128, null=False, blank=False,)
    new_password = models.CharField(max_length=128, null=False, blank=False,)

    class Meta:
        verbose_name = "Historico Claves"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
        ]

    def __str__(self):
        return str(self.pk)


class Bloqueos(CommonStructure):
    ultima_fecha_login = models.DateTimeField(null=False,  blank=False)

    class Meta:
        verbose_name = "Bloqueos Usuarios"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['ultima_fecha_login'],),
        ]

    def __str__(self):
        return str(self.pk)


class Accesos(CommonStructure):

    type = models.CharField(max_length=25, null=False, blank=False,)
    user_agent = models.TextField(null=True, blank=True, default=None,)
    host_from = models.CharField(
        max_length=25, null=True, blank=True, default=None)
    referer = models.TextField(null=True, blank=True, default=None,)
    url = models.CharField(max_length=200, null=True, blank=True, default=None)
    content_type = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    method = models.CharField(max_length=25, null=True,
                              blank=True, default=None)

    class Meta:
        verbose_name = "Accesos Login"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['url'],),
            models.Index(fields=['type'],),
            models.Index(fields=['method'],),
        ]

    def __str__(self):
        return '{} | {}'.format(str(self.pk), self.url)


class Intentos(CommonStructure):

    type = models.CharField(max_length=25, null=False, blank=False,)
    user_agent = models.TextField(null=True, blank=True, default=None,)
    host_from = models.CharField(
        max_length=25, null=True, blank=True, default=None)
    referer = models.TextField(null=True, blank=True, default=None,)
    url = models.CharField(max_length=200, null=True, blank=True, default=None)
    content_type = models.CharField(
        max_length=100, null=True, blank=True, default=None,)
    method = models.CharField(max_length=25, null=True,
                              blank=True, default=None)
    userid = models.CharField(max_length=25, null=True,
                              blank=True, default=None)
    username = None

    class Meta:
        verbose_name = "Intentos Login"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['fhregistro'],),
            models.Index(fields=['url'],),
            models.Index(fields=['type'],),
            models.Index(fields=['method'],),
            models.Index(fields=['userid'],),
        ]

    def __str__(self):
        return '{} | {}'.format(str(self.pk), self.url)

    @property
    def username(self):
        raise AttributeError("'Manager' object has no attribute 'username'")


class Seguridad(CommonStructure):
    parametro = models.CharField(max_length=35, null=False, blank=False,)
    descripcion = models.CharField(max_length=100, null=False, blank=False,)
    valor = models.PositiveIntegerField(null=False, blank=False,
                                        default=0,)
    valor_default = models.PositiveIntegerField(null=True, blank=False,
                                                default=0,)
    estado = models.ForeignKey(
        'metas.Estados', on_delete=models.SET_NULL,
        null=True, default=DEFAULT_STATUS)

    class Meta:
        verbose_name = "Login Seguridad"
        get_latest_by = ["-fhregistro", ]
        ordering = ["-fhregistro", ]
        indexes = [
            models.Index(fields=['descripcion'],),
            models.Index(fields=['parametro'],),
            models.Index(fields=['fhregistro'],),
        ]

    def __str__(self):
        return str('{} | {}'.format(self.parametro, self.descripcion))
