from django.contrib import admin
from apps.login import models as mdls

# Register your models here.


class SeguridadAdmin(admin.ModelAdmin):
    list_display = [
        "id", "parametro", "descripcion",
        "valor", "valor_default", "estado",
        "_format_fhregistro", "_format_fhmodificacion",

    ]
    list_display_links = ["id", "parametro", "descripcion", ]
    fields = [
        "id", "parametro",
        "descripcion", "valor",
        "valor_default", "estado",
        "fhregistro",
        "fhmodificacion",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "parametro", "descripcion", "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class AccesosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "username", "type",
        "host_from",
        "method",
        "_format_fhregistro", "_format_fhmodificacion",

    ]
    list_display_links = ["id", "username", ]
    fields = [
        "id", "username",
        "type", "host_from",
        "user_agent", "referer",
        "url", "method",
        "fhregistro",
        "fhmodificacion",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "username", "type", "host_from",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class BloqueosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "username", "ultima_fecha_login",
        "_format_fhregistro", "_format_fhmodificacion",

    ]
    list_display_links = ["id", "username", "ultima_fecha_login"]
    fields = [
        "id", "username",
        "ultima_fecha_login",
        "fhregistro",
        "fhmodificacion",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "username",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class ClaveHistoricosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "username", "old_password", "new_password",
        "_format_fhregistro", "_format_fhmodificacion",

    ]
    list_display_links = ["id", "username", "old_password"]
    fields = [
        "id", "username",
        "old_password",
        "new_password",
        "fhregistro",
        "fhmodificacion",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "username",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"

wmodels = [
    mdls.ClaveHistoricos, mdls.Bloqueos,
    mdls.Accesos, mdls.Seguridad,
]
winterfaces = [
    ClaveHistoricosAdmin, BloqueosAdmin,
    AccesosAdmin, SeguridadAdmin,
]
for windex, witem in enumerate(wmodels):
    admin.site.register(wmodels[windex], winterfaces[windex])
