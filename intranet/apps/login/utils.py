
from django.contrib.auth import models as mdlauth
from django.db.models import Q, F
from apps.mail.views import SendMailer
from apps.catalogos import models as mdlcat
from apps.login import models as mdllogin
from apps.common.utils import Validators
from django.urls import reverse
from django.http.request import HttpRequest as request
from datetime import datetime, timedelta
from dateutil import relativedelta
from django.conf import settings

from apps.login import (
    models as mdls,
)

LOG_IN = 1
LOG_OUT = 2
LOG_ERROR = 3


def updateUserPassword(wusername, wpasswd):
    status, error = True, None
    try:
        objUser = mdlauth.User.objects.filter(username=wusername)
        if objUser.exists():
            objUser = objUser.first()
            woldpasswd = objUser.password
            objUser.set_password(wpasswd)
            objUser.save()
            if hasattr(objUser, 'userextension'):
                objUser.userextension.fecha_cambio_clave = datetime.now()
                objUser.userextension.is_new = 0
                objUser.userextension.save()
            # history password
            status, werror = savePasswordHistory(
                woldpasswd, objUser.password, objUser
            )
            if werror:
                raise ValueError(werror)
    except Exception as e:
        status = False
        error = str(e)
    return [status, error]


def getSecurityParameter(wparam):
    value = 0
    obj = mdllogin.Seguridad.objects.filter(
        parametro__exact=wparam, estado=1,
    )
    if obj.exists():
        item = obj.get()
        value = item.valor_default if item.valor == 0 else item.valor
    return value


def checkUserAttempsLimit(wuser):
    status, error = True, None
    try:
        ATTEMPTS_FLAG = getSecurityParameter("PARAM_LOGATTEMPS_FLAG")
        # check if validation is enable
        if ATTEMPTS_FLAG == 0:
            return [status, error]

        LIMIT_ATTEMTPS = getSecurityParameter("PARAM_LOGIN_ATTEMPS")
        INTERVAL_LOCK = getSecurityParameter("PARAM_ACCOUNT_LOCK_TIME")
        INTERVAL_RANGE = 2
        range_from = (datetime.now() - timedelta(minutes=INTERVAL_RANGE))
        range_to = datetime.now()
        Qfilters = Q(
            id__isnull=False, fhregistro__gte=range_from,
            fhregistro__lte=range_to, userid=wuser,
        )
        objIntento = mdllogin.Intentos.objects.filter(Qfilters)
        if objIntento.count() >= LIMIT_ATTEMTPS:
            objUser = mdlauth.User.objects.filter(username__exact=wuser)
            if objUser.exists():
                item = objUser.first()
                wstatus = True if hasattr(item, 'userextension') else False
                time_lock = datetime.now() + timedelta(minutes=INTERVAL_LOCK)
                if wstatus:
                    item.userextension.is_lock = 1
                    item.userextension.fecha_bloqueo = time_lock
                    item.userextension.save()
                else:
                    objBloqueo = mdlcat.UserExtension(
                        is_lock=1, username=item,
                        fecha_bloqueo=time_lock
                    )
                    objBloqueo.save()
    except Exception as e:
        status = False
        error = str(e)
        print(error)
    return [status, error]


def messageUserLock():
    return str("""
        Usuario se encuentra bloqueado por exceso de intentos, vuelva a intentarlo en otro momento.
    """).strip()


def checkIfUserIsLocked(wuser):
    status, error = True, None
    try:
        obj = mdlauth.User.objects.filter(username__exact=wuser)
        if obj.exists():
            user = obj.get()
            if hasattr(user, 'userextension'):
                if user.userextension.is_lock == 1 \
                        and user.userextension.fecha_bloqueo != None:
                    if user.userextension.fecha_bloqueo >= datetime.now():
                        raise ValueError(messageUserLock())
                user.userextension.is_lock = 0
                user.userextension.fecha_bloqueo = None
                user.userextension.save()
    except Exception as e:
        status = False
        error = str(e)
    return [status, error]


def saveUserAttemps(wuser, werror, request, wtype=3):
    status, error = True, None
    try:
        wparameters = {
            "type": 'LOG_ERROR' if wtype in [3, ] else 'LOG_DISABLED',
            "method": request.environ.get("REQUEST_METHOD"),
            "user_agent": request.environ.get("HTTP_USER_AGENT"),
            "host_from": request.environ.get("REMOTE_ADDR"),
            "referer": request.environ.get("HTTP_REFERER"),
            "content_type": request.environ.get("CONTENT_TYPE"),
            "url": request.build_absolute_uri(),
            "userid": wuser,
        }
        obj = mdls.Intentos(**wparameters)
        obj.save()
    except Exception as e:
        status = False
        error = str(e)
    return [status, error]


def saveUserLogs(wuser, request, wtype=1):
    status, error = True, None
    try:
        wparameters = {
            "type": 'LOGIN' if wtype in [1, ] else 'LOGOUT',
            "method": request.environ.get("REQUEST_METHOD"),
            "user_agent": request.environ.get("HTTP_USER_AGENT"),
            "host_from": request.environ.get("REMOTE_ADDR"),
            "referer": request.environ.get("HTTP_REFERER"),
            "content_type": request.environ.get("CONTENT_TYPE"),
            "url": request.build_absolute_uri(),
        }
        if isinstance(wuser, mdlauth.User):
            wparameters["username"] = wuser
        obj = mdls.Accesos(**wparameters)
        obj.save()
    except Exception as e:
        status = False
        error = str(e)
    return [status, error]


def savePasswordHistory(wold, wnew, wuser):
    status, error = True, None
    try:
        wparameters = {
            "old_password": wold,
            "new_password": wnew,
            "username": wuser,
        }
        obj = mdls.ClaveHistoricos(**wparameters)
        obj.save()
    except Exception as e:
        status = False
        error = str(e)
    return [status, error]


def resetPasswordUrlHash():
    whash = Validators.getMd5Hash(
        Validators.getDatetimeHash(), 11
    )
    wpath = reverse(
        'login:reset_password_hash',
        kwargs={"hashid": whash, },
    )
    return wpath


def changedPasswordMailBody():
    return """
        Por el presente se informa que su clave de acceso ha sido actualizada a la fecha de {},
        si usted no realizó dicha acción favor de contactarse al siguiente email {}.
    """.strip()


def resetPasswordMailBody():
    return """
		Para proceder con el restablecimiento de clave favor de
		ingresar al siguiente link {} el cual estará vigente sólo
		por un periodo de 24 horas.
	""".strip()


def changedPasswordSendMail(**kwargs):
    status, error = True, None
    try:
        # send mail
        wparameters = {
            "summary": "Cambio de Clave de acceso",
            "current_date": datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
        }
        wcontext = {
            "content_header": wparameters.get("summary"),
            "content_body": changedPasswordMailBody().format(
                wparameters.get("current_date"),
                settings.PROJECT_MAIL_CONTACT,
            ),
        }
        wsettings = {
            'subject': wparameters.get("summary"),
            "to": list(set([kwargs.get("email")])) or list(),
            "template": 'mail/mail_password_recovered.html',
            "context": wcontext,
        }
        objMail = SendMailer(**wsettings)
        error, status = objMail.BuildMessage()
        if error:
            raise ValueError(str(error))
        objMail.Send()
    except Exception as e:
        status, error = False, str(e)
        print(error)
    return [status, error]


def resetPasswordSendMail(**kwargs):
    status, error = True, None
    try:
        # send mail
        wparameters = {
            "summary": "Restablecimiento Clave de acceso",
        }
        wcontext = {
            "content_header": wparameters.get("summary"),
            "content_body": resetPasswordMailBody().format(
                kwargs.get("request").build_absolute_uri(
                    resetPasswordUrlHash()
                )
            ),
        }
        wsettings = {
            'subject': wparameters.get("summary"),
            "to": list(set([kwargs.get("email")])) or list(),
            "template": 'mail/mail_reset_password.html',
            "context": wcontext,
        }
        objMail = SendMailer(**wsettings)
        error, status = objMail.BuildMessage()
        if error:
            raise ValueError(str(error))
        objMail.Send()
        # save details about the email
        objItem = mdlauth.User.objects.filter(
            email__iexact=kwargs.get("email"))
        wdata = {
            "email": kwargs.get("email"),
            "hashid": wcontext.get("content_body").split("/")[-2],
            "username": objItem.first(),
            "fecha_vigencia": datetime.now() + timedelta(hours=24),
        }
        mdlcat.RecuperoClave.objects.create(**wdata)
    except Exception as e:
        status, error = False, str(e)
        print(error)
    return [status, error]


def resetPasswordValidateHash(whash=''):
    status, error = True, None
    try:
        Qlist = Q(
            hashid=whash,
            fecha_recupero__isnull=True,
        )
        obj = mdlcat.RecuperoClave.objects.filter(Qlist)
        # if hash id is not register in database
        if not obj.exists():
            raise ValueError(
                "Error, hashid doesn't exists or achieved its expiration date .")
        # validating if hashid has not expired
        diff = relativedelta.relativedelta(
            datetime.now(), obj.get().fhregistro
        )
        if diff.days >= 1:
            raise ValueError(
                "Error, hashid doesn't exists or achieved its expiration date .")

    except Exception as e:
        status, error = False, str(e)
    return [status, error]
