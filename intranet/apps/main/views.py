from django.shortcuts import render
from django.views import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
# Create your views here.


class MainView(View):

    template_name = 'main/index.html'
    defaultResponse = {
        "ok": 1,
        "status": 200,
        "result": "Request has been processed successfuly.",
    }

    @method_decorator(login_required)
    def get(self, request):
        warguments = {}
        return render(request, self.template_name, **warguments)
