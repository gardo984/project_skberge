drop procedure if exists `sp_metas_actualizar_empleados` ; 
CREATE  PROCEDURE `sp_metas_actualizar_empleados`()
begin
	drop table if exists tmp_empleados ;
	create table tmp_empleados 
	select z.*,
		ca.id as area_id, ca.descripcion as area_descripcion,  
		cs.id as sucursal_id, cs.descripcion as sucursal_descripcion,
		mp.id as puesto_id, mp.descripcion as puesto_descripcion,
		me.id as estado_id, me.descripcion as estadol_descripcion
	from (
		select 
			v.id,
			v.usr_nombre, v.usr_email, v.usr_usuario, 
			v.usr_telefono,  v.usr_particular, v.usr_direccion,
			v.usr_estado, 
			upper(v.usr_area) as usr_area, v.usr_sucursal,
			upper(v.usr_puesto) as usr_puesto
		from metas_empleadosmodel as v
	) as z
		left join catalogos_area as ca on ca.descripcion = z.usr_area
		left join catalogos_sucursal as cs on cs.cod_sucursal = z.usr_sucursal
		left join metas_puestos as mp on mp.descripcion = z.usr_puesto
		left join metas_estados as me on me.id = if(ifnull(z.usr_estado,0) in(0),2,1)
	group by z.id ;
	
	alter table tmp_empleados modify id int unsigned not null , add primary key (id) ; 
	alter table tmp_empleados add index(usr_nombre), add index(usr_usuario) ;
	update tmp_empleados set usr_nombre = upper(usr_nombre) ;
	
	## Update data
	set @cmd = concat("update metas_empleados as e inner join tmp_empleados as tm on {{wcondition}}
					set e.email_empresarial = tm.usr_email, e.usr_usuario = tm.usr_usuario, e.numero_anexo = tm.usr_telefono,
						e.numero_celular = tm.usr_particular, e.estado_id = tm.estado_id,
						e.direccion = tm.usr_direccion, e.area_id = tm.area_id,
						e.sucursal_id = tm.sucursal_id, e.id_puesto_id = tm.puesto_id,
						e.fhmodificacion = now() ") ;
	set @p_cmd1 = replace(@cmd,"{{wcondition}}","(ucase(concat(e.nombres,' ',e.apellidos)) = tm.usr_nombre) and length(ifnull(tm.usr_usuario,'')) = 0 ") ;
	set @p_cmd2 = replace(@cmd,"{{wcondition}}","(e.usr_usuario = tm.usr_usuario) and length(ifnull(tm.usr_usuario,'')) > 0") ;
	PREPARE cmd from @p_cmd1; execute cmd; deallocate prepare cmd;
	PREPARE cmd from @p_cmd2; execute cmd; deallocate prepare cmd;
				
	## Insert data
	
	set session group_concat_max_len =(1024*1024)*1024 ;
	select ifnull(group_concat(ucase(concat(e.nombres,' ',e.apellidos)) separator ","),'') as usr_nombres,
		ifnull(group_concat(usr_usuario separator ","),'') as usr_usuario
	from metas_empleados as e into @p_nombres, @p_usuarios ;
	
	set @cmd_ins = concat("insert into metas_empleados(nombres, apellidos, fhregistro,
						email_empresarial,usr_usuario,numero_anexo,numero_celular,
						estado_id,direccion,area_id,sucursal_id,id_puesto_id)
				select  substring_index(e.usr_nombre,' ',1) as nombres, 
					trim(replace(e.usr_nombre,substring_index(e.usr_nombre,' ',1),'')) as  apellidos,
					now() as fhregistro, e.usr_email, e.usr_usuario, e.usr_telefono, e.usr_particular, 
					e.estado_id, e.usr_direccion, e.area_id, e.sucursal_id, e.puesto_id
				from tmp_empleados as e where {{wcondition}} ");
			
	set @p_cmd1 = replace(@cmd_ins,"{{wcondition}}","not find_in_set(e.usr_nombre,@p_nombres) and length(ifnull(e.usr_usuario,'')) = 0") ;
	set @p_cmd2 = replace(@cmd_ins,"{{wcondition}}","not find_in_set(e.usr_usuario,@p_usuarios) and length(ifnull(e.usr_usuario,'')) > 0") ;
	PREPARE cmd from @p_cmd1; execute cmd; deallocate prepare cmd;
	PREPARE cmd from @p_cmd2; execute cmd; deallocate prepare cmd;
	
	drop table tmp_empleados ;
	select * from metas_empleados ;
end ;

drop procedure if exists `sp_metas_listar_concesionarios` ;
CREATE PROCEDURE `sp_metas_listar_concesionarios`(
	p_marca int unsigned, 
	p_jefe int unsigned
)
begin
	select  m.id, m.nombre, m.ndoc, if(mjc.id is null,0,1) as asignado 
	from metas_concesionario as m
		left join metas_marcajefeconcesionario as mjc on mjc.concesionario_id = m.id 
			and mjc.jefe_id = p_jefe and mjc.marca_id = p_marca
	order by m.nombre ASC ;
end ;

drop procedure if exists  `sp_metas_obtener_ventas`;
CREATE PROCEDURE `sp_metas_obtener_ventas`(
	p_month int unsigned,
	p_empleado int unsigned
)
begin
	select  z.id_marca, z.cod_marca, z.marca, z.empleado_id,
		z.empleado_nombres, z.empleado_email, z.id_concesionario,
		group_concat(distinct z.ruc_concesionario) as ruc_concesionario,
		group_concat(distinct z.nombre_concesionario) as nombre_concesionario,
		z.sueldo, sum(z.nventas) as nventas, 
		sum(z.meta) as meta, sum(z.nventas_anuladas) as nventas_anuladas,
		((sum(z.nventas_anuladas) / sum(z.nventas)) * 100) as nventas_anuladas_porc
	from (
		select 
			m.id as id_marca, v.cod_marca, v.marca,
			em.id as empleado_id, concat(em.nombres,' ',em.apellidos) as empleado_nombres,
			em.email_personal as empleado_email,
			c.id as id_concesionario, 
			group_concat(distinct v.ruc_concesionario) as ruc_concesionario, 
			group_concat(distinct v.nombre_concesionario) as nombre_concesionario, 
			v.fecha_venta_cliente, v.fecha_venta_importador,
			em.sueldo,sum(if(v.fecha_anulacion is null,1,0)) as nventas, ifnull(mj.meta,0) as meta,
			month(date(v.fecha_venta_cliente)) as mes,
			month(date(v.fecha_anulacion)) as mes_anulado,
			sum(if(v.fecha_anulacion is null,0,1)) as nventas_anuladas
		from metas_cuponesmodel as v
			inner join metas_concesionario as c on c.ndoc = v.ruc_concesionario
			inner join metas_marca as m on m.cod_marca = v.cod_marca
			inner join metas_marcajefeconcesionario as asg on asg.concesionario_id = c.id and asg.marca_id = m.id
			inner join metas_empleados as em on em.id = asg.jefe_id
			left join metas_metajefe as mj on mj.jefe_id = em.id and mj.marca_id = m.id 
					and mj.mes = month(date(v.fecha_venta_cliente))
		group by v.marca, em.id, month(date(v.fecha_venta_cliente))
		#group by v.marca,v.ruc_concesionario
	) as z
	where z.id_marca is not null
		and if(p_month is not null,p_month in (z.mes,z.mes_anulado) , z.id_marca is not null)
		and if(p_empleado is not null,z.empleado_id = p_empleado, z.id_marca is not null)
	group by z.marca, z.empleado_id 
	order by z.marca, z.empleado_id ;
end ;
